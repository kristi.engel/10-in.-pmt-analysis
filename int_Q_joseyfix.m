clear
maxidx = 1;
numnoise = 0;
myDir = '/Users/kristiengel/Documents/MATLAB/20200107/20200107-0005/'; %gets directory
myFiles = dir(fullfile(myDir,'*.csv')); %gets all txt files in struct
warning('off','all');
%figure1 = figure;
for k = 1:length(myFiles)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current time logcurrent tmpChannelA tmpTime units inftest
  baseFileName = myFiles(k).name;
  fullFileName = fullfile(myDir, baseFileName);
  fprintf(1, 'Now reading %s\n', fullFileName);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    inftest = test.ChannelA;
    j = 1;
    for j = 1:length(inftest)
        if strcmpi(inftest(j),'-Infinity') == 1
             fprintf(1, 'File %s contains a channel overage\n', fullFileName);
        end
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName);
      % Plot it just to make sure
        figure3 = figure;
        plot(m(:,1),m(:,2))
      numnoise = numnoise + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName);
               m(i,2)
            end
            time(j) = m(i,1)*(1e-9);
            if time(j) > 9e-8
                fprintf(1, '%s has a late pulse\n', fullFileName);
            end
            current(j) = m(i,2);
            %logcurrent(j) = log10(m(i,2));
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ(k) = trapz(time,current);
        if totalQ(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName);
             totalQ(k)
             %probidx = k
        end

        plot(time,current)
        %plot(time,logcurrent)
        title({'Current Wfm - Az.P 4, Track Case 2 - 20200107-0005'},'FontSize', 18);
        xlabel({'time (s)'},'FontSize', 14);
        ylabel({'-current/A'},'FontSize', 14);
        %ylabel({'log(-current/A)'});
        hold on
        
    
end


hold off
xmin = min(totalQ);
xmax = max(totalQ);
nbin = (xmax - xmin)/(50000000*(1.602e-19));
nbin = round(nbin);
figure2 = figure;
hist1 = histogram(totalQ,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.5)
hold on
% Create title
title({'Q - Azimuthal Position 4, Track Case 2 - 20200107-0005'},'FontSize', 18);
        xlabel({'Charge'},'FontSize', 14);
        ylabel({'Counts'},'FontSize', 14);
avgQ = mean(totalQ);
fprintf(1, 'Average Q = %s\n', avgQ);
sigQ = std(totalQ);
fprintf(1, 'Standard Deviation = %s\n', sigQ);