clear
%Case 1
maxidx = 1;
numnoise = 0;
myDir = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0001/'; %gets directory
myFiles = dir(fullfile(myDir,'*.csv')); %gets all txt files in struct
warning('off','all');
%figure1 = figure;
for k = 1:length(myFiles)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current time logcurrent
  baseFileName = myFiles(k).name;
  fullFileName = fullfile(myDir, baseFileName);
  fprintf(1, 'Now reading %s\n', fullFileName);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName);
      % Plot it just to make sure
        figure3 = figure;
        plot(m(:,1),m(:,2))
      numnoise = numnoise + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName);
               %probidx = k
            end
            time(j) = m(i,1)*(1e-9);
            current(j) = m(i,2);
            %logcurrent(j) = log10(m(i,2));
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ(k) = trapz(time,current);
        if totalQ(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName);
             totalQ(k)
             %probidx = k
        end

        %plot(time,current)
        %plot(time,logcurrent)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0005'});
        %xlabel({'time (s)'});
        %ylabel({'-current/A'});
        %ylabel({'log(-current/A)'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx1 = 1;
numnoise1 = 0;
myDir1 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0002/'; %gets directory
myFiles1 = dir(fullfile(myDir1,'*.csv')); %gets all txt files in struct
%figure3 = figure;
for k = 1:length(myFiles1)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current1 time1 tmpChannelA tmpTime units
  baseFileName1 = myFiles1(k).name;
  fullFileName1 = fullfile(myDir1, baseFileName1);
  fprintf(1, 'Now reading %s\n', fullFileName1);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName1);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName1);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName1);
      % Plot it just to make sure
        figure3 = figure;
        plot(m(:,1),m(:,2))
      numnoise1 = numnoise1 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx1 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx1
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx1:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx1 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName1);
               %probidx = k
            end
            time1(j) = m(i,1)*(1e-9);
            current1(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ1(k) = trapz(time1,current1);
        if totalQ1(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName1);
             totalQ1(k)
             %probidx = k
        end

        %plot(time1,current1)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0006'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx2 = 1;
numnoise2 = 0;
myDir2 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0003/'; %gets directory
myFiles2 = dir(fullfile(myDir2,'*.csv')); %gets all txt files in struct
%figure4 = figure;
for k = 1:length(myFiles2)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current2 time2 tmpChannelA tmpTime units
  baseFileName2 = myFiles2(k).name;
  fullFileName2 = fullfile(myDir2, baseFileName2);
  fprintf(1, 'Now reading %s\n', fullFileName2);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName2);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName2);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName2);
      % Plot it just to make sure
        figure4 = figure;
        plot(m(:,1),m(:,2))
      numnoise2 = numnoise2 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx2 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx2
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx2:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx2 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName2);
               %probidx = k
            end
            time2(j) = m(i,1)*(1e-9);
            current2(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ2(k) = trapz(time2,current2);
        if totalQ2(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName2);
             totalQ2(k)
             %probidx = k
        end

        %plot(time2,current2)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0007'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 2
maxidx3 = 1;
numnoise3 = 0;
myDir3 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0004/'; %gets directory
myFiles3 = dir(fullfile(myDir3,'*.csv')); %gets all txt files in struct
%figure5 = figure;
for k = 1:length(myFiles3)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current3 time3 tmpChannelA tmpTime units
  baseFileName3 = myFiles3(k).name;
  fullFileName3 = fullfile(myDir3, baseFileName3);
  fprintf(1, 'Now reading %s\n', fullFileName3);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName3);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName3);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName3);
      % Plot it just to make sure
        figure5 = figure;
        plot(m(:,1),m(:,2))
      numnoise3 = numnoise3 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx3 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx3
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx3:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx3 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName3);
               %probidx = k
            end
            time3(j) = m(i,1)*(1e-9);
            current3(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ3(k) = trapz(time3,current3);
        if totalQ3(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName3);
             totalQ3(k)
             %probidx = k
        end

        %plot(time3,current3)
        %title({'Current Wfm - Az.P 4, Track Case 2 - 20191202-0008'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx4 = 1;
numnoise4 = 0;
myDir4 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0005/'; %gets directory
myFiles4 = dir(fullfile(myDir4,'*.csv')); %gets all txt files in struct
%figure6 = figure;
for k = 1:length(myFiles4)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current4 time4 tmpChannelA tmpTime units
  baseFileName4 = myFiles4(k).name;
  fullFileName4 = fullfile(myDir4, baseFileName4);
  fprintf(1, 'Now reading %s\n', fullFileName4);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName4);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName4);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName4);
      % Plot it just to make sure
        figure6 = figure;
        plot(m(:,1),m(:,2))
      numnoise4 = numnoise4 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx4 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx4
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx4:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx4 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName4);
               %probidx = k
            end
            time4(j) = m(i,1)*(1e-9);
            current4(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ4(k) = trapz(time4,current4);
        if totalQ4(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName4);
             totalQ4(k)
             %probidx = k
        end

        %plot(time4,current4)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0009'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 3
maxidx5 = 1;
numnoise5 = 0;
myDir5 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0006/'; %gets directory
myFiles5 = dir(fullfile(myDir5,'*.csv')); %gets all txt files in struct
%figure7 = figure;
for k = 1:length(myFiles5)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current5 time5 tmpChannelA tmpTime units
  baseFileName5 = myFiles5(k).name;
  fullFileName5 = fullfile(myDir5, baseFileName5);
  fprintf(1, 'Now reading %s\n', fullFileName5);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName5);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName5);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName5);
      % Plot it just to make sure
        figure7 = figure;
        plot(m(:,1),m(:,2))
      numnoise5 = numnoise5 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx5 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx5
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx5:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx5 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName5);
               %probidx = k
            end
            time5(j) = m(i,1)*(1e-9);
            current5(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ5(k) = trapz(time5,current5);
        if totalQ5(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName5);
             totalQ5(k)
             %probidx = k
        end

        %plot(time5,current5)
        %title({'Current Wfm - Az.P 4, Track Case 3 - 20191202-0010'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx6 = 1;
numnoise6 = 0;
myDir6 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0007/'; %gets directory
myFiles6 = dir(fullfile(myDir6,'*.csv')); %gets all txt files in struct
%figure8 = figure;
for k = 1:length(myFiles6)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current6 time6 tmpChannelA tmpTime units
  baseFileName6 = myFiles6(k).name;
  fullFileName6 = fullfile(myDir6, baseFileName6);
  fprintf(1, 'Now reading %s\n', fullFileName6);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName6);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName6);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName6);
      % Plot it just to make sure
        figure8 = figure;
        plot(m(:,1),m(:,2))
      numnoise6 = numnoise6 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx6 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx6
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx6:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx6 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName6);
               %probidx = k
            end
            time6(j) = m(i,1)*(1e-9);
            current6(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ6(k) = trapz(time6,current6);
        if totalQ6(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName6);
             totalQ6(k)
             %probidx = k
        end

        %plot(time6,current6)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0011'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 4
maxidx7 = 1;
numnoise7 = 0;
myDir7 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0008/'; %gets directory
myFiles7 = dir(fullfile(myDir7,'*.csv')); %gets all txt files in struct
%figure9 = figure;
for k = 1:length(myFiles7)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current7 time7 tmpChannelA tmpTime units
  baseFileName7 = myFiles7(k).name;
  fullFileName7 = fullfile(myDir7, baseFileName7);
  fprintf(1, 'Now reading %s\n', fullFileName7);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName7);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName7);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName7);
      % Plot it just to make sure
        figure9 = figure;
        plot(m(:,1),m(:,2))
      numnoise7 = numnoise7 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx7 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx7
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx7:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx7 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName7);
               %probidx = k
            end
            time7(j) = m(i,1)*(1e-9);
            current7(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ7(k) = trapz(time7,current7);
        if totalQ7(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName7);
             totalQ7(k)
             %probidx = k
        end

        %plot(time7,current7)
        %title({'Current Wfm - Az.P 4, Track Case 4 - 20191202-0012'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx8 = 1;
numnoise8 = 0;
myDir8 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0009/'; %gets directory
myFiles8 = dir(fullfile(myDir8,'*.csv')); %gets all txt files in struct
%figure10 = figure;
for k = 1:length(myFiles8)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current8 time8 tmpChannelA tmpTime units
  baseFileName8 = myFiles8(k).name;
  fullFileName8 = fullfile(myDir8, baseFileName8);
  fprintf(1, 'Now reading %s\n', fullFileName8);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName8);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName8);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName8);
      % Plot it just to make sure
        figure10 = figure;
        plot(m(:,1),m(:,2))
      numnoise8 = numnoise8 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx8 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx8
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx8:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx8 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName8);
               %probidx = k
            end
            time8(j) = m(i,1)*(1e-9);
            current8(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ8(k) = trapz(time8,current8);
        if totalQ8(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName8);
             totalQ8(k)
             %probidx = k
        end

        %plot(time8,current8)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0013'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 5
maxidx9 = 1;
numnoise9 = 0;
myDir9 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0010/'; %gets directory
myFiles9 = dir(fullfile(myDir9,'*.csv')); %gets all txt files in struct
%figure11 = figure;
for k = 1:length(myFiles9)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current9 time9 tmpChannelA tmpTime units
  baseFileName9 = myFiles9(k).name;
  fullFileName9 = fullfile(myDir9, baseFileName9);
  fprintf(1, 'Now reading %s\n', fullFileName9);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName9);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName9);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName9);
      % Plot it just to make sure
        figure11 = figure;
        plot(m(:,1),m(:,2))
      numnoise9 = numnoise9 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx9 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx9
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx9:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx9 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName9);
               %probidx = k
            end
            time9(j) = m(i,1)*(1e-9);
            current9(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ9(k) = trapz(time9,current9);
        if totalQ9(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName9);
             totalQ9(k)
             %probidx = k
        end

        %plot(time9,current9)
        %title({'Current Wfm - Az.P 4, Track Case 5 - 20191202-0014'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx10 = 1;
numnoise10 = 0;
myDir10 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0012/'; %gets directory
myFiles10 = dir(fullfile(myDir10,'*.csv')); %gets all txt files in struct
%figure12 = figure;
for k = 1:length(myFiles10)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current10 time10 tmpChannelA tmpTime units
  baseFileName10 = myFiles10(k).name;
  fullFileName10 = fullfile(myDir10, baseFileName10);
  fprintf(1, 'Now reading %s\n', fullFileName10);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName10);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName10);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName10);
      % Plot it just to make sure
        figure12 = figure;
        plot(m(:,1),m(:,2))
      numnoise10 = numnoise10 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx10 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx10
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx10:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx10 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName10);
               %probidx = k
            end
            time10(j) = m(i,1)*(1e-9);
            current10(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ10(k) = trapz(time10,current10);
        if totalQ10(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName10);
             totalQ10(k)
             %probidx = k
        end

        %plot(time10,current10)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0015'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 6
maxidx11 = 1;
numnoise11 = 0;
myDir11 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0014/'; %gets directory
myFiles11 = dir(fullfile(myDir11,'*.csv')); %gets all txt files in struct
%figure13 = figure;
for k = 1:length(myFiles11)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current11 time11 tmpChannelA tmpTime units
  baseFileName11 = myFiles11(k).name;
  fullFileName11 = fullfile(myDir11, baseFileName11);
  fprintf(1, 'Now reading %s\n', fullFileName11);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName11);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName11);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName11);
      % Plot it just to make sure
        figure13 = figure;
        plot(m(:,1),m(:,2))
      numnois11 = numnoise11 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx11 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx11
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx11:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx11 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName11);
               %probidx = k
            end
            time11(j) = m(i,1)*(1e-9);
            current11(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ11(k) = trapz(time11,current11);
        if totalQ11(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName11);
             totalQ11(k)
             %probidx = k
        end

        %plot(time11,current11)
        %title({'Current Wfm - Az.P 4, Track Case 6 - 20191202-0016'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx12 = 1;
numnoise12 = 0;
myDir12 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0015/'; %gets directory
myFiles12 = dir(fullfile(myDir12,'*.csv')); %gets all txt files in struct
%figure14 = figure;
for k = 1:length(myFiles12)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current12 time12 tmpChannelA tmpTime units
  baseFileName12 = myFiles12(k).name;
  fullFileName12 = fullfile(myDir12, baseFileName12);
  fprintf(1, 'Now reading %s\n', fullFileName12);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName12);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName12);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName12);
      % Plot it just to make sure
        figure14 = figure;
        plot(m(:,1),m(:,2))
      numnoise12 = numnoise12 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx12 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx12
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx12:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx12 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName12);
               %probidx = k
            end
            time12(j) = m(i,1)*(1e-9);
            current12(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ12(k) = trapz(time12,current12);
        if totalQ12(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName12);
             totalQ12(k)
             %probidx = k
        end

        %plot(time12,current12)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0017'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 7
maxidx13 = 1;
numnoise13 = 0;
myDir13 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0016/'; %gets directory
myFiles13 = dir(fullfile(myDir13,'*.csv')); %gets all txt files in struct
%figure15 = figure;
for k = 1:length(myFiles13)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current13 time13 tmpChannelA tmpTime units
  baseFileName13 = myFiles13(k).name;
  fullFileName13 = fullfile(myDir13, baseFileName13);
  fprintf(1, 'Now reading %s\n', fullFileName13);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName13);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName13);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName13);
      % Plot it just to make sure
        figure15 = figure;
        plot(m(:,1),m(:,2))
      numnois13 = numnoise13 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx13 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx13
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx13:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx13 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName13);
               %probidx = k
            end
            time13(j) = m(i,1)*(1e-9);
            current13(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ13(k) = trapz(time13,current13);
        if totalQ13(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName13);
             totalQ13(k)
             %probidx = k
        end

        %plot(time13,current13)
        %title({'Current Wfm - Az.P 4, Track Case 7 - 20191202-0018'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx14 = 1;
numnoise14 = 0;
myDir14 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0017/'; %gets directory
myFiles14 = dir(fullfile(myDir14,'*.csv')); %gets all txt files in struct
%figure16 = figure;
for k = 1:length(myFiles14)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current14 time14 tmpChannelA tmpTime units
  baseFileName14 = myFiles14(k).name;
  fullFileName14 = fullfile(myDir14, baseFileName14);
  fprintf(1, 'Now reading %s\n', fullFileName14);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName14);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName14);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName14);
      % Plot it just to make sure
        figure16 = figure;
        plot(m(:,1),m(:,2))
      numnoise14 = numnoise14 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx14 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx14
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx14:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx14 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName14);
               %probidx = k
            end
            time14(j) = m(i,1)*(1e-9);
            current14(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ14(k) = trapz(time14,current14);
        if totalQ14(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName14);
             totalQ14(k)
             %probidx = k
        end

        %plot(time14,current14)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0019'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 8
maxidx15 = 1;
numnoise15 = 0;
myDir15 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0018/'; %gets directory
myFiles15 = dir(fullfile(myDir15,'*.csv')); %gets all txt files in struct
%figure17 = figure;
for k = 1:length(myFiles15)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current15 time15 tmpChannelA tmpTime units
  baseFileName15 = myFiles15(k).name;
  fullFileName15 = fullfile(myDir15, baseFileName15);
  fprintf(1, 'Now reading %s\n', fullFileName15);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName15);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName15);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName15);
      % Plot it just to make sure
        figure17 = figure;
        plot(m(:,1),m(:,2))
      numnois15 = numnoise15 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx15 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx15
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx15:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx15 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName15);
               %probidx = k
            end
            time15(j) = m(i,1)*(1e-9);
            current15(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ15(k) = trapz(time15,current15);
        if totalQ15(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName15);
             totalQ15(k)
             %probidx = k
        end

        %plot(time15,current15)
        %title({'Current Wfm - Az.P 4, Track Case 8 - 20191202-0020'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx16 = 1;
numnoise16 = 0;
myDir16 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0019/'; %gets directory
myFiles16 = dir(fullfile(myDir16,'*.csv')); %gets all txt files in struct
%figure18 = figure;
for k = 1:length(myFiles16)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current16 time16 tmpChannelA tmpTime units
  baseFileName16 = myFiles16(k).name;
  fullFileName16 = fullfile(myDir16, baseFileName16);
  fprintf(1, 'Now reading %s\n', fullFileName16);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName16);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName16);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName16);
      % Plot it just to make sure
        figure18 = figure;
        plot(m(:,1),m(:,2))
      numnoise16 = numnoise16 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx16 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx16
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx16:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx16 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName16);
               %probidx = k
            end
            time16(j) = m(i,1)*(1e-9);
            current16(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ16(k) = trapz(time16,current16);
        if totalQ16(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName16);
             totalQ16(k)
             %probidx = k
        end

        %plot(time16,current16)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0021'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 9
maxidx17 = 1;
numnoise17 = 0;
myDir17 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0020/'; %gets directory
myFiles17 = dir(fullfile(myDir17,'*.csv')); %gets all txt files in struct
%figure19 = figure;
for k = 1:length(myFiles17)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current17 time17 tmpChannelA tmpTime units
  baseFileName17 = myFiles17(k).name;
  fullFileName17 = fullfile(myDir17, baseFileName17);
  fprintf(1, 'Now reading %s\n', fullFileName17);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName17);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName17);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName17);
      % Plot it just to make sure
        figure19 = figure;
        plot(m(:,1),m(:,2))
      numnois17 = numnoise17 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx17 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx17
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx17:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx17 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName17);
               %probidx = k
            end
            time17(j) = m(i,1)*(1e-9);
            current17(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ17(k) = trapz(time17,current17);
        if totalQ17(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName17);
             totalQ17(k)
             %probidx = k
        end

        %plot(time17,current17)
        %title({'Current Wfm - Az.P 4, Track Case 9 - 20191202-0022'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx18 = 1;
numnoise18 = 0;
myDir18 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0022/'; %gets directory
myFiles18 = dir(fullfile(myDir18,'*.csv')); %gets all txt files in struct
%figure20 = figure;
for k = 1:length(myFiles18)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current18 time18 tmpChannelA tmpTime units
  baseFileName18 = myFiles18(k).name;
  fullFileName18 = fullfile(myDir18, baseFileName18);
  fprintf(1, 'Now reading %s\n', fullFileName18);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName18);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName18);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName18);
      % Plot it just to make sure
        figure20 = figure;
        plot(m(:,1),m(:,2))
      numnoise18 = numnoise18 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx18 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx18
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx18:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx18 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName18);
               %probidx = k
            end
            time18(j) = m(i,1)*(1e-9);
            current18(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ18(k) = trapz(time18,current18);
        if totalQ18(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName18);
             totalQ18(k)
             %probidx = k
        end

        %plot(time18,current18)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0023'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 10
maxidx19 = 1;
numnoise19 = 0;
myDir19 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0023/'; %gets directory
myFiles19 = dir(fullfile(myDir19,'*.csv')); %gets all txt files in struct
%figure21 = figure;
for k = 1:length(myFiles19)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current19 time19 tmpChannelA tmpTime units
  baseFileName19 = myFiles19(k).name;
  fullFileName19 = fullfile(myDir19, baseFileName19);
  fprintf(1, 'Now reading %s\n', fullFileName19);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName19);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName19);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName19);
      % Plot it just to make sure
        figure21 = figure;
        plot(m(:,1),m(:,2))
      numnois19 = numnoise19 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx19 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx19
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx19:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx19 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName19);
               %probidx = k
            end
            time19(j) = m(i,1)*(1e-9);
            current19(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ19(k) = trapz(time19,current19);
        if totalQ19(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName19);
             totalQ19(k)
             %probidx = k
        end

        %plot(time19,current19)
        %title({'Current Wfm - Az.P 4, Track Case 10 - 20191202-0024'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx20 = 1;
numnoise20 = 0;
myDir20 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0024/'; %gets directory
myFiles20 = dir(fullfile(myDir20,'*.csv')); %gets all txt files in struct
%figure22 = figure;
for k = 1:length(myFiles20)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current20 time20 tmpChannelA tmpTime units
  baseFileName20 = myFiles20(k).name;
  fullFileName20 = fullfile(myDir20, baseFileName20);
  fprintf(1, 'Now reading %s\n', fullFileName20);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName20);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName20);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName20);
      % Plot it just to make sure
        figure22 = figure;
        plot(m(:,1),m(:,2))
      numnoise20 = numnoise20 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx20 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx20
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx20:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx20 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName20);
               %probidx = k
            end
            time20(j) = m(i,1)*(1e-9);
            current20(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ20(k) = trapz(time20,current20);
        if totalQ20(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName20);
             totalQ20(k)
             %probidx = k
        end

        %plot(time20,current20)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0025'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 11
maxidx21 = 1;
numnoise21 = 0;
myDir21 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0025/'; %gets directory
myFiles21 = dir(fullfile(myDir21,'*.csv')); %gets all txt files in struct
%figure23 = figure;
for k = 1:length(myFiles21)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current21 time21 tmpChannelA tmpTime units
  baseFileName21 = myFiles21(k).name;
  fullFileName21 = fullfile(myDir21, baseFileName21);
  fprintf(1, 'Now reading %s\n', fullFileName21);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName21);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName21);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName21);
      % Plot it just to make sure
        figure23 = figure;
        plot(m(:,1),m(:,2))
      numnois21 = numnoise21 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx21 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx21
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx21:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx21 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName21);
               %probidx = k
            end
            time21(j) = m(i,1)*(1e-9);
            current21(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ21(k) = trapz(time21,current21);
        if totalQ21(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName21);
             totalQ21(k)
             %probidx = k
        end

        %plot(time21,current21)
        %title({'Current Wfm - Az.P 4, Track Case 11 - 20191202-0026'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx22 = 1;
numnoise22 = 0;
myDir22 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0026/'; %gets directory
myFiles22 = dir(fullfile(myDir22,'*.csv')); %gets all txt files in struct
%figure24 = figure;
for k = 1:length(myFiles22)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current22 time22 tmpChannelA tmpTime units
  baseFileName22 = myFiles22(k).name;
  fullFileName22 = fullfile(myDir22, baseFileName22);
  fprintf(1, 'Now reading %s\n', fullFileName22);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName22);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName22);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName22);
      % Plot it just to make sure
        figure24 = figure;
        plot(m(:,1),m(:,2))
      numnoise22 = numnoise22 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx22 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx22
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx22:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx22 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName22);
               %probidx = k
            end
            time22(j) = m(i,1)*(1e-9);
            current22(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ22(k) = trapz(time22,current22);
        if totalQ22(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName22);
             totalQ22(k)
             %probidx = k
        end

        %plot(time22,current22)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0027'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 12
maxidx23 = 1;
numnoise23 = 0;
myDir23 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0027/'; %gets directory
myFiles23 = dir(fullfile(myDir23,'*.csv')); %gets all txt files in struct
%figure25 = figure;
for k = 1:length(myFiles23)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current23 time23 tmpChannelA tmpTime units
  baseFileName23 = myFiles23(k).name;
  fullFileName23 = fullfile(myDir23, baseFileName23);
  fprintf(1, 'Now reading %s\n', fullFileName23);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName23);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName23);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName23);
      % Plot it just to make sure
        figure25 = figure;
        plot(m(:,1),m(:,2))
      numnois23 = numnoise23 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx23 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx23
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx23:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx23 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName23);
               %probidx = k
            end
            time23(j) = m(i,1)*(1e-9);
            current23(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ23(k) = trapz(time23,current23);
        if totalQ23(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName23);
             totalQ23(k)
             %probidx = k
        end

        %plot(time23,current23)
        %title({'Current Wfm - Az.P 4, Track Case 12 - 20191202-0028'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx24 = 1;
numnoise24 = 0;
myDir24 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0028/'; %gets directory
myFiles24 = dir(fullfile(myDir24,'*.csv')); %gets all txt files in struct
%figure26 = figure;
for k = 1:length(myFiles24)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current24 time24 tmpChannelA tmpTime units
  baseFileName24 = myFiles24(k).name;
  fullFileName24 = fullfile(myDir24, baseFileName24);
  fprintf(1, 'Now reading %s\n', fullFileName24);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName24);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName24);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName24);
      % Plot it just to make sure
        figure26 = figure;
        plot(m(:,1),m(:,2))
      numnoise24 = numnoise24 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx24 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx24
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx24:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx24 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName24);
               %probidx = k
            end
            time24(j) = m(i,1)*(1e-9);
            current24(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ24(k) = trapz(time24,current24);
        if totalQ24(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName24);
             totalQ24(k)
             %probidx = k
        end

        %plot(time24,current24)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0029'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 13
maxidx25 = 1;
numnoise25 = 0;
myDir25 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0030/'; %gets directory
myFiles25 = dir(fullfile(myDir25,'*.csv')); %gets all txt files in struct
%figure27 = figure;
for k = 1:length(myFiles25)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current25 time25 tmpChannelA tmpTime units
  baseFileName25 = myFiles25(k).name;
  fullFileName25 = fullfile(myDir25, baseFileName25);
  fprintf(1, 'Now reading %s\n', fullFileName25);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName25);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName25);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName25);
      % Plot it just to make sure
        figure27 = figure;
        plot(m(:,1),m(:,2))
      numnois25 = numnoise25 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx25 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx25
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx25:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx25 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName25);
               %probidx = k
            end
            time25(j) = m(i,1)*(1e-9);
            current25(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ25(k) = trapz(time25,current25);
        if totalQ25(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName25);
             totalQ25(k)
             %probidx = k
        end

        %plot(time25,current25)
        %title({'Current Wfm - Az.P 4, Track Case 13 - 20191202-0030'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx26 = 1;
numnoise26 = 0;
myDir26 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0031/'; %gets directory
myFiles26 = dir(fullfile(myDir26,'*.csv')); %gets all txt files in struct
%figure28 = figure;
for k = 1:length(myFiles26)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current26 time26 tmpChannelA tmpTime units
  baseFileName26 = myFiles26(k).name;
  fullFileName26 = fullfile(myDir26, baseFileName26);
  fprintf(1, 'Now reading %s\n', fullFileName26);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName26);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName26);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName26);
      % Plot it just to make sure
        figure28 = figure;
        plot(m(:,1),m(:,2))
      numnoise26 = numnoise26 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx26 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx26
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx26:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx26 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName26);
               %probidx = k
            end
            time26(j) = m(i,1)*(1e-9);
            current26(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ26(k) = trapz(time26,current26);
        if totalQ26(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName26);
             totalQ26(k)
             %probidx = k
        end

        %plot(time26,current26)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0031'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 14
maxidx27 = 1;
numnoise27 = 0;
myDir27 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0032/'; %gets directory
myFiles27 = dir(fullfile(myDir27,'*.csv')); %gets all txt files in struct
%figure29 = figure;
for k = 1:length(myFiles27)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current27 time27 tmpChannelA tmpTime units
  baseFileName27 = myFiles27(k).name;
  fullFileName27 = fullfile(myDir27, baseFileName27);
  fprintf(1, 'Now reading %s\n', fullFileName27);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName27);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName27);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName27);
      % Plot it just to make sure
        figure29 = figure;
        plot(m(:,1),m(:,2))
      numnois27 = numnoise27 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx27 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx27
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx27:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx27 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName27);
               %probidx = k
            end
            time27(j) = m(i,1)*(1e-9);
            current27(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ27(k) = trapz(time27,current27);
        if totalQ27(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName27);
             totalQ27(k)
             %probidx = k
        end

        %plot(time27,current27)
        %title({'Current Wfm - Az.P 4, Track Case 14 - 20191202-0032'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx28 = 1;
numnoise28 = 0;
myDir28 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0033/'; %gets directory
myFiles28 = dir(fullfile(myDir28,'*.csv')); %gets all txt files in struct
%figure30 = figure;
for k = 1:length(myFiles28)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current28 time28 tmpChannelA tmpTime units
  baseFileName28 = myFiles28(k).name;
  fullFileName28 = fullfile(myDir28, baseFileName28);
  fprintf(1, 'Now reading %s\n', fullFileName28);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName28);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName28);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName28);
      % Plot it just to make sure
        figure30 = figure;
        plot(m(:,1),m(:,2))
      numnoise28 = numnoise28 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx28 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx28
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx28:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx28 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName28);
               %probidx = k
            end
            time28(j) = m(i,1)*(1e-9);
            current28(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ28(k) = trapz(time28,current28);
        if totalQ28(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName28);
             totalQ28(k)
             %probidx = k
        end

        %plot(time28,current28)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0033'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx29 = 1;
numnoise29 = 0;
myDir29 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0034/'; %gets directory
myFiles29 = dir(fullfile(myDir29,'*.csv')); %gets all txt files in struct
%figure31 = figure;
for k = 1:length(myFiles29)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current29 time29 tmpChannelA tmpTime units
  baseFileName29 = myFiles29(k).name;
  fullFileName29 = fullfile(myDir29, baseFileName29);
  fprintf(1, 'Now reading %s\n', fullFileName29);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName29);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName29);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName29);
      % Plot it just to make sure
        figure31 = figure;
        plot(m(:,1),m(:,2))
      numnoise29 = numnoise29 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx29 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx29
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx29:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx29 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName29);
               %probidx = k
            end
            time29(j) = m(i,1)*(1e-9);
            current29(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ29(k) = trapz(time29,current29);
        if totalQ29(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName29);
             totalQ29(k)
             %probidx = k
        end

        %plot(time29,current29)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0033'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx30 = 1;
numnoise30 = 0;
myDir30 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0035/'; %gets directory
myFiles30 = dir(fullfile(myDir30,'*.csv')); %gets all txt files in struct
%figure32 = figure;
for k = 1:length(myFiles30)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current30 time30 tmpChannelA tmpTime units
  baseFileName30 = myFiles30(k).name;
  fullFileName30 = fullfile(myDir30, baseFileName30);
  fprintf(1, 'Now reading %s\n', fullFileName30);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName30);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName30);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName30);
      % Plot it just to make sure
        figure32 = figure;
        plot(m(:,1),m(:,2))
      numnoise30 = numnoise30 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx30 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx30
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx30:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx30 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName30);
               %probidx = k
            end
            time30(j) = m(i,1)*(1e-9);
            current30(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ30(k) = trapz(time30,current30);
        if totalQ30(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName30);
             totalQ30(k)
             %probidx = k
        end

        %plot(time30,current30)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0033'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx31 = 1;
numnoise31 = 0;
myDir31 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0036/'; %gets directory
myFiles31 = dir(fullfile(myDir31,'*.csv')); %gets all txt files in struct
%figure33 = figure;
for k = 1:length(myFiles31)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current31 time31 tmpChannelA tmpTime units
  baseFileName31 = myFiles31(k).name;
  fullFileName31 = fullfile(myDir31, baseFileName31);
  fprintf(1, 'Now reading %s\n', fullFileName31);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName31);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName31);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName31);
      % Plot it just to make sure
        figure33 = figure;
        plot(m(:,1),m(:,2))
      numnoise31 = numnoise31 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx31 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx31
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx31:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx31 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName31);
               %probidx = k
            end
            time31(j) = m(i,1)*(1e-9);
            current31(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ31(k) = trapz(time31,current31);
        if totalQ31(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName31);
             totalQ31(k)
             %probidx = k
        end

        %plot(time31,current31)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0033'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 2
maxidx32 = 1;
numnoise32 = 0;
myDir32 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0037/'; %gets directory
myFiles32 = dir(fullfile(myDir32,'*.csv')); %gets all txt files in struct
%figure34 = figure;
for k = 1:length(myFiles32)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current32 time32 tmpChannelA tmpTime units
  baseFileName32 = myFiles32(k).name;
  fullFileName32 = fullfile(myDir32, baseFileName32);
  fprintf(1, 'Now reading %s\n', fullFileName32);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName32);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName32);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName32);
      % Plot it just to make sure
        figure34 = figure;
        plot(m(:,1),m(:,2))
      numnoise32 = numnoise32 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx32 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx32
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx32:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx32 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName32);
               %probidx = k
            end
            time32(j) = m(i,1)*(1e-9);
            current32(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ32(k) = trapz(time32,current32);
        if totalQ32(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName32);
             totalQ32(k)
             %probidx = k
        end

        %plot(time32,current32)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0033'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx33 = 1;
numnoise33 = 0;
myDir33 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0038/'; %gets directory
myFiles33 = dir(fullfile(myDir33,'*.csv')); %gets all txt files in struct
%figure35 = figure;
for k = 1:length(myFiles33)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current33 time33 tmpChannelA tmpTime units
  baseFileName33 = myFiles33(k).name;
  fullFileName33 = fullfile(myDir33, baseFileName33);
  fprintf(1, 'Now reading %s\n', fullFileName33);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName33);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName33);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName33);
      % Plot it just to make sure
        figure35 = figure;
        plot(m(:,1),m(:,2))
      numnoise33 = numnoise33 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx33 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx33
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx33:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx33 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName33);
               %probidx = k
            end
            time33(j) = m(i,1)*(1e-9);
            current33(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ33(k) = trapz(time33,current33);
        if totalQ33(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName33);
             totalQ33(k)
             %probidx = k
        end

        %plot(time33,current33)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0033'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 7
maxidx34 = 1;
numnoise34 = 0;
myDir34 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0039/'; %gets directory
myFiles34 = dir(fullfile(myDir34,'*.csv')); %gets all txt files in struct
%figure36 = figure;
for k = 1:length(myFiles34)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current34 time34 tmpChannelA tmpTime units
  baseFileName34 = myFiles34(k).name;
  fullFileName34 = fullfile(myDir34, baseFileName34);
  fprintf(1, 'Now reading %s\n', fullFileName34);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName34);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName34);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName34);
      % Plot it just to make sure
        figure36 = figure;
        plot(m(:,1),m(:,2))
      numnoise34 = numnoise34 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx34 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx34
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx34:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx34 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName34);
               %probidx = k
            end
            time34(j) = m(i,1)*(1e-9);
            current34(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ34(k) = trapz(time34,current34);
        if totalQ34(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName34);
             totalQ34(k)
             %probidx = k
        end

        %plot(time34,current34)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0033'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx35 = 1;
numnoise35 = 0;
myDir35 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0040/'; %gets directory
myFiles35 = dir(fullfile(myDir35,'*.csv')); %gets all txt files in struct
%figure37 = figure;
for k = 1:length(myFiles35)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current35 time35 tmpChannelA tmpTime units
  baseFileName35 = myFiles35(k).name;
  fullFileName35 = fullfile(myDir35, baseFileName35);
  fprintf(1, 'Now reading %s\n', fullFileName35);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName35);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName35);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName35);
      % Plot it just to make sure
        figure37 = figure;
        plot(m(:,1),m(:,2))
      numnoise35 = numnoise35 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx35 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx35
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx35:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx35 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName35);
               %probidx = k
            end
            time35(j) = m(i,1)*(1e-9);
            current35(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ35(k) = trapz(time35,current35);
        if totalQ35(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName35);
             totalQ35(k)
             %probidx = k
        end

        %plot(time35,current35)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0033'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 8
maxidx36 = 1;
numnoise36 = 0;
myDir36 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0041/'; %gets directory
myFiles36 = dir(fullfile(myDir36,'*.csv')); %gets all txt files in struct
%figure38 = figure;
for k = 1:length(myFiles36)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current36 time36 tmpChannelA tmpTime units
  baseFileName36 = myFiles36(k).name;
  fullFileName36 = fullfile(myDir36, baseFileName36);
  fprintf(1, 'Now reading %s\n', fullFileName36);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName36);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName36);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName36);
      % Plot it just to make sure
        figure38 = figure;
        plot(m(:,1),m(:,2))
      numnoise36 = numnoise36 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx36 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx36
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx36:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx36 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName36);
               %probidx = k
            end
            time36(j) = m(i,1)*(1e-9);
            current36(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ36(k) = trapz(time36,current36);
        if totalQ36(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName36);
             totalQ36(k)
             %probidx = k
        end

        %plot(time36,current36)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0033'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx37 = 1;
numnoise37 = 0;
myDir37 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0042/'; %gets directory
myFiles37 = dir(fullfile(myDir37,'*.csv')); %gets all txt files in struct
%figure39 = figure;
for k = 1:length(myFiles37)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current37 time37 tmpChannelA tmpTime units
  baseFileName37 = myFiles37(k).name;
  fullFileName37 = fullfile(myDir37, baseFileName37);
  fprintf(1, 'Now reading %s\n', fullFileName37);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName37);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName37);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName37);
      % Plot it just to make sure
        figure39 = figure;
        plot(m(:,1),m(:,2))
      numnoise37 = numnoise37 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx37 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx37
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx37:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx37 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName37);
               %probidx = k
            end
            time37(j) = m(i,1)*(1e-9);
            current37(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ37(k) = trapz(time37,current37);
        if totalQ37(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName37);
             totalQ37(k)
             %probidx = k
        end

        %plot(time37,current37)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0033'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 9
maxidx38 = 1;
numnoise38 = 0;
myDir38 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0043/'; %gets directory
myFiles38 = dir(fullfile(myDir38,'*.csv')); %gets all txt files in struct
%figure40 = figure;
for k = 1:length(myFiles38)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current38 time38 tmpChannelA tmpTime units
  baseFileName38 = myFiles38(k).name;
  fullFileName38 = fullfile(myDir38, baseFileName38);
  fprintf(1, 'Now reading %s\n', fullFileName38);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName38);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName38);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName38);
      % Plot it just to make sure
        figure40 = figure;
        plot(m(:,1),m(:,2))
      numnoise38 = numnoise38 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx38 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx38
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx38:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx38 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName38);
               %probidx = k
            end
            time38(j) = m(i,1)*(1e-9);
            current38(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ38(k) = trapz(time38,current38);
        if totalQ38(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName38);
             totalQ38(k)
             %probidx = k
        end

        %plot(time38,current38)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0033'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx39 = 1;
numnoise39 = 0;
myDir39 = '/Users/kristiengel/Documents/MATLAB/20191204/20191204-0044/'; %gets directory
myFiles39 = dir(fullfile(myDir39,'*.csv')); %gets all txt files in struct
%figure41 = figure;
for k = 1:length(myFiles39)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current39 time39 tmpChannelA tmpTime units
  baseFileName39 = myFiles39(k).name;
  fullFileName39 = fullfile(myDir39, baseFileName39);
  fprintf(1, 'Now reading %s\n', fullFileName39);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName39);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName39);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName39);
      % Plot it just to make sure
        figure41 = figure;
        plot(m(:,1),m(:,2))
      numnoise39 = numnoise39 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx39 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx39
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx39:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx39 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName39);
               %probidx = k
            end
            time39(j) = m(i,1)*(1e-9);
            current39(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ39(k) = trapz(time39,current39);
        if totalQ39(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName39);
             totalQ39(k)
             %probidx = k
        end

        %plot(time39,current39)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0033'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%----------------------------------------------------------

hold off
xmin = min(totalQ);
xmax = max(totalQ);
nbin = (xmax - xmin)/(50000000*(1.602e-19));
nbin = round(nbin);
figure2 = figure;
axes2 = axes('Parent',figure2);
hold(axes2,'on');
%hist1 = histogram(totalQ,nbin,'Normalization','probability',...
%'DisplayStyle','stairs','LineWidth',0.5);
%set(hist1,'DisplayName','Case 1-0001, 1213h','EdgeColor',[0 0 0]);
hold on
%
hist2 = histogram(totalQ1,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1);
set(hist2,'DisplayName','Case 1-0002, 1216h','EdgeColor',[0 0 0]);
%
%hist3 = histogram(totalQ2,nbin,'Normalization','probability',...
%'DisplayStyle','stairs','LineWidth',0.5);
%set(hist3,'DisplayName','Case 1-0003, 1219h','EdgeColor',[0 0 0]);
%
hist4 = histogram(totalQ3,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1);
set(hist4,'DisplayName','Case 2-0004, 1222h');
%
%hist5 = histogram(totalQ4,nbin,'Normalization','probability',...
%'DisplayStyle','stairs','LineWidth',0.5);
%set(hist5,'DisplayName','Case 1-0005, 1225h','EdgeColor',[0 0 0]);
%
hist6 = histogram(totalQ5,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1);
set(hist6,'DisplayName','Case 3-0006, 1228h');
%
%hist7 = histogram(totalQ6,nbin,'Normalization','probability',...
%'DisplayStyle','stairs','LineWidth',0.5);
%set(hist7,'DisplayName','Case 1-0007, 1230h','EdgeColor',[0 0 0]);
%
hist8 = histogram(totalQ7,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1);
set(hist8,'DisplayName','Case 4-0008, 1233h');
%
%hist9 = histogram(totalQ8,nbin,'Normalization','probability',...
%'DisplayStyle','stairs','LineWidth',0.5);
%set(hist9,'DisplayName','Case 1-0009, 1235h','EdgeColor',[0 0 0]);
%
hist10 = histogram(totalQ9,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1);
set(hist10,'DisplayName','Case 5-0010, 1238h');
%
%hist11 = histogram(totalQ10,nbin,'Normalization','probability',...
%'DisplayStyle','stairs','LineWidth',0.5);
%set(hist11,'DisplayName','Case 1-0012, 1243h','EdgeColor',[0 0 0]);
%
hist12 = histogram(totalQ11,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1);
set(hist12,'DisplayName','Case 6-0014, 1247h');
%
%hist13 = histogram(totalQ12,nbin,'Normalization','probability',...
%'DisplayStyle','stairs','LineWidth',0.5);
%set(hist13,'DisplayName','Case 1-0015, 1250h','EdgeColor',[0 0 0]);
%
hist14 = histogram(totalQ13,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1);
set(hist14,'DisplayName','Case 7-0016, 1253h');
%
%hist15 = histogram(totalQ14,nbin,'Normalization','probability',...
%'DisplayStyle','stairs','LineWidth',0.5);
%set(hist15,'DisplayName','Case 1-0017, 1256h','EdgeColor',[0 0 0]);
%
hist16 = histogram(totalQ15,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1);
set(hist16,'DisplayName','Case 8-0018, 1259h');
%
%hist17 = histogram(totalQ16,nbin,'Normalization','probability',...
%'DisplayStyle','stairs','LineWidth',0.5);
%set(hist17,'DisplayName','Case 1-0019, 1302h','EdgeColor',[0 0 0]);
%
hist18 = histogram(totalQ17,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1);
set(hist18,'DisplayName','Case 9-0020, 1304h');
%
%hist19 = histogram(totalQ18,nbin,'Normalization','probability',...
%'DisplayStyle','stairs','LineWidth',0.5);
%set(hist19,'DisplayName','Case 1-0022, 1306h','EdgeColor',[0 0 0]);
%
hist20 = histogram(totalQ19,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1);
set(hist20,'DisplayName','Case 10-0023, 1313h');
%
%hist21 = histogram(totalQ20,nbin,'Normalization','probability',...
%'DisplayStyle','stairs','LineWidth',0.5);
%set(hist21,'DisplayName','Case 1-0024, 1316h','EdgeColor',[0 0 0]);
%
hist22 = histogram(totalQ21,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1);
set(hist22,'DisplayName','Case 11-0025, 1319h');
%
%hist23 = histogram(totalQ22,nbin,'Normalization','probability',...
%'DisplayStyle','stairs','LineWidth',0.5);
%set(hist23,'DisplayName','Case 1-0026, 1323h','EdgeColor',[0 0 0]);
%
hist24 = histogram(totalQ23,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1);
set(hist24,'DisplayName','Case 12-0027, 1325h');
%
%hist25 = histogram(totalQ24,nbin,'Normalization','probability',...
%'DisplayStyle','stairs','LineWidth',0.5);
%set(hist25,'DisplayName','Case 1-0028, 1330h','EdgeColor',[0 0 0]);
%
hist26 = histogram(totalQ25,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1);
set(hist26,'DisplayName','Case 13-0030, 1349h');
%
%hist27 = histogram(totalQ26,nbin,'Normalization','probability',...
%'DisplayStyle','stairs','LineWidth',0.5);
%set(hist27,'DisplayName','Case 1-0031, 1416h','EdgeColor',[0 0 0]);
%
hist28 = histogram(totalQ27,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1);
set(hist28,'DisplayName','Case 14-0032, 1418h');
%
%hist29 = histogram(totalQ28,nbin,'Normalization','probability',...
%'DisplayStyle','stairs','LineWidth',0.5);
%set(hist29,'DisplayName','Case 1-0033, 1442h','EdgeColor',[0 0 0]);
%
%hist30 = histogram(totalQ29,nbin,'Normalization','probability',...
%'DisplayStyle','stairs','LineWidth',0.5);
%set(hist30,'DisplayName','Case 1-0034, 1630h','EdgeColor',[0 0 0]);
%
hist31 = histogram(totalQ30,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1);
set(hist31,'DisplayName','Case 1-0035, 1633h','EdgeColor',[0 0 0]);
%
%hist32 = histogram(totalQ31,nbin,'Normalization','probability',...
%'DisplayStyle','stairs','LineWidth',0.5);
%set(hist32,'DisplayName','Case 1-0036, 1635h','EdgeColor',[0 0 0]);
%
hist33 = histogram(totalQ32,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1);
set(hist33,'DisplayName','Case 2-0037, 1637h');
%
%hist34 = histogram(totalQ33,nbin,'Normalization','probability',...
%'DisplayStyle','stairs','LineWidth',0.5);
%set(hist34,'DisplayName','Case 1-0038, 1640h','EdgeColor',[0 0 0]);
%
hist35 = histogram(totalQ34,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1);
set(hist35,'DisplayName','Case 7-0039, 1642h');
%
%hist36 = histogram(totalQ35,nbin,'Normalization','probability',...
%'DisplayStyle','stairs','LineWidth',0.5);
%set(hist36,'DisplayName','Case 1-0040, 1645h','EdgeColor',[0 0 0]);
%
hist37 = histogram(totalQ36,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1);
set(hist37,'DisplayName','Case 8-0041, 1648h');
%
%hist38 = histogram(totalQ37,nbin,'Normalization','probability',...
%'DisplayStyle','stairs','LineWidth',0.5);
%set(hist38,'DisplayName','Case 1-0042, 1650h','EdgeColor',[0 0 0]);
%
hist39 = histogram(totalQ38,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1);
set(hist39,'DisplayName','Case 9-0043, 1653h');
%
%hist40 = histogram(totalQ39,nbin,'Normalization','probability',...
%'DisplayStyle','stairs','LineWidth',0.5);
%set(hist40,'DisplayName','Case 1-0044, 1656h','EdgeColor',[0 0 0]);
%
% Create title
title({'Q - Azimuthal Position 4, December 4th, 2019 - Coincidence = 2'});
box(axes2,'on');
legend1 = legend(axes2,'show');
%------------
avgQ = mean(totalQ);
fprintf(1, 'Average Q (1-0001) = %s\n', avgQ);
sigQ = std(totalQ);
fprintf(1, '.    Standard Deviation (1-0001) = %s\n', sigQ);
%
avgQ1 = mean(totalQ1);
fprintf(1, 'Average Q (1-0002) = %s\n', avgQ1);
sigQ1 = std(totalQ1);
fprintf(1, '.    Standard Deviation (1-0002) = %s\n', sigQ1);
%
avgQ2 = mean(totalQ2);
fprintf(1, 'Average Q (1-0003) = %s\n', avgQ2);
sigQ2 = std(totalQ2);
fprintf(1, '.    Standard Deviation (1-0003) = %s\n', sigQ2);
%
avgQ3 = mean(totalQ3);
fprintf(1, 'Average Q (2-0004) = %s\n', avgQ3);
sigQ3 = std(totalQ3);
fprintf(1, '.    Standard Deviation (2-0004) = %s\n', sigQ3);
%
avgQ4 = mean(totalQ4);
fprintf(1, 'Average Q (1-0005) = %s\n', avgQ4);
sigQ4 = std(totalQ4);
fprintf(1, '.    Standard Deviation (1-0005) = %s\n', sigQ4);
%
avgQ5 = mean(totalQ5);
fprintf(1, 'Average Q (3-0006) = %s\n', avgQ5);
sigQ5 = std(totalQ5);
fprintf(1, '.    Standard Deviation (3-0006) = %s\n', sigQ5);
%
avgQ6 = mean(totalQ6);
fprintf(1, 'Average Q (1-0007) = %s\n', avgQ6);
sigQ6 = std(totalQ6);
fprintf(1, '.    Standard Deviation (1-0007) = %s\n', sigQ6);
%
avgQ7 = mean(totalQ7);
fprintf(1, 'Average Q (4-0008) = %s\n', avgQ7);
sigQ7 = std(totalQ7);
fprintf(1, '.    Standard Deviation (4-0008) = %s\n', sigQ7);
%
avgQ8 = mean(totalQ8);
fprintf(1, 'Average Q (1-0009) = %s\n', avgQ8);
sigQ8 = std(totalQ8);
fprintf(1, '.    Standard Deviation (1-0009) = %s\n', sigQ8);
%
avgQ9 = mean(totalQ9);
fprintf(1, 'Average Q (5-0010) = %s\n', avgQ9);
sigQ9 = std(totalQ9);
fprintf(1, '.    Standard Deviation (5-0010) = %s\n', sigQ9);
%
avgQ10 = mean(totalQ10);
fprintf(1, 'Average Q (1-0012) = %s\n', avgQ10);
sigQ10 = std(totalQ10);
fprintf(1, '.    Standard Deviation (1-0012) = %s\n', sigQ10);
%
avgQ11 = mean(totalQ11);
fprintf(1, 'Average Q (6-0014) = %s\n', avgQ11);
sigQ11 = std(totalQ11);
fprintf(1, '.    Standard Deviation (6-0014) = %s\n', sigQ11);
%
avgQ12 = mean(totalQ12);
fprintf(1, 'Average Q (1-0015) = %s\n', avgQ12);
sigQ12 = std(totalQ12);
fprintf(1, '.    Standard Deviation (1-0015) = %s\n', sigQ12);
%
avgQ13 = mean(totalQ13);
fprintf(1, 'Average Q (7-0016) = %s\n', avgQ13);
sigQ13 = std(totalQ13);
fprintf(1, '.    Standard Deviation (7-0016) = %s\n', sigQ13);
%
avgQ14 = mean(totalQ14);
fprintf(1, 'Average Q (1-0017) = %s\n', avgQ14);
sigQ14 = std(totalQ14);
fprintf(1, '.    Standard Deviation (1-0017) = %s\n', sigQ14);
%
avgQ15 = mean(totalQ15);
fprintf(1, 'Average Q (8-0018) = %s\n', avgQ15);
sigQ15 = std(totalQ15);
fprintf(1, '.    Standard Deviation (8-0018) = %s\n', sigQ15);
%
avgQ16 = mean(totalQ16);
fprintf(1, 'Average Q (1-0019) = %s\n', avgQ16);
sigQ16 = std(totalQ16);
fprintf(1, '.    Standard Deviation (1-0019) = %s\n', sigQ16);
%
avgQ17 = mean(totalQ17);
fprintf(1, 'Average Q (9-0020) = %s\n', avgQ17);
sigQ17 = std(totalQ17);
fprintf(1, '.    Standard Deviation (9-0020) = %s\n', sigQ17);
%
avgQ18 = mean(totalQ18);
fprintf(1, 'Average Q (1-0022) = %s\n', avgQ18);
sigQ18 = std(totalQ18);
fprintf(1, '.    Standard Deviation (1-0022) = %s\n', sigQ18);
%
avgQ19 = mean(totalQ19);
fprintf(1, 'Average Q (10-0023) = %s\n', avgQ19);
sigQ19 = std(totalQ19);
fprintf(1, '.    Standard Deviation (10-0023) = %s\n', sigQ19);
%
avgQ20 = mean(totalQ20);
fprintf(1, 'Average Q (1-0024) = %s\n', avgQ20);
sigQ20 = std(totalQ20);
fprintf(1, '.    Standard Deviation (1-0024) = %s\n', sigQ20);
%
avgQ21 = mean(totalQ21);
fprintf(1, 'Average Q (11-0025) = %s\n', avgQ21);
sigQ21 = std(totalQ21);
fprintf(1, '.    Standard Deviation (11-0025) = %s\n', sigQ21);
%
avgQ22 = mean(totalQ22);
fprintf(1, 'Average Q (1-0026) = %s\n', avgQ22);
sigQ22 = std(totalQ22);
fprintf(1, '.    Standard Deviation (1-0026) = %s\n', sigQ22);
%
avgQ23 = mean(totalQ23);
fprintf(1, 'Average Q (12-0027) = %s\n', avgQ23);
sigQ23 = std(totalQ23);
fprintf(1, '.    Standard Deviation (12-0027) = %s\n', sigQ23);
%
avgQ24 = mean(totalQ24);
fprintf(1, 'Average Q (1-0028) = %s\n', avgQ24);
sigQ24 = std(totalQ24);
fprintf(1, '.    Standard Deviation (1-0028) = %s\n', sigQ24);
%
avgQ25 = mean(totalQ25);
fprintf(1, 'Average Q (13-0030) = %s\n', avgQ25);
sigQ25 = std(totalQ25);
fprintf(1, '.    Standard Deviation (13-0030) = %s\n', sigQ25);
%
avgQ26 = mean(totalQ26);
fprintf(1, 'Average Q (1-0031) = %s\n', avgQ26);
sigQ26 = std(totalQ26);
fprintf(1, '.    Standard Deviation (1-0031) = %s\n', sigQ26);
%
avgQ27 = mean(totalQ27);
fprintf(1, 'Average Q (14-0032) = %s\n', avgQ27);
sigQ27 = std(totalQ27);
fprintf(1, '.    Standard Deviation (14-0032) = %s\n', sigQ27);
%
avgQ28 = mean(totalQ28);
fprintf(1, 'Average Q (1-0033) = %s\n', avgQ28);
sigQ28 = std(totalQ28);
fprintf(1, '.    Standard Deviation (1-0033) = %s\n', sigQ28);
%
avgQ29 = mean(totalQ29);
fprintf(1, 'Average Q (1-0034) = %s\n', avgQ29);
sigQ29 = std(totalQ29);
fprintf(1, '.    Standard Deviation (1-0034) = %s\n', sigQ29);
%
avgQ30 = mean(totalQ30);
fprintf(1, 'Average Q (1-0035) = %s\n', avgQ30);
sigQ30 = std(totalQ30);
fprintf(1, '.    Standard Deviation (1-0035) = %s\n', sigQ30);
%
avgQ31 = mean(totalQ31);
fprintf(1, 'Average Q (1-0036) = %s\n', avgQ31);
sigQ31 = std(totalQ31);
fprintf(1, '.    Standard Deviation (1-0036) = %s\n', sigQ31);
%
avgQ32 = mean(totalQ32);
fprintf(1, 'Average Q (2-0037) = %s\n', avgQ32);
sigQ32 = std(totalQ32);
fprintf(1, '.    Standard Deviation (2-0037) = %s\n', sigQ32);
%
avgQ33 = mean(totalQ33);
fprintf(1, 'Average Q (1-0038) = %s\n', avgQ33);
sigQ33 = std(totalQ33);
fprintf(1, '.    Standard Deviation (1-0038) = %s\n', sigQ33);
%
avgQ34 = mean(totalQ34);
fprintf(1, 'Average Q (7-0039) = %s\n', avgQ34);
sigQ34 = std(totalQ34);
fprintf(1, '.    Standard Deviation (7-0039) = %s\n', sigQ34);
%
avgQ35 = mean(totalQ35);
fprintf(1, 'Average Q (1-0040) = %s\n', avgQ35);
sigQ35 = std(totalQ35);
fprintf(1, '.    Standard Deviation (1-0040) = %s\n', sigQ35);
%
avgQ36 = mean(totalQ36);
fprintf(1, 'Average Q (8-0041) = %s\n', avgQ36);
sigQ36 = std(totalQ36);
fprintf(1, '.    Standard Deviation (8-0041) = %s\n', sigQ36);
%
avgQ37 = mean(totalQ37);
fprintf(1, 'Average Q (1-0042) = %s\n', avgQ37);
sigQ37 = std(totalQ37);
fprintf(1, '.    Standard Deviation (1-0042) = %s\n', sigQ37);
%
avgQ38 = mean(totalQ38);
fprintf(1, 'Average Q (9-0043) = %s\n', avgQ38);
sigQ38 = std(totalQ38);
fprintf(1, '.    Standard Deviation (9-0043) = %s\n', sigQ38);
%
avgQ39 = mean(totalQ39);
fprintf(1, 'Average Q (1-0044) = %s\n', avgQ39);
sigQ39 = std(totalQ39);
fprintf(1, '.    Standard Deviation (1-0044) = %s\n', sigQ39);