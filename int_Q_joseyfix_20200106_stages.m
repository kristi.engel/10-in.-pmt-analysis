clear
%Case 1
maxidx = 1;
numnoise = 0;
myDir = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0001/'; %gets directory
myFiles = dir(fullfile(myDir,'*.csv')); %gets all txt files in struct
warning('off','all');
%figure1 = figure;
for k = 1:length(myFiles)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current time logcurrent
  baseFileName = myFiles(k).name;
  fullFileName = fullfile(myDir, baseFileName);
  fprintf(1, 'Now reading %s\n', fullFileName);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName);
      % Plot it just to make sure
        figure3 = figure;
        plot(m(:,1),m(:,2))
      numnoise = numnoise + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName);
               %probidx = k
            end
            time(j) = m(i,1)*(1e-9);
            current(j) = m(i,2);
            %logcurrent(j) = log10(m(i,2));
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ(k) = trapz(time,current);
        if totalQ(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName);
             totalQ(k)
             %probidx = k
        end

        %plot(time,current)
        %plot(time,logcurrent)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0005'});
        %xlabel({'time (s)'});
        %ylabel({'-current/A'});
        %ylabel({'log(-current/A)'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx1 = 1;
numnoise1 = 0;
myDir1 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0002/'; %gets directory
myFiles1 = dir(fullfile(myDir1,'*.csv')); %gets all txt files in struct
%figure3 = figure;
for k = 1:length(myFiles1)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current1 time1 tmpChannelA tmpTime units
  baseFileName1 = myFiles1(k).name;
  fullFileName1 = fullfile(myDir1, baseFileName1);
  fprintf(1, 'Now reading %s\n', fullFileName1);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName1);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName1);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName1);
      % Plot it just to make sure
        figure3 = figure;
        plot(m(:,1),m(:,2))
      numnoise1 = numnoise1 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx1 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx1
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx1:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx1 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName1);
               %probidx = k
            end
            time1(j) = m(i,1)*(1e-9);
            current1(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ1(k) = trapz(time1,current1);
        if totalQ1(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName1);
             totalQ1(k)
             %probidx = k
        end

        %plot(time1,current1)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0006'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx2 = 1;
numnoise2 = 0;
myDir2 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0003/'; %gets directory
myFiles2 = dir(fullfile(myDir2,'*.csv')); %gets all txt files in struct
%figure4 = figure;
for k = 1:length(myFiles2)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current2 time2 tmpChannelA tmpTime units
  baseFileName2 = myFiles2(k).name;
  fullFileName2 = fullfile(myDir2, baseFileName2);
  fprintf(1, 'Now reading %s\n', fullFileName2);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName2);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName2);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName2);
      % Plot it just to make sure
        figure4 = figure;
        plot(m(:,1),m(:,2))
      numnoise2 = numnoise2 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx2 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx2
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx2:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx2 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName2);
               %probidx = k
            end
            time2(j) = m(i,1)*(1e-9);
            current2(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ2(k) = trapz(time2,current2);
        if totalQ2(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName2);
             totalQ2(k)
             %probidx = k
        end

        %plot(time2,current2)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0007'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 2
maxidx3 = 1;
numnoise3 = 0;
myDir3 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0004/'; %gets directory
myFiles3 = dir(fullfile(myDir3,'*.csv')); %gets all txt files in struct
%figure5 = figure;
for k = 1:length(myFiles3)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current3 time3 tmpChannelA tmpTime units
  baseFileName3 = myFiles3(k).name;
  fullFileName3 = fullfile(myDir3, baseFileName3);
  fprintf(1, 'Now reading %s\n', fullFileName3);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName3);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName3);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName3);
      % Plot it just to make sure
        figure5 = figure;
        plot(m(:,1),m(:,2))
      numnoise3 = numnoise3 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx3 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx3
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx3:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx3 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName3);
               %probidx = k
            end
            time3(j) = m(i,1)*(1e-9);
            current3(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ3(k) = trapz(time3,current3);
        if totalQ3(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName3);
             totalQ3(k)
             %probidx = k
        end

        %plot(time3,current3)
        %title({'Current Wfm - Az.P 4, Track Case 2 - 20191202-0008'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx4 = 1;
numnoise4 = 0;
myDir4 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0005/'; %gets directory
myFiles4 = dir(fullfile(myDir4,'*.csv')); %gets all txt files in struct
%figure6 = figure;
for k = 1:length(myFiles4)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current4 time4 tmpChannelA tmpTime units
  baseFileName4 = myFiles4(k).name;
  fullFileName4 = fullfile(myDir4, baseFileName4);
  fprintf(1, 'Now reading %s\n', fullFileName4);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName4);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName4);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName4);
      % Plot it just to make sure
        figure6 = figure;
        plot(m(:,1),m(:,2))
      numnoise4 = numnoise4 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx4 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx4
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx4:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx4 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName4);
               %probidx = k
            end
            time4(j) = m(i,1)*(1e-9);
            current4(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ4(k) = trapz(time4,current4);
        if totalQ4(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName4);
             totalQ4(k)
             %probidx = k
        end

        %plot(time4,current4)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0009'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 3
maxidx5 = 1;
numnoise5 = 0;
myDir5 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0006/'; %gets directory
myFiles5 = dir(fullfile(myDir5,'*.csv')); %gets all txt files in struct
%figure7 = figure;
for k = 1:length(myFiles5)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current5 time5 tmpChannelA tmpTime units
  baseFileName5 = myFiles5(k).name;
  fullFileName5 = fullfile(myDir5, baseFileName5);
  fprintf(1, 'Now reading %s\n', fullFileName5);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName5);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName5);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName5);
      % Plot it just to make sure
        figure7 = figure;
        plot(m(:,1),m(:,2))
      numnoise5 = numnoise5 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx5 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx5
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx5:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx5 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName5);
               %probidx = k
            end
            time5(j) = m(i,1)*(1e-9);
            current5(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ5(k) = trapz(time5,current5);
        if totalQ5(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName5);
             totalQ5(k)
             %probidx = k
        end

        %plot(time5,current5)
        %title({'Current Wfm - Az.P 4, Track Case 3 - 20191202-0010'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx6 = 1;
numnoise6 = 0;
myDir6 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0007/'; %gets directory
myFiles6 = dir(fullfile(myDir6,'*.csv')); %gets all txt files in struct
%figure8 = figure;
for k = 1:length(myFiles6)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current6 time6 tmpChannelA tmpTime units
  baseFileName6 = myFiles6(k).name;
  fullFileName6 = fullfile(myDir6, baseFileName6);
  fprintf(1, 'Now reading %s\n', fullFileName6);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName6);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName6);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName6);
      % Plot it just to make sure
        figure8 = figure;
        plot(m(:,1),m(:,2))
      numnoise6 = numnoise6 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx6 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx6
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx6:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx6 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName6);
               %probidx = k
            end
            time6(j) = m(i,1)*(1e-9);
            current6(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ6(k) = trapz(time6,current6);
        if totalQ6(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName6);
             totalQ6(k)
             %probidx = k
        end

        %plot(time6,current6)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0011'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 4
maxidx7 = 1;
numnoise7 = 0;
myDir7 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0008/'; %gets directory
myFiles7 = dir(fullfile(myDir7,'*.csv')); %gets all txt files in struct
%figure9 = figure;
for k = 1:length(myFiles7)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current7 time7 tmpChannelA tmpTime units
  baseFileName7 = myFiles7(k).name;
  fullFileName7 = fullfile(myDir7, baseFileName7);
  fprintf(1, 'Now reading %s\n', fullFileName7);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName7);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName7);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName7);
      % Plot it just to make sure
        figure9 = figure;
        plot(m(:,1),m(:,2))
      numnoise7 = numnoise7 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx7 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx7
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx7:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx7 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName7);
               %probidx = k
            end
            time7(j) = m(i,1)*(1e-9);
            current7(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ7(k) = trapz(time7,current7);
        if totalQ7(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName7);
             totalQ7(k)
             %probidx = k
        end

        %plot(time7,current7)
        %title({'Current Wfm - Az.P 4, Track Case 4 - 20191202-0012'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx8 = 1;
numnoise8 = 0;
myDir8 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0009/'; %gets directory
myFiles8 = dir(fullfile(myDir8,'*.csv')); %gets all txt files in struct
%figure10 = figure;
for k = 1:length(myFiles8)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current8 time8 tmpChannelA tmpTime units
  baseFileName8 = myFiles8(k).name;
  fullFileName8 = fullfile(myDir8, baseFileName8);
  fprintf(1, 'Now reading %s\n', fullFileName8);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName8);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName8);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName8);
      % Plot it just to make sure
        figure10 = figure;
        plot(m(:,1),m(:,2))
      numnoise8 = numnoise8 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx8 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx8
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx8:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx8 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName8);
               %probidx = k
            end
            time8(j) = m(i,1)*(1e-9);
            current8(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ8(k) = trapz(time8,current8);
        if totalQ8(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName8);
             totalQ8(k)
             %probidx = k
        end

        %plot(time8,current8)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0013'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 5
maxidx9 = 1;
numnoise9 = 0;
myDir9 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0011/'; %gets directory
myFiles9 = dir(fullfile(myDir9,'*.csv')); %gets all txt files in struct
%figure11 = figure;
for k = 1:length(myFiles9)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current9 time9 tmpChannelA tmpTime units
  baseFileName9 = myFiles9(k).name;
  fullFileName9 = fullfile(myDir9, baseFileName9);
  fprintf(1, 'Now reading %s\n', fullFileName9);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName9);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName9);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName9);
      % Plot it just to make sure
        figure11 = figure;
        plot(m(:,1),m(:,2))
      numnoise9 = numnoise9 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx9 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx9
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx9:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx9 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName9);
               %probidx = k
            end
            time9(j) = m(i,1)*(1e-9);
            current9(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ9(k) = trapz(time9,current9);
        if totalQ9(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName9);
             totalQ9(k)
             %probidx = k
        end

        %plot(time9,current9)
        %title({'Current Wfm - Az.P 4, Track Case 5 - 20191202-0014'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx10 = 1;
numnoise10 = 0;
myDir10 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0012/'; %gets directory
myFiles10 = dir(fullfile(myDir10,'*.csv')); %gets all txt files in struct
%figure12 = figure;
for k = 1:length(myFiles10)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current10 time10 tmpChannelA tmpTime units
  baseFileName10 = myFiles10(k).name;
  fullFileName10 = fullfile(myDir10, baseFileName10);
  fprintf(1, 'Now reading %s\n', fullFileName10);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName10);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName10);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName10);
      % Plot it just to make sure
        figure12 = figure;
        plot(m(:,1),m(:,2))
      numnoise10 = numnoise10 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx10 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx10
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx10:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx10 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName10);
               %probidx = k
            end
            time10(j) = m(i,1)*(1e-9);
            current10(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ10(k) = trapz(time10,current10);
        if totalQ10(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName10);
             totalQ10(k)
             %probidx = k
        end

        %plot(time10,current10)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0015'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 6
maxidx11 = 1;
numnoise11 = 0;
myDir11 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0013/'; %gets directory
myFiles11 = dir(fullfile(myDir11,'*.csv')); %gets all txt files in struct
%figure13 = figure;
for k = 1:length(myFiles11)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current11 time11 tmpChannelA tmpTime units
  baseFileName11 = myFiles11(k).name;
  fullFileName11 = fullfile(myDir11, baseFileName11);
  fprintf(1, 'Now reading %s\n', fullFileName11);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName11);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName11);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName11);
      % Plot it just to make sure
        figure13 = figure;
        plot(m(:,1),m(:,2))
      numnois11 = numnoise11 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx11 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx11
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx11:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx11 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName11);
               %probidx = k
            end
            time11(j) = m(i,1)*(1e-9);
            current11(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ11(k) = trapz(time11,current11);
        if totalQ11(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName11);
             totalQ11(k)
             %probidx = k
        end

        %plot(time11,current11)
        %title({'Current Wfm - Az.P 4, Track Case 6 - 20191202-0016'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx12 = 1;
numnoise12 = 0;
myDir12 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0014/'; %gets directory
myFiles12 = dir(fullfile(myDir12,'*.csv')); %gets all txt files in struct
%figure14 = figure;
for k = 1:length(myFiles12)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current12 time12 tmpChannelA tmpTime units
  baseFileName12 = myFiles12(k).name;
  fullFileName12 = fullfile(myDir12, baseFileName12);
  fprintf(1, 'Now reading %s\n', fullFileName12);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName12);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName12);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName12);
      % Plot it just to make sure
        figure14 = figure;
        plot(m(:,1),m(:,2))
      numnoise12 = numnoise12 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx12 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx12
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx12:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx12 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName12);
               %probidx = k
            end
            time12(j) = m(i,1)*(1e-9);
            current12(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ12(k) = trapz(time12,current12);
        if totalQ12(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName12);
             totalQ12(k)
             %probidx = k
        end

        %plot(time12,current12)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0017'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 7
maxidx13 = 1;
numnoise13 = 0;
myDir13 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0015/'; %gets directory
myFiles13 = dir(fullfile(myDir13,'*.csv')); %gets all txt files in struct
%figure15 = figure;
for k = 1:length(myFiles13)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current13 time13 tmpChannelA tmpTime units
  baseFileName13 = myFiles13(k).name;
  fullFileName13 = fullfile(myDir13, baseFileName13);
  fprintf(1, 'Now reading %s\n', fullFileName13);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName13);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName13);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName13);
      % Plot it just to make sure
        figure15 = figure;
        plot(m(:,1),m(:,2))
      numnois13 = numnoise13 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx13 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx13
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx13:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx13 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName13);
               %probidx = k
            end
            time13(j) = m(i,1)*(1e-9);
            current13(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ13(k) = trapz(time13,current13);
        if totalQ13(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName13);
             totalQ13(k)
             %probidx = k
        end

        %plot(time13,current13)
        %title({'Current Wfm - Az.P 4, Track Case 7 - 20191202-0018'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx14 = 1;
numnoise14 = 0;
myDir14 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0016/'; %gets directory
myFiles14 = dir(fullfile(myDir14,'*.csv')); %gets all txt files in struct
%figure16 = figure;
for k = 1:length(myFiles14)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current14 time14 tmpChannelA tmpTime units
  baseFileName14 = myFiles14(k).name;
  fullFileName14 = fullfile(myDir14, baseFileName14);
  fprintf(1, 'Now reading %s\n', fullFileName14);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName14);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName14);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName14);
      % Plot it just to make sure
        figure16 = figure;
        plot(m(:,1),m(:,2))
      numnoise14 = numnoise14 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx14 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx14
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx14:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx14 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName14);
               %probidx = k
            end
            time14(j) = m(i,1)*(1e-9);
            current14(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ14(k) = trapz(time14,current14);
        if totalQ14(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName14);
             totalQ14(k)
             %probidx = k
        end

        %plot(time14,current14)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0019'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 8
maxidx15 = 1;
numnoise15 = 0;
myDir15 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0017/'; %gets directory
myFiles15 = dir(fullfile(myDir15,'*.csv')); %gets all txt files in struct
%figure17 = figure;
for k = 1:length(myFiles15)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current15 time15 tmpChannelA tmpTime units
  baseFileName15 = myFiles15(k).name;
  fullFileName15 = fullfile(myDir15, baseFileName15);
  fprintf(1, 'Now reading %s\n', fullFileName15);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName15);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName15);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName15);
      % Plot it just to make sure
        figure17 = figure;
        plot(m(:,1),m(:,2))
      numnois15 = numnoise15 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx15 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx15
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx15:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx15 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName15);
               %probidx = k
            end
            time15(j) = m(i,1)*(1e-9);
            current15(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ15(k) = trapz(time15,current15);
        if totalQ15(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName15);
             totalQ15(k)
             %probidx = k
        end

        %plot(time15,current15)
        %title({'Current Wfm - Az.P 4, Track Case 8 - 20191202-0020'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx16 = 1;
numnoise16 = 0;
myDir16 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0018/'; %gets directory
myFiles16 = dir(fullfile(myDir16,'*.csv')); %gets all txt files in struct
%figure18 = figure;
for k = 1:length(myFiles16)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current16 time16 tmpChannelA tmpTime units
  baseFileName16 = myFiles16(k).name;
  fullFileName16 = fullfile(myDir16, baseFileName16);
  fprintf(1, 'Now reading %s\n', fullFileName16);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName16);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName16);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName16);
      % Plot it just to make sure
        figure18 = figure;
        plot(m(:,1),m(:,2))
      numnoise16 = numnoise16 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx16 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx16
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx16:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx16 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName16);
               %probidx = k
            end
            time16(j) = m(i,1)*(1e-9);
            current16(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ16(k) = trapz(time16,current16);
        if totalQ16(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName16);
             totalQ16(k)
             %probidx = k
        end

        %plot(time16,current16)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0021'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 9
maxidx17 = 1;
numnoise17 = 0;
myDir17 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0019/'; %gets directory
myFiles17 = dir(fullfile(myDir17,'*.csv')); %gets all txt files in struct
%figure19 = figure;
for k = 1:length(myFiles17)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current17 time17 tmpChannelA tmpTime units
  baseFileName17 = myFiles17(k).name;
  fullFileName17 = fullfile(myDir17, baseFileName17);
  fprintf(1, 'Now reading %s\n', fullFileName17);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName17);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName17);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName17);
      % Plot it just to make sure
        figure19 = figure;
        plot(m(:,1),m(:,2))
      numnois17 = numnoise17 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx17 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx17
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx17:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx17 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName17);
               %probidx = k
            end
            time17(j) = m(i,1)*(1e-9);
            current17(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ17(k) = trapz(time17,current17);
        if totalQ17(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName17);
             totalQ17(k)
             %probidx = k
        end

        %plot(time17,current17)
        %title({'Current Wfm - Az.P 4, Track Case 9 - 20191202-0022'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx18 = 1;
numnoise18 = 0;
myDir18 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0021/'; %gets directory
myFiles18 = dir(fullfile(myDir18,'*.csv')); %gets all txt files in struct
%figure20 = figure;
for k = 1:length(myFiles18)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current18 time18 tmpChannelA tmpTime units
  baseFileName18 = myFiles18(k).name;
  fullFileName18 = fullfile(myDir18, baseFileName18);
  fprintf(1, 'Now reading %s\n', fullFileName18);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName18);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName18);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName18);
      % Plot it just to make sure
        figure20 = figure;
        plot(m(:,1),m(:,2))
      numnoise18 = numnoise18 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx18 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx18
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx18:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx18 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName18);
               %probidx = k
            end
            time18(j) = m(i,1)*(1e-9);
            current18(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ18(k) = trapz(time18,current18);
        if totalQ18(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName18);
             totalQ18(k)
             %probidx = k
        end

        %plot(time18,current18)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0023'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 10
maxidx19 = 1;
numnoise19 = 0;
myDir19 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0022/'; %gets directory
myFiles19 = dir(fullfile(myDir19,'*.csv')); %gets all txt files in struct
%figure21 = figure;
for k = 1:length(myFiles19)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current19 time19 tmpChannelA tmpTime units
  baseFileName19 = myFiles19(k).name;
  fullFileName19 = fullfile(myDir19, baseFileName19);
  fprintf(1, 'Now reading %s\n', fullFileName19);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName19);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName19);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName19);
      % Plot it just to make sure
        figure21 = figure;
        plot(m(:,1),m(:,2))
      numnois19 = numnoise19 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx19 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx19
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx19:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx19 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName19);
               %probidx = k
            end
            time19(j) = m(i,1)*(1e-9);
            current19(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ19(k) = trapz(time19,current19);
        if totalQ19(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName19);
             totalQ19(k)
             %probidx = k
        end

        %plot(time19,current19)
        %title({'Current Wfm - Az.P 4, Track Case 10 - 20191202-0024'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx20 = 1;
numnoise20 = 0;
myDir20 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0023/'; %gets directory
myFiles20 = dir(fullfile(myDir20,'*.csv')); %gets all txt files in struct
%figure22 = figure;
for k = 1:length(myFiles20)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current20 time20 tmpChannelA tmpTime units
  baseFileName20 = myFiles20(k).name;
  fullFileName20 = fullfile(myDir20, baseFileName20);
  fprintf(1, 'Now reading %s\n', fullFileName20);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName20);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName20);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName20);
      % Plot it just to make sure
        figure22 = figure;
        plot(m(:,1),m(:,2))
      numnoise20 = numnoise20 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx20 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx20
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx20:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx20 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName20);
               %probidx = k
            end
            time20(j) = m(i,1)*(1e-9);
            current20(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ20(k) = trapz(time20,current20);
        if totalQ20(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName20);
             totalQ20(k)
             %probidx = k
        end

        %plot(time20,current20)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0025'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 11
maxidx21 = 1;
numnoise21 = 0;
myDir21 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0024/'; %gets directory
myFiles21 = dir(fullfile(myDir21,'*.csv')); %gets all txt files in struct
%figure23 = figure;
for k = 1:length(myFiles21)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current21 time21 tmpChannelA tmpTime units
  baseFileName21 = myFiles21(k).name;
  fullFileName21 = fullfile(myDir21, baseFileName21);
  fprintf(1, 'Now reading %s\n', fullFileName21);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName21);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName21);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName21);
      % Plot it just to make sure
        figure23 = figure;
        plot(m(:,1),m(:,2))
      numnois21 = numnoise21 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx21 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx21
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx21:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx21 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName21);
               %probidx = k
            end
            time21(j) = m(i,1)*(1e-9);
            current21(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ21(k) = trapz(time21,current21);
        if totalQ21(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName21);
             totalQ21(k)
             %probidx = k
        end

        %plot(time21,current21)
        %title({'Current Wfm - Az.P 4, Track Case 11 - 20191202-0026'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx22 = 1;
numnoise22 = 0;
myDir22 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0025/'; %gets directory
myFiles22 = dir(fullfile(myDir22,'*.csv')); %gets all txt files in struct
%figure24 = figure;
for k = 1:length(myFiles22)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current22 time22 tmpChannelA tmpTime units
  baseFileName22 = myFiles22(k).name;
  fullFileName22 = fullfile(myDir22, baseFileName22);
  fprintf(1, 'Now reading %s\n', fullFileName22);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName22);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName22);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName22);
      % Plot it just to make sure
        figure24 = figure;
        plot(m(:,1),m(:,2))
      numnoise22 = numnoise22 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx22 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx22
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx22:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx22 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName22);
               %probidx = k
            end
            time22(j) = m(i,1)*(1e-9);
            current22(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ22(k) = trapz(time22,current22);
        if totalQ22(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName22);
             totalQ22(k)
             %probidx = k
        end

        %plot(time22,current22)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0027'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 12
maxidx23 = 1;
numnoise23 = 0;
myDir23 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0026/'; %gets directory
myFiles23 = dir(fullfile(myDir23,'*.csv')); %gets all txt files in struct
%figure25 = figure;
for k = 1:length(myFiles23)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current23 time23 tmpChannelA tmpTime units
  baseFileName23 = myFiles23(k).name;
  fullFileName23 = fullfile(myDir23, baseFileName23);
  fprintf(1, 'Now reading %s\n', fullFileName23);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName23);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName23);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName23);
      % Plot it just to make sure
        figure25 = figure;
        plot(m(:,1),m(:,2))
      numnois23 = numnoise23 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx23 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx23
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx23:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx23 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName23);
               %probidx = k
            end
            time23(j) = m(i,1)*(1e-9);
            current23(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ23(k) = trapz(time23,current23);
        if totalQ23(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName23);
             totalQ23(k)
             %probidx = k
        end

        %plot(time23,current23)
        %title({'Current Wfm - Az.P 4, Track Case 12 - 20191202-0028'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx24 = 1;
numnoise24 = 0;
myDir24 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0027/'; %gets directory
myFiles24 = dir(fullfile(myDir24,'*.csv')); %gets all txt files in struct
%figure26 = figure;
for k = 1:length(myFiles24)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current24 time24 tmpChannelA tmpTime units
  baseFileName24 = myFiles24(k).name;
  fullFileName24 = fullfile(myDir24, baseFileName24);
  fprintf(1, 'Now reading %s\n', fullFileName24);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName24);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName24);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName24);
      % Plot it just to make sure
        figure26 = figure;
        plot(m(:,1),m(:,2))
      numnoise24 = numnoise24 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx24 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx24
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx24:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx24 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName24);
               %probidx = k
            end
            time24(j) = m(i,1)*(1e-9);
            current24(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ24(k) = trapz(time24,current24);
        if totalQ24(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName24);
             totalQ24(k)
             %probidx = k
        end

        %plot(time24,current24)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0029'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 13
maxidx25 = 1;
numnoise25 = 0;
myDir25 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0028/'; %gets directory
myFiles25 = dir(fullfile(myDir25,'*.csv')); %gets all txt files in struct
%figure27 = figure;
for k = 1:length(myFiles25)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current25 time25 tmpChannelA tmpTime units
  baseFileName25 = myFiles25(k).name;
  fullFileName25 = fullfile(myDir25, baseFileName25);
  fprintf(1, 'Now reading %s\n', fullFileName25);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName25);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName25);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName25);
      % Plot it just to make sure
        figure27 = figure;
        plot(m(:,1),m(:,2))
      numnois25 = numnoise25 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx25 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx25
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx25:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx25 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName25);
               %probidx = k
            end
            time25(j) = m(i,1)*(1e-9);
            current25(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ25(k) = trapz(time25,current25);
        if totalQ25(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName25);
             totalQ25(k)
             %probidx = k
        end

        %plot(time25,current25)
        %title({'Current Wfm - Az.P 4, Track Case 13 - 20191202-0030'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx26 = 1;
numnoise26 = 0;
myDir26 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0029/'; %gets directory
myFiles26 = dir(fullfile(myDir26,'*.csv')); %gets all txt files in struct
%figure28 = figure;
for k = 1:length(myFiles26)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current26 time26 tmpChannelA tmpTime units
  baseFileName26 = myFiles26(k).name;
  fullFileName26 = fullfile(myDir26, baseFileName26);
  fprintf(1, 'Now reading %s\n', fullFileName26);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName26);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName26);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName26);
      % Plot it just to make sure
        figure28 = figure;
        plot(m(:,1),m(:,2))
      numnoise26 = numnoise26 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx26 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx26
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx26:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx26 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName26);
               %probidx = k
            end
            time26(j) = m(i,1)*(1e-9);
            current26(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ26(k) = trapz(time26,current26);
        if totalQ26(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName26);
             totalQ26(k)
             %probidx = k
        end

        %plot(time26,current26)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0031'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 14
maxidx27 = 1;
numnoise27 = 0;
myDir27 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0030/'; %gets directory
myFiles27 = dir(fullfile(myDir27,'*.csv')); %gets all txt files in struct
%figure29 = figure;
for k = 1:length(myFiles27)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current27 time27 tmpChannelA tmpTime units
  baseFileName27 = myFiles27(k).name;
  fullFileName27 = fullfile(myDir27, baseFileName27);
  fprintf(1, 'Now reading %s\n', fullFileName27);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName27);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName27);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName27);
      % Plot it just to make sure
        figure29 = figure;
        plot(m(:,1),m(:,2))
      numnois27 = numnoise27 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx27 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx27
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx27:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx27 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName27);
               %probidx = k
            end
            time27(j) = m(i,1)*(1e-9);
            current27(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ27(k) = trapz(time27,current27);
        if totalQ27(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName27);
             totalQ27(k)
             %probidx = k
        end

        %plot(time27,current27)
        %title({'Current Wfm - Az.P 4, Track Case 14 - 20191202-0032'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end

%----------------------------------------------------------
%Case 1
maxidx28 = 1;
numnoise28 = 0;
myDir28 = '/Users/kristiengel/Documents/MATLAB/20200106/20200106-0031/'; %gets directory
myFiles28 = dir(fullfile(myDir28,'*.csv')); %gets all txt files in struct
%figure30 = figure;
for k = 1:length(myFiles28)
    clear m mTest Test test max Max volts fiveper ldiff leftfiveper left rdiff ...
        right rightfiveper current28 time28 tmpChannelA tmpTime units
  baseFileName28 = myFiles28(k).name;
  fullFileName28 = fullfile(myDir28, baseFileName28);
  fprintf(1, 'Now reading %s\n', fullFileName28);
    % Read in the files
    % HI KRISTI
    % Here is my proposed solution - readtable reads the csv as a table,
    % and since it detects variable length character arays it puts each
    % column as a cell arrray of character arrays
    test = readtable(fullFileName28);
    
    % Because cell arrays are kinda annoying here I'm stripping off the
    % units, and converting the cell array into an array of doubles - once
    % for each column
    tmpChannelA = str2double(test.ChannelA(2:end));
    tmpTime = str2double(test.Time(2:end));
    % Now I just have to put these two columns back together
    mTest =[tmpTime,tmpChannelA] ;
    
    % Okay, now lets see what those units are
    units = test.ChannelA{1};
    % Why do we even need these silly parentheses
    units = strrep(strrep(units,'(',''),')','');
    
    %  Okay - lets see if that was volts
    if ~strcmpi(units,'V')
        % NOT VOLTS ?!!!????? 
        fprintf(1, 'File %s is not in Volts\n', fullFileName28);
        mTest(:,2)=(1e-3)*mTest(:,2); % Then make it volts
    end
    
    m=mTest; % I was never here - carry on
    % Make it positive
    m(:,2) = m(:,2)*(-1);
    % 50 ohm load; I = V/R
    m(:,2) = m(:,2)/(50);
    % Plot it just to make sure
    %figure1 = figure;
    %plot(m(:,1),m(:,2))
    % Find the max value
    Max = max(m);
    Max(k) = Max(2);
    if Max(k) < 0.0062 %0.0068 coinc = 2 value; 0.0000001 no coinc
      Max(2)
      fprintf(1, 'File %s is just noise\n', fullFileName28);
      % Plot it just to make sure
        figure30 = figure;
        plot(m(:,1),m(:,2))
      numnoise28 = numnoise28 + 1;
    %else
        %for i = 1:length(m)
         %volts(i) = m(i,2);
        %end
    end
    hold on
    maxidx28 = find(m(:,2) == Max(k),1,'last');
    fiveper = 0.05*max(m(:,2));
    
    % Finding left bound of integration
    for i = 1:maxidx28
        ldiff(i) = (m(i,2)-fiveper);
    end
    %leftfiveper = min(ldiff)
    %find(ldiff == leftfiveper)
    left = find(ldiff < 0,1,'last') + 1;
    % Finding right bound of integration
    j = 1;
    for i = maxidx28:length(m)
        rdiff(j) = (m(i,2)-fiveper);
        j = j + 1;
    end
    %rightfiveper = min(rdiff)
    %find(rdiff == rightfiveper)
    right = maxidx28 + find(rdiff <0,1) - 1;

    % Making an array just of the current values in the correct range
        j = 1;
        for i = left:right
            if m(i,2) < 0
               fprintf(1, 'File %s is problematic\n', fullFileName28);
               %probidx = k
            end
            time28(j) = m(i,1)*(1e-9);
            current28(j) = m(i,2);
            j = j + 1;
        end
        % Integrate under the curve to get the charge Q
        totalQ28(k) = trapz(time28,current28);
        if totalQ28(k) < 0
             fprintf(1, 'File %s is problematic\n', fullFileName28);
             totalQ28(k)
             %probidx = k
        end

        %plot(time28,current28)
        %title({'Current Wfm - Az.P 4, Track Case 1 - 20191202-0033'});
        %xlabel({'time (ns)'});
        %ylabel({'-current/A'});
        hold on
        
    
end


%------------------------------------------------------------------------------
%------------------------------------------------------------------------------

hold off
xmin = min(totalQ);
xmax = max(totalQ);
nbin = (xmax - xmin)/(50000000*(1.602e-19));
nbin = round(nbin);
%------------------------
%Step 1
figure2 = figure;
axes2 = axes('Parent',figure2);
hold(axes2,'on');
hist1 = histogram(totalQ,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',2);
set(hist1,'DisplayName','Case 1-0001, 1247h','EdgeColor',[0 0 0]);
hold on
%
title({'Q - Azimuthal Position 4, January 6th, 2020 - Coincidence = 2 (1)'},'FontSize', 20);
        xlabel({'Charge (C)'},'FontSize', 18);
        ylabel({'Counts (normalized)'},'FontSize', 18);
box(axes2,'on');
legend1 = legend(axes2,'show','FontSize', 14);
hold off
%------------------------
%Step 2
figure3 = figure;
axes3 = axes('Parent',figure3);
hold(axes3,'on');
hist2 = histogram(totalQ,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.0);
set(hist2,'DisplayName','Case 1-0001, 1247h','EdgeColor',[0 0 0]);
hold on
%
hist3 = histogram(totalQ1,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',2);
set(hist3,'DisplayName','Case 1-0002, 1249h','EdgeColor',[0 0 0]);
%
hist4 = histogram(totalQ2,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.5);
set(hist4,'DisplayName','Case 1-0003, 1253h','EdgeColor',[0 0 0]);
%
% Create title
title({'Q - Azimuthal Position 4, January 6th, 2020 - Coincidence = 2 (2)'},'FontSize', 20);
        xlabel({'Charge (C)'},'FontSize', 18);
        ylabel({'Counts (normalized)'},'FontSize', 18);
box(axes3,'on');
legend2 = legend(axes3,'show','FontSize', 14);
hold off
%------------------------
%Step 3
figure4 = figure;
axes4 = axes('Parent',figure4);
hold(axes4,'on');
hist5 = histogram(totalQ,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist5,'DisplayName','Case 1-0001, 1247h','EdgeColor',[0 0 0]);
hold on
%
hist5 = histogram(totalQ1,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist5,'DisplayName','Case 1-0002, 1249h','EdgeColor',[0 0 0]);
%
hist6 = histogram(totalQ2,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.0);
set(hist6,'DisplayName','Case 1-0003, 1253h','EdgeColor',[0 0 0]);
%
hist7 = histogram(totalQ3,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',2);
set(hist7,'DisplayName','Case 2-0004, 1258h');
%
hist8 = histogram(totalQ4,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.5);
set(hist8,'DisplayName','Case 1-0005, 1258h','EdgeColor',[0 0 0]);
%
% Create title
title({'Q - Azimuthal Position 4, January 6th, 2020 - Coincidence = 2 (3)'},'FontSize', 20);
        xlabel({'Charge (C)'},'FontSize', 18);
        ylabel({'Counts (normalized)'},'FontSize', 18);
box(axes4,'on');
legend3 = legend(axes4,'show','FontSize', 14);
hold off
%------------------------
%Step 4
figure5 = figure;
axes5 = axes('Parent',figure5);
hold(axes5,'on');
hist9 = histogram(totalQ,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist9,'DisplayName','Case 1-0001, 1247h','EdgeColor',[0 0 0]);
hold on
%
hist10 = histogram(totalQ1,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist10,'DisplayName','Case 1-0002, 1249h','EdgeColor',[0 0 0]);
%
hist11 = histogram(totalQ2,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist11,'DisplayName','Case 1-0003, 1253h','EdgeColor',[0 0 0]);
%
hist12 = histogram(totalQ3,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist12,'DisplayName','Case 2-0004, 1256h');
%
hist13 = histogram(totalQ4,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.0);
set(hist13,'DisplayName','Case 1-0005, 1258h','EdgeColor',[0 0 0]);
%
hist14 = histogram(totalQ5,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',2);
set(hist14,'DisplayName','Case 3-0006, 1300h');
%
hist15 = histogram(totalQ6,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.5);
set(hist15,'DisplayName','Case 1-0007, 1303h','EdgeColor',[0 0 0]);
%
% Create title
title({'Q - Azimuthal Position 4, January 6th, 2020 - Coincidence = 2 (4)'},'FontSize', 20);
        xlabel({'Charge (C)'},'FontSize', 18);
        ylabel({'Counts (normalized)'},'FontSize', 18);
box(axes5,'on');
legend4 = legend(axes5,'show','FontSize', 14);
hold off
%------------------------
%Step 5
figure6 = figure;
axes6 = axes('Parent',figure6);
hold(axes6,'on');
hist16 = histogram(totalQ,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist16,'DisplayName','Case 1-0001, 1247h','EdgeColor',[0 0 0]);
hold on
%
hist17 = histogram(totalQ1,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist17,'DisplayName','Case 1-0002, 1249h','EdgeColor',[0 0 0]);
%
hist18 = histogram(totalQ2,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist18,'DisplayName','Case 1-0003, 1253h','EdgeColor',[0 0 0]);
%
hist19 = histogram(totalQ3,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist19,'DisplayName','Case 2-0004, 1256h');
%
hist20 = histogram(totalQ4,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist20,'DisplayName','Case 1-0005, 1258h','EdgeColor',[0 0 0]);
%
hist21 = histogram(totalQ5,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist21,'DisplayName','Case 3-0006, 1300h');
%
hist22 = histogram(totalQ6,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.0);
set(hist22,'DisplayName','Case 1-0007, 1303h','EdgeColor',[0 0 0]);
%
hist23 = histogram(totalQ7,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',2);
set(hist23,'DisplayName','Case 4-0008, 1306');
%
hist24 = histogram(totalQ8,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.5);
set(hist24,'DisplayName','Case 1-0009, 13008h','EdgeColor',[0 0 0]);
%
% Create title
title({'Q - Azimuthal Position 4, January 6th, 2020 - Coincidence = 2 (5)'},'FontSize', 20);
        xlabel({'Charge (C)'},'FontSize', 18);
        ylabel({'Counts (normalized)'},'FontSize', 18);
box(axes6,'on');
legend5 = legend(axes6,'show','FontSize', 14);
hold off
%------------------------
%Step 6
figure7 = figure;
axes7 = axes('Parent',figure7);
hold(axes7,'on');
hist25 = histogram(totalQ,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist25,'DisplayName','Case 1-0001, 1247h','EdgeColor',[0 0 0]);
hold on
%
hist26 = histogram(totalQ1,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist26,'DisplayName','Case 1-0002, 1249h','EdgeColor',[0 0 0]);
%
hist27 = histogram(totalQ2,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist27,'DisplayName','Case 1-0003, 1253h','EdgeColor',[0 0 0]);
%
hist28 = histogram(totalQ3,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist28,'DisplayName','Case 2-0004, 1256h');
%
hist29 = histogram(totalQ4,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist29,'DisplayName','Case 1-0005, 1258h','EdgeColor',[0 0 0]);
%
hist30 = histogram(totalQ5,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist30,'DisplayName','Case 3-0006, 1300h');
%
hist31 = histogram(totalQ6,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist31,'DisplayName','Case 1-0007, 1303h','EdgeColor',[0 0 0]);
%
hist32 = histogram(totalQ7,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist32,'DisplayName','Case 4-0008, 1306h');
%
hist33 = histogram(totalQ8,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.0);
set(hist33,'DisplayName','Case 1-0009, 1308h','EdgeColor',[0 0 0]);
%
hist34 = histogram(totalQ9,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',2);
set(hist34,'DisplayName','Case 5-0011, 1312h');
%
hist35 = histogram(totalQ10,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.5);
set(hist35,'DisplayName','Case 1-0012, 1316h','EdgeColor',[0 0 0]);
%
% Create title
title({'Q - Azimuthal Position 4, January 6th, 2020 - Coincidence = 2 (6)'},'FontSize', 20);
        xlabel({'Charge (C)'},'FontSize', 18);
        ylabel({'Counts (normalized)'},'FontSize', 18);
box(axes7,'on');
legend6 = legend(axes7,'show','FontSize', 14);
hold off
%------------------------
%Step 7
figure8 = figure;
axes8 = axes('Parent',figure8);
hold(axes8,'on');
hist36 = histogram(totalQ,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist36,'DisplayName','Case 1-0001, 1247h','EdgeColor',[0 0 0]);
hold on
%
hist37 = histogram(totalQ1,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist37,'DisplayName','Case 1-0002, 1249h','EdgeColor',[0 0 0]);
%
hist38 = histogram(totalQ2,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist38,'DisplayName','Case 1-0003, 1253h','EdgeColor',[0 0 0]);
%
hist39 = histogram(totalQ3,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist39,'DisplayName','Case 2-0004, 1256h');
%
hist40 = histogram(totalQ4,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist40,'DisplayName','Case 1-0005, 1258h','EdgeColor',[0 0 0]);
%
hist41 = histogram(totalQ5,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist41,'DisplayName','Case 3-0006, 1300h');
%
hist42 = histogram(totalQ6,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist42,'DisplayName','Case 1-0007, 1303h','EdgeColor',[0 0 0]);
%
hist43 = histogram(totalQ7,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist43,'DisplayName','Case 4-0008, 1306h');
%
hist44 = histogram(totalQ8,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist44,'DisplayName','Case 1-0009, 1308h','EdgeColor',[0 0 0]);
%
hist45 = histogram(totalQ9,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist45,'DisplayName','Case 5-0011, 1312h');
%
hist46 = histogram(totalQ10,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.0);
set(hist46,'DisplayName','Case 1-0012, 1316h','EdgeColor',[0 0 0]);
%
hist47 = histogram(totalQ11,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',2);
set(hist47,'DisplayName','Case 6-0013, 1318h');
%
hist48 = histogram(totalQ12,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.5);
set(hist48,'DisplayName','Case 1-0014, 1321h','EdgeColor',[0 0 0]);
%
% Create title
title({'Q - Azimuthal Position 4, January 6th, 2020 - Coincidence = 2 (7)'},'FontSize', 20);
        xlabel({'Charge (C)'},'FontSize', 18);
        ylabel({'Counts (normalized)'},'FontSize', 18);
box(axes8,'on');
legend7 = legend(axes8,'show','FontSize', 14);
hold off
%------------------------
%Step 8
figure9 = figure;
axes9 = axes('Parent',figure9);
hold(axes9,'on');
hist49 = histogram(totalQ,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist49,'DisplayName','Case 1-0001, 1247h','EdgeColor',[0 0 0]);
hold on
%
hist50 = histogram(totalQ1,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist50,'DisplayName','Case 1-0002, 1249h','EdgeColor',[0 0 0]);
%
hist51 = histogram(totalQ2,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist51,'DisplayName','Case 1-0003, 1253h','EdgeColor',[0 0 0]);
%
hist52 = histogram(totalQ3,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist52,'DisplayName','Case 2-0004, 1256h');
%
hist53 = histogram(totalQ4,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist53,'DisplayName','Case 1-0005, 1258h','EdgeColor',[0 0 0]);
%
hist54 = histogram(totalQ5,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist54,'DisplayName','Case 3-0006, 1300h');
%
hist55 = histogram(totalQ6,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist55,'DisplayName','Case 1-0007, 1303h','EdgeColor',[0 0 0]);
%
hist56 = histogram(totalQ7,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist56,'DisplayName','Case 4-0008, 1306h');
%
hist57 = histogram(totalQ8,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist57,'DisplayName','Case 1-0009, 1308h','EdgeColor',[0 0 0]);
%
hist58 = histogram(totalQ9,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist58,'DisplayName','Case 5-0011, 1312h');
%
hist59 = histogram(totalQ10,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist59,'DisplayName','Case 1-0012, 1316h','EdgeColor',[0 0 0]);
%
hist60 = histogram(totalQ11,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist60,'DisplayName','Case 6-0013, 1318h');
%
hist61 = histogram(totalQ12,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.0);
set(hist61,'DisplayName','Case 1-0014, 1321h','EdgeColor',[0 0 0]);
%
hist62 = histogram(totalQ13,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',2);
set(hist62,'DisplayName','Case 7-0015, 1324h');
%
hist63 = histogram(totalQ14,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.5);
set(hist63,'DisplayName','Case 1-0016, 1326h','EdgeColor',[0 0 0]);
%
% Create title
title({'Q - Azimuthal Position 4, January 6th, 2020 - Coincidence = 2 (8)'},'FontSize', 20);
        xlabel({'Charge (C)'},'FontSize', 18);
        ylabel({'Counts (normalized)'},'FontSize', 18);
box(axes9,'on');
legend8 = legend(axes9,'show','FontSize', 14);
hold off
%------------------------
%Step 9
figure10 = figure;
axes10 = axes('Parent',figure10);
hold(axes10,'on');
hist64 = histogram(totalQ,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist64,'DisplayName','Case 1-0001, 1247h','EdgeColor',[0 0 0]);
hold on
%
hist65 = histogram(totalQ1,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist65,'DisplayName','Case 1-0002, 1249h','EdgeColor',[0 0 0]);
%
hist66 = histogram(totalQ2,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist66,'DisplayName','Case 1-0003, 1253h','EdgeColor',[0 0 0]);
%
hist67 = histogram(totalQ3,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist67,'DisplayName','Case 2-0004, 1256h');
%
hist68 = histogram(totalQ4,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist68,'DisplayName','Case 1-0005, 1258h','EdgeColor',[0 0 0]);
%
hist69 = histogram(totalQ5,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist69,'DisplayName','Case 3-0006, 1300h');
%
hist70 = histogram(totalQ6,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist70,'DisplayName','Case 1-0007, 1303h','EdgeColor',[0 0 0]);
%
hist71 = histogram(totalQ7,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist71,'DisplayName','Case 4-0008, 1306h');
%
hist72 = histogram(totalQ8,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist72,'DisplayName','Case 1-0009, 1308h','EdgeColor',[0 0 0]);
%
hist73 = histogram(totalQ9,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist73,'DisplayName','Case 5-0011, 1312h');
%
hist74 = histogram(totalQ10,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist74,'DisplayName','Case 1-0012, 1316h','EdgeColor',[0 0 0]);
%
hist75 = histogram(totalQ11,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist75,'DisplayName','Case 6-0013, 1318h');
%
hist76 = histogram(totalQ12,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist76,'DisplayName','Case 1-0014, 1321h','EdgeColor',[0 0 0]);
%
hist77 = histogram(totalQ13,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist77,'DisplayName','Case 7-0015, 1324h');
%
hist78 = histogram(totalQ14,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.0);
set(hist78,'DisplayName','Case 1-0016, 1326h','EdgeColor',[0 0 0]);
%
hist79 = histogram(totalQ15,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',2);
set(hist79,'DisplayName','Case 8-0017, 1329h');
%
hist80 = histogram(totalQ16,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.5);
set(hist80,'DisplayName','Case 1-0018, 1331h','EdgeColor',[0 0 0]);
%
% Create title
title({'Q - Azimuthal Position 4, January 6th, 2020 - Coincidence = 2 (9)'},'FontSize', 20);
        xlabel({'Charge (C)'},'FontSize', 18);
        ylabel({'Counts (normalized)'},'FontSize', 18);
box(axes10,'on');
legend9 = legend(axes10,'show','FontSize', 14);
hold off
%------------------------
%Step 10
figure11 = figure;
axes11 = axes('Parent',figure11);
hold(axes11,'on');
hist81 = histogram(totalQ,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist81,'DisplayName','Case 1-0001, 1247h','EdgeColor',[0 0 0]);
hold on
%
hist82 = histogram(totalQ1,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist82,'DisplayName','Case 1-0002, 1249h','EdgeColor',[0 0 0]);
%
hist83 = histogram(totalQ2,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist83,'DisplayName','Case 1-0003, 1253h','EdgeColor',[0 0 0]);
%
hist84 = histogram(totalQ3,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist84,'DisplayName','Case 2-0004, 1256h');
%
hist85 = histogram(totalQ4,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist85,'DisplayName','Case 1-0005, 1258h','EdgeColor',[0 0 0]);
%
hist86 = histogram(totalQ5,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist86,'DisplayName','Case 3-0006, 1300h');
%
hist87 = histogram(totalQ6,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist87,'DisplayName','Case 1-0007, 1303h','EdgeColor',[0 0 0]);
%
hist88 = histogram(totalQ7,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist88,'DisplayName','Case 4-0008, 1306h');
%
hist89 = histogram(totalQ8,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist89,'DisplayName','Case 1-0009, 1308h','EdgeColor',[0 0 0]);
%
hist90 = histogram(totalQ9,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist90,'DisplayName','Case 5-0011, 1312h');
%
hist91 = histogram(totalQ10,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist91,'DisplayName','Case 1-0012, 1316h','EdgeColor',[0 0 0]);
%
hist92 = histogram(totalQ11,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist92,'DisplayName','Case 6-0013, 1318h');
%
hist93 = histogram(totalQ12,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist93,'DisplayName','Case 1-0014, 1321h','EdgeColor',[0 0 0]);
%
hist94 = histogram(totalQ13,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist94,'DisplayName','Case 7-0015, 1324h');
%
hist95 = histogram(totalQ14,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist95,'DisplayName','Case 1-0016, 1326h','EdgeColor',[0 0 0]);
%
hist96 = histogram(totalQ15,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist96,'DisplayName','Case 8-0017, 1329h');
%
hist97 = histogram(totalQ16,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.0);
set(hist97,'DisplayName','Case 1-0018, 1331h','EdgeColor',[0 0 0]);
%
hist98 = histogram(totalQ17,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',2);
set(hist98,'DisplayName','Case 9-0019, 1334h');
%
hist99 = histogram(totalQ18,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.5);
set(hist99,'DisplayName','Case 1-0021, 1338h','EdgeColor',[0 0 0]);
%
% Create title
title({'Q - Azimuthal Position 4, January 6th, 2020 - Coincidence = 2 (10)'},'FontSize', 20);
        xlabel({'Charge (C)'},'FontSize', 18);
        ylabel({'Counts (normalized)'},'FontSize', 18);
box(axes11,'on');
legend10 = legend(axes11,'show','FontSize', 14);
hold off
%------------------------
%Step 11
figure12 = figure;
axes12 = axes('Parent',figure12);
hold(axes12,'on');
hist100 = histogram(totalQ,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist100,'DisplayName','Case 1-0001, 1247h','EdgeColor',[0 0 0]);
hold on
%
hist101 = histogram(totalQ1,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist101,'DisplayName','Case 1-0002, 1249h','EdgeColor',[0 0 0]);
%
hist102 = histogram(totalQ2,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist102,'DisplayName','Case 1-0003, 1253h','EdgeColor',[0 0 0]);
%
hist103 = histogram(totalQ3,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist103,'DisplayName','Case 2-0004, 1256h');
%
hist104 = histogram(totalQ4,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist104,'DisplayName','Case 1-0005, 1258h','EdgeColor',[0 0 0]);
%
hist105 = histogram(totalQ5,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist105,'DisplayName','Case 3-0006, 1300h');
%
hist106 = histogram(totalQ6,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist106,'DisplayName','Case 1-0007, 1303h','EdgeColor',[0 0 0]);
%
hist107 = histogram(totalQ7,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist107,'DisplayName','Case 4-0008, 1306h');
%
hist108 = histogram(totalQ8,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist108,'DisplayName','Case 1-0009, 1308h','EdgeColor',[0 0 0]);
%
hist109 = histogram(totalQ9,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist109,'DisplayName','Case 5-0011, 1312h');
%
hist110 = histogram(totalQ10,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist110,'DisplayName','Case 1-0012, 1316h','EdgeColor',[0 0 0]);
%
hist111 = histogram(totalQ11,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist111,'DisplayName','Case 6-0013, 1318h');
%
hist112 = histogram(totalQ12,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist112,'DisplayName','Case 1-0014, 1321h','EdgeColor',[0 0 0]);
%
hist113 = histogram(totalQ13,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist113,'DisplayName','Case 7-0015, 1324h');
%
hist114 = histogram(totalQ14,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist114,'DisplayName','Case 1-0016, 1326h','EdgeColor',[0 0 0]);
%
hist115 = histogram(totalQ15,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist115,'DisplayName','Case 8-0017, 1329h');
%
hist116 = histogram(totalQ16,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist116,'DisplayName','Case 1-0018, 1331h','EdgeColor',[0 0 0]);
%
hist117 = histogram(totalQ17,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist117,'DisplayName','Case 9-0019, 1334h');
%
hist118 = histogram(totalQ18,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.0);
set(hist118,'DisplayName','Case 1-0021, 1338h','EdgeColor',[0 0 0]);
%
hist119 = histogram(totalQ19,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',2);
set(hist119,'DisplayName','Case 10-0022, 1341h');
%
hist120 = histogram(totalQ20,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.5);
set(hist120,'DisplayName','Case 1-0023, 1345h','EdgeColor',[0 0 0]);
%
% Create title
title({'Q - Azimuthal Position 4, January 6th, 2020 - Coincidence = 2 (11)'},'FontSize', 20);
        xlabel({'Charge (C)'},'FontSize', 18);
        ylabel({'Counts (normalized)'},'FontSize', 18);
box(axes12,'on');
legend11 = legend(axes12,'show','FontSize', 14);
hold off
%------------------------
%Step 12
figure13 = figure;
axes13 = axes('Parent',figure13);
hold(axes13,'on');
hist121 = histogram(totalQ,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist121,'DisplayName','Case 1-0001, 1247h','EdgeColor',[0 0 0]);
hold on
%
hist122 = histogram(totalQ1,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist122,'DisplayName','Case 1-0002, 1249h','EdgeColor',[0 0 0]);
%
hist123 = histogram(totalQ2,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist123,'DisplayName','Case 1-0003, 1253h','EdgeColor',[0 0 0]);
%
hist124 = histogram(totalQ3,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist124,'DisplayName','Case 2-0004, 1256h');
%
hist125 = histogram(totalQ4,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist125,'DisplayName','Case 1-0005, 1258h','EdgeColor',[0 0 0]);
%
hist126 = histogram(totalQ5,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist126,'DisplayName','Case 3-0006, 1300h');
%
hist127 = histogram(totalQ6,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist127,'DisplayName','Case 1-0007, 1303h','EdgeColor',[0 0 0]);
%
hist128 = histogram(totalQ7,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist128,'DisplayName','Case 4-0008, 1306h');
%
hist129 = histogram(totalQ8,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist129,'DisplayName','Case 1-0009, 1308h','EdgeColor',[0 0 0]);
%
hist130 = histogram(totalQ9,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist130,'DisplayName','Case 5-0011, 1312h');
%
hist131 = histogram(totalQ10,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist131,'DisplayName','Case 1-0012, 1316h','EdgeColor',[0 0 0]);
%
hist132 = histogram(totalQ11,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist132,'DisplayName','Case 6-0013, 1318h');
%
hist133 = histogram(totalQ12,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist133,'DisplayName','Case 1-0014, 1321h','EdgeColor',[0 0 0]);
%
hist134 = histogram(totalQ13,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist134,'DisplayName','Case 7-0015, 1324h');
%
hist135 = histogram(totalQ14,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist135,'DisplayName','Case 1-0016, 1326h','EdgeColor',[0 0 0]);
%
hist136 = histogram(totalQ15,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist136,'DisplayName','Case 8-0017, 1329h');
%
hist137 = histogram(totalQ16,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist137,'DisplayName','Case 1-0018, 1331h','EdgeColor',[0 0 0]);
%
hist138 = histogram(totalQ17,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist138,'DisplayName','Case 9-0019, 1334h');
%
hist139 = histogram(totalQ18,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist139,'DisplayName','Case 1-0021, 1338h','EdgeColor',[0 0 0]);
%
hist140 = histogram(totalQ19,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist140,'DisplayName','Case 10-0022, 1341h');
%
hist141 = histogram(totalQ20,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.0);
set(hist141,'DisplayName','Case 1-0023, 1345h','EdgeColor',[0 0 0]);
%
hist142 = histogram(totalQ21,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',2);
set(hist142,'DisplayName','Case 11-0024, 1347h');
%
hist143 = histogram(totalQ22,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.5);
set(hist143,'DisplayName','Case 1-0025, 1351h','EdgeColor',[0 0 0]);
%
% Create title
title({'Q - Azimuthal Position 4, January 6th, 2020 - Coincidence = 2 (12)'},'FontSize', 20);
        xlabel({'Charge (C)'},'FontSize', 18);
        ylabel({'Counts (normalized)'},'FontSize', 18);
box(axes13,'on');
legend12 = legend(axes13,'show','FontSize', 14);
hold off
%------------------------
%Step 13
figure14 = figure;
axes14 = axes('Parent',figure14);
hold(axes14,'on');
hist144 = histogram(totalQ,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist144,'DisplayName','Case 1-0001, 1247h','EdgeColor',[0 0 0]);
hold on
%
hist145 = histogram(totalQ1,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist145,'DisplayName','Case 1-0002, 1249h','EdgeColor',[0 0 0]);
%
hist146 = histogram(totalQ2,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist146,'DisplayName','Case 1-0003, 1253h','EdgeColor',[0 0 0]);
%
hist147 = histogram(totalQ3,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist147,'DisplayName','Case 2-0004, 1256h');
%
hist148 = histogram(totalQ4,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist148,'DisplayName','Case 1-0005, 1258h','EdgeColor',[0 0 0]);
%
hist149 = histogram(totalQ5,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist149,'DisplayName','Case 3-0006, 1300h');
%
hist150 = histogram(totalQ6,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist150,'DisplayName','Case 1-0007, 1303h','EdgeColor',[0 0 0]);
%
hist151 = histogram(totalQ7,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist151,'DisplayName','Case 4-0008, 1306h');
%
hist152 = histogram(totalQ8,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist152,'DisplayName','Case 1-0009, 1308h','EdgeColor',[0 0 0]);
%
hist153 = histogram(totalQ9,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist153,'DisplayName','Case 5-0011, 1312h');
%
hist154 = histogram(totalQ10,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist154,'DisplayName','Case 1-0012, 1316h','EdgeColor',[0 0 0]);
%
hist155 = histogram(totalQ11,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist155,'DisplayName','Case 6-0013, 1318h');
%
hist156 = histogram(totalQ12,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist156,'DisplayName','Case 1-0014, 1321h','EdgeColor',[0 0 0]);
%
hist157 = histogram(totalQ13,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist157,'DisplayName','Case 7-0015, 1324h');
%
hist158 = histogram(totalQ14,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist158,'DisplayName','Case 1-0016, 1326h','EdgeColor',[0 0 0]);
%
hist159 = histogram(totalQ15,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist159,'DisplayName','Case 8-0017, 1329h');
%
hist160 = histogram(totalQ16,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist160,'DisplayName','Case 1-0018, 1331h','EdgeColor',[0 0 0]);
%
hist161 = histogram(totalQ17,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist161,'DisplayName','Case 9-0019, 1334h');
%
hist162 = histogram(totalQ18,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist162,'DisplayName','Case 1-0021, 1338h','EdgeColor',[0 0 0]);
%
hist163 = histogram(totalQ19,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist163,'DisplayName','Case 10-0022, 1341h');
%
hist164 = histogram(totalQ20,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist164,'DisplayName','Case 1-0023, 1345h','EdgeColor',[0 0 0]);
%
hist165 = histogram(totalQ21,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist165,'DisplayName','Case 11-0024, 1347h');
%
hist166 = histogram(totalQ22,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.0);
set(hist166,'DisplayName','Case 1-0025, 1351h','EdgeColor',[0 0 0]);
%
hist167 = histogram(totalQ23,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',2);
set(hist167,'DisplayName','Case 12-0026, 1356h');
%
hist168 = histogram(totalQ24,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.5);
set(hist168,'DisplayName','Case 1-0027, 1400h','EdgeColor',[0 0 0]);
%
% Create title
title({'Q - Azimuthal Position 4, January 6th, 2020 - Coincidence = 2 (13)'},'FontSize', 20);
        xlabel({'Charge (C)'},'FontSize', 18);
        ylabel({'Counts (normalized)'},'FontSize', 18);
box(axes14,'on');
legend13 = legend(axes14,'show','FontSize', 14);
hold off
%------------------------
%Step 14
figure15 = figure;
axes15 = axes('Parent',figure15);
hold(axes15,'on');
hist169 = histogram(totalQ,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist169,'DisplayName','Case 1-0001, 1247h','EdgeColor',[0 0 0]);
hold on
%
hist170 = histogram(totalQ1,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist170,'DisplayName','Case 1-0002, 1249h','EdgeColor',[0 0 0]);
%
hist171 = histogram(totalQ2,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist171,'DisplayName','Case 1-0003, 1253h','EdgeColor',[0 0 0]);
%
hist172 = histogram(totalQ3,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist172,'DisplayName','Case 2-0004, 1256h');
%
hist173 = histogram(totalQ4,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist173,'DisplayName','Case 1-0005, 1258h','EdgeColor',[0 0 0]);
%
hist174 = histogram(totalQ5,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist174,'DisplayName','Case 3-0006, 1300h');
%
hist175 = histogram(totalQ6,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist175,'DisplayName','Case 1-0007, 1303h','EdgeColor',[0 0 0]);
%
hist176 = histogram(totalQ7,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist176,'DisplayName','Case 4-0008, 1306h');
%
hist177 = histogram(totalQ8,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist177,'DisplayName','Case 1-0009, 1308h','EdgeColor',[0 0 0]);
%
hist178 = histogram(totalQ9,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist178,'DisplayName','Case 5-0011, 1312h');
%
hist179 = histogram(totalQ10,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist179,'DisplayName','Case 1-0012, 1316h','EdgeColor',[0 0 0]);
%
hist180 = histogram(totalQ11,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist180,'DisplayName','Case 6-0013, 1318h');
%
hist181 = histogram(totalQ12,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist181,'DisplayName','Case 1-0014, 1321h','EdgeColor',[0 0 0]);
%
hist182 = histogram(totalQ13,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist182,'DisplayName','Case 7-0015, 1324h');
%
hist183 = histogram(totalQ14,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist183,'DisplayName','Case 1-0016, 1326h','EdgeColor',[0 0 0]);
%
hist184 = histogram(totalQ15,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist184,'DisplayName','Case 8-0017, 1329h');
%
hist185 = histogram(totalQ16,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist185,'DisplayName','Case 1-0018, 1331h','EdgeColor',[0 0 0]);
%
hist186 = histogram(totalQ17,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist186,'DisplayName','Case 9-0019, 1334h');
%
hist187 = histogram(totalQ18,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist187,'DisplayName','Case 1-0021, 1338h','EdgeColor',[0 0 0]);
%
hist188 = histogram(totalQ19,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist188,'DisplayName','Case 10-0022, 1341h');
%
hist189 = histogram(totalQ20,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist189,'DisplayName','Case 1-0023, 1345h','EdgeColor',[0 0 0]);
%
hist190 = histogram(totalQ21,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist190,'DisplayName','Case 11-0024, 1347h');
%
hist191 = histogram(totalQ22,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist191,'DisplayName','Case 1-0025, 1351h','EdgeColor',[0 0 0]);
%
hist192 = histogram(totalQ23,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist192,'DisplayName','Case 12-0026, 1356h');
%
hist193 = histogram(totalQ24,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.0);
set(hist193,'DisplayName','Case 1-0027, 1400h','EdgeColor',[0 0 0]);
%
hist194 = histogram(totalQ25,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',2);
set(hist194,'DisplayName','Case 13-0028, 1402h');
%
hist195 = histogram(totalQ26,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.5);
set(hist195,'DisplayName','Case 1-0029, 1416h','EdgeColor',[0 0 0]);
%
% Create title
title({'Q - Azimuthal Position 4, January 6th, 2020 - Coincidence = 2 (14)'},'FontSize', 20);
        xlabel({'Charge (C)'},'FontSize', 18);
        ylabel({'Counts (normalized)'},'FontSize', 18);
box(axes15,'on');
legend14 = legend(axes15,'show','FontSize', 14);
hold off
%------------------------
%Step 15
figure16 = figure;
axes16 = axes('Parent',figure16);
hold(axes16,'on');
hist196 = histogram(totalQ,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist196,'DisplayName','Case 1-0001, 1247h','EdgeColor',[0 0 0]);
hold on
%
hist197 = histogram(totalQ1,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist197,'DisplayName','Case 1-0002, 1249h','EdgeColor',[0 0 0]);
%
hist198 = histogram(totalQ2,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist198,'DisplayName','Case 1-0003, 1253h','EdgeColor',[0 0 0]);
%
hist199 = histogram(totalQ3,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist199,'DisplayName','Case 2-0004, 1256h');
%
hist200 = histogram(totalQ4,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist200,'DisplayName','Case 1-0005, 1258h','EdgeColor',[0 0 0]);
%
hist201 = histogram(totalQ5,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist201,'DisplayName','Case 3-0006, 1300h');
%
hist202 = histogram(totalQ6,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist202,'DisplayName','Case 1-0007, 1303h','EdgeColor',[0 0 0]);
%
hist203 = histogram(totalQ7,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist203,'DisplayName','Case 4-0008, 1306h');
%
hist204 = histogram(totalQ8,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist204,'DisplayName','Case 1-0009, 1308h','EdgeColor',[0 0 0]);
%
hist205 = histogram(totalQ9,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist205,'DisplayName','Case 5-0011, 1312h');
%
hist206 = histogram(totalQ10,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist206,'DisplayName','Case 1-0012, 1316h','EdgeColor',[0 0 0]);
%
hist207 = histogram(totalQ11,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist207,'DisplayName','Case 6-0013, 1318h');
%
hist208 = histogram(totalQ12,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist208,'DisplayName','Case 1-0014, 1321h','EdgeColor',[0 0 0]);
%
hist209 = histogram(totalQ13,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist209,'DisplayName','Case 7-0015, 1324h');
%
hist210 = histogram(totalQ14,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist210,'DisplayName','Case 1-0016, 1326h','EdgeColor',[0 0 0]);
%
hist211 = histogram(totalQ15,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist211,'DisplayName','Case 8-0017, 1329h');
%
hist212 = histogram(totalQ16,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist212,'DisplayName','Case 1-0018, 1331h','EdgeColor',[0 0 0]);
%
hist213 = histogram(totalQ17,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist213,'DisplayName','Case 9-0019, 1334h');
%
hist214 = histogram(totalQ18,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist214,'DisplayName','Case 1-0021, 1338h','EdgeColor',[0 0 0]);
%
hist215 = histogram(totalQ19,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist215,'DisplayName','Case 10-0022, 1341h');
%
hist216 = histogram(totalQ20,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist216,'DisplayName','Case 1-0023, 1345h','EdgeColor',[0 0 0]);
%
hist217 = histogram(totalQ21,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist217,'DisplayName','Case 11-0024, 1347h');
%
hist218 = histogram(totalQ22,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist218,'DisplayName','Case 1-0025, 1351h','EdgeColor',[0 0 0]);
%
hist219 = histogram(totalQ23,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist219,'DisplayName','Case 12-0026, 1356h');
%
hist220 = histogram(totalQ24,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist220,'DisplayName','Case 1-0027, 1400h','EdgeColor',[0 0 0]);
%
hist221 = histogram(totalQ25,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',0.5);
set(hist221,'DisplayName','Case 13-0028, 1402h');
%
hist222 = histogram(totalQ26,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.0);
set(hist222,'DisplayName','Case 1-0029, 1416h','EdgeColor',[0 0 0]);
%
hist223 = histogram(totalQ27,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',2);
set(hist223,'DisplayName','Case 14-0030, 1419h');
%
hist224 = histogram(totalQ28,nbin,'Normalization','probability',...
    'DisplayStyle','stairs','LineWidth',1.5);
set(hist224,'DisplayName','Case 1-0031, 1431h','EdgeColor',[0 0 0]);
%
% Create title
title({'Q - Azimuthal Position 4, January 6th, 2020 - Coincidence = 2 (15)'},'FontSize', 20);
        xlabel({'Charge (C)'},'FontSize', 18);
        ylabel({'Counts (normalized)'},'FontSize', 18);
box(axes16,'on');
legend15 = legend(axes16,'show','FontSize', 14);