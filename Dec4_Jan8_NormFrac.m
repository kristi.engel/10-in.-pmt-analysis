%% 02 December - 08 Jan Test Results
% PMT in Azimuthal position 4

%% Relative Fractional Counts w/ Error

%Positions from center; half inch code
Xh = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5];
%Positions from center; half inch code - Bob run
Xhb = [0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5];
%Positions from center; half inch code - incomplete run
Xhi = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5];
%Positions from center; quarter inch code
Xq = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 3.75, 4, 4.25, 4.5, 4.75, 5,...
    5.25, 5.5, 5.75, 6, 6.25, 6.5, 6.75];

%Normalized Fraction; Half Inch Run #1 12/02
Y1 = [0.965073529, 0.966911765, 0.971530249, 0.954725275, 0.864791289,...
    0.921028466, 0.950160037, 0.905777778, 0.78313253, 0.64957265,...
    0.510822511, 0.335546702, 0.064826982, 0.052816901];
err1 = [0.036263397, 0.036309168, 0.035836046, 0.035213327, 0.033527128,...
    0.035145966, 0.035801133, 0.034201942, 0.030624324, 0.02712026,...
    0.02356335, 0.018625702, 0.007657143, 0.006908081];
%Normalized Fraction; Half Inch Run #2 12/03
Y2 = [1.002375297, 1.025641026, 0.985146143, 0.966618287, 0.857685009,...
    0.895676692, 0.950071327, 0.907863, 0.804619827, 0.645727483,...
    0.514563107, 0.327808471, 0.052785924, 0.052955083];
err2 = [0.037811301, 0.038746828, 0.037538067, 0.037246726, 0.03409854,...
    0.034911207, 0.036506881, 0.035685968, 0.03295411, 0.028091067,...
    0.023895749, 0.018743634, 0.007277431, 0.007169492];
%Normalized Fraction; Half Inch Run #3 12/03
Y3 = [0.98935631, 0.998457584, 0.94535809, 0.877922078, 0.778958017,...
    0.917948718, 0.921630094, 0.819502075, 0.799363057, 0.65179543,...
    0.553714591, 0.310454065, 0.061278394, 0.060956385];
err3 = [0.038716997, 0.039233221, 0.038433643, 0.036228649, 0.033089841,...
    0.03706216, 0.03750766, 0.034618458, 0.034463644, 0.030665702,...
    0.02749111, 0.019460651, 0.008168583, 0.008125023];
%Normalized Fraction; Half Inch Run #4 12/04
Y4 = [0.96794129, 0.968602826, 0.952936601, 0.949272513, 0.856006364,...
    0.845522096, 0.920436817, 0.935394372, 0.853010164, 0.631578947,...
    0.579341317, 0.319205804, 0.072837633, 0.062854979];
err4 = [0.033310882, 0.033593032, 0.033083239, 0.033180255, 0.03118426,...
    0.030674543, 0.032378885, 0.032989215, 0.030844554, 0.024996615,...
    0.023648491, 0.016812693, 0.007568117, 0.007006804];
%Normalized Fraction; Half Inch Run #5 12/06
Y5 = [0.98710538, 0.9754135, 0.957033017, 0.897092755, 0.84821024,...
    0.92513369, 0.916780355, 0.91136974, 0.795149415, 0.756989247,...
    0.855578371, 0.392286501, 0.067843866, 0.052150046];
err5 = [0.036208648, 0.036019282, 0.035776444, 0.03463143, 0.033085417,...
    0.034726713, 0.034871602, 0.034463105, 0.031025224, 0.033497,...
    0.040893269, 0.022738927, 0.00807408, 0.006996919];
%Normalized Fraction; Half Inch Run #6 01/06
Y6 = [0.990274841, 1.009230769, 0.976744186, 0.948402948, 0.902419984,...
    0.887264332, 0.940038685, 0.908326967, 0.800465839, 0.63069269,...
    0.562996595, 0.337980566, 0.074684772, 0.064578313];
err6 = [0.03538489, 0.034177187, 0.033853295, 0.033838962, 0.031973849,...
    0.031395445, 0.032697844, 0.031765661, 0.029499429, 0.025198475,...
    0.023365674, 0.018271205, 0.008668576, 0.008015857];
%Normalized Fraction; Half Inch Run #7 01/06
Y7 = [1.021402214, 0.96207285, 1.001235076, 0.892112421, 0.81993418,...
    0.911058824, 0.883742911, 0.843853821, 0.773859897, 0.62972973,...
    0.568276911, 0.272653722];
err7 = [0.033745668, 0.03271269, 0.035172584, 0.034199096, 0.032970477,...
    0.035328015, 0.03470431, 0.033748539, 0.031767986, 0.027312168,...
    0.023561647, 0.01583245];
%Normalized Fraction; Half Inch Run #8 01/07
Y8 = [0.972672065, 0.924889543, 0.938916256, 0.94515633, 0.903160041,...
    0.927226463, 0.914947157, 0.850596659, 0.783910196, 0.694939759,...
    0.509766556, 0.341346154, 0.067426872, 0.066074951];
err8 = [0.038252802, 0.036442164, 0.036868801, 0.037772642, 0.036556814,...
    0.037165484, 0.036636537, 0.034020266, 0.031948974, 0.030042707,...
    0.02468858, 0.019601922, 0.008313399, 0.008204601];
%Normalized Fraction; Half Inch Run #9 01/08
Y9 = [0.969565217, 1.052585064, 0.96373057, 0.88579795, 0.899823217,...
    0.952023586, 0.931154567, 0.93818571, 0.892125135, 0.761076379,...
    0.740186916, 0.387699494, 0.094898323, 0.077452668];
err9 = [0.035381075, 0.037680808, 0.035117848, 0.030588625, 0.027727355,...
    0.0274455, 0.027039662, 0.027159544, 0.026378589, 0.023899479,...
    0.025136701, 0.018982587, 0.008421689, 0.006793914];

%Averaged Normalized Fraction; Azimuthal Position 4 (Prior Tests; 06/06/19)
Yavg4 = [0.981802648, 1.028865794, 0.997276748, 0.934058818, 0.907278947,...
    0.982333064, 0.948667327, 0.939313907, 0.847033942, 0.717111256,...
    0.575783076, 0.393563998, 0.065332997, 0.060880174];
errAvg4 = [0.018029324, 0.018582973, 0.018141091, 0.017155722, 0.01685918,...
    0.017858786, 0.017355145, 0.017262031, 0.016119061, 0.014480761,...
    0.012556997, 0.009977517, 0.003795995, 0.003656637];


% Create figure
figure1 = figure;
% Create axes
axes1 = axes('Parent',figure1);
hold(axes1,'on');
set(gca,'FontSize',18)
%
EBh1 = errorbar(Xh,Y1,err1,'LineWidth',1.5);
set(EBh1,'DisplayName','1/2" Test #1 12/02/19 Poissonian','Color',...
    [0.00 0.45 0.74]);
%
%EBh2 = errorbar(Xh,Y2,err2,'LineWidth',1.5);
%set(EBh2,'DisplayName','1/2" Test #2 12/03/19 Poissonian','Color',...
 %   [0.32 0.55 0.02]);
%
%EBh3 = errorbar(Xh,Y3,err3,'LineWidth',1.5);
%set(EBh3,'DisplayName','1/2" Test #3 12/03/19 Poissonian','Color',...
 %   [0.32 0.55 0.02]);
%
%EBh4 = errorbar(Xh,Y4,err4,'LineWidth',1.5);
%set(EBh4,'DisplayName','1/2" Test #4 12/04/19 Poissonian','Color',...
 %   [0.32 0.55 0.02]);
%
%EBh5 = errorbar(Xh,Y5,err5,'LineWidth',1.5);
%set(EBh5,'DisplayName','1/2" Test #5 12/06/19 NON-POISSONIAN','Color',...
 %   [1 0 0]);
%
%EBh6 = errorbar(Xh,Y6,err6,'LineWidth',1.5);
%set(EBh6,'DisplayName','1/2" Test #6 01/06/20 NON-POISSONIAN','Color',...
 %   [1 0 0]);
%
%EBh7 = errorbar(Xhi,Y7,err7,'LineWidth',1.5);
%set(EBh7,'DisplayName','1/2" Test #7 01/06/20 NON-POISSONIAN','Color',...
 %   [1 0 0]);
%
%EBh8 = errorbar(Xh,Y8,err8,'LineWidth',1.5);
%set(EBh8,'DisplayName','1/2" Test #8 01/07/20 Poissonian','Color',...
 %   [0.32 0.55 0.02]);
%
%EBh9 = errorbar(Xh,Y9,err9,'LineWidth',1.5);
%set(EBh9,'DisplayName','1/2" Test #9 01/08/20 NON-POISSSONIAN','Color',...
 %   [1 0 0]);
%
EBavg2 = errorbar(Xh,Yavg4,errAvg4,'LineWidth',2);
set(EBavg2,'DisplayName','Avg. Azimuthal Pos. 4 06/06/19','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of coinc. rate to rate at center'},'FontSize', 20);
% Create xlabel
xlabel({'Distance From Center of 10" PMT (inches)'},'FontSize', 20);
% Create title
title({'10" PMT Testing - Dec 02 Result w/ Prior Az4 Average'},'FontSize', 28);
box(axes1,'on');
% Set the remaining axes properties
set(axes1,'XGrid','on','XTick',...
    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7],'YGrid','on','YTick',...
    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2]);
% Create legend
% Create legend
legend1 = legend(axes1,'show','FontSize', 18);
set(legend1,...
    'location','southwest');

%----------------------------------------

% Create figure
figure2 = figure;
% Create axes
axes2 = axes('Parent',figure2);
hold(axes2,'on');
set(gca,'FontSize',18)
%
%EBh1 = errorbar(Xh,Y1,err1,'LineWidth',1.5);
%set(EBh1,'DisplayName','1/2" Test #1 12/02/19 Poissonian','Color',...
 %   [0.00 0.45 0.74]);
%
EBh2 = errorbar(Xh,Y2,err2,'LineWidth',1.5);
set(EBh2,'DisplayName','1/2" Test #2 12/03/19 Poissonian','Color',...
    [0.85 0.33 0.10]);
%
%EBh3 = errorbar(Xh,Y3,err3,'LineWidth',1.5);
%set(EBh3,'DisplayName','1/2" Test #3 12/03/19 Poissonian','Color',...
 %   [0.93 0.69 0.13]);
%
%EBh4 = errorbar(Xh,Y4,err4,'LineWidth',1.5);
%set(EBh4,'DisplayName','1/2" Test #4 12/04/19 Poissonian','Color',...
 %   [0.32 0.55 0.02]);
%
%EBh5 = errorbar(Xh,Y5,err5,'LineWidth',1.5);
%set(EBh5,'DisplayName','1/2" Test #5 12/06/19 NON-POISSONIAN','Color',...
 %   [1 0 0]);
%
%EBh6 = errorbar(Xh,Y6,err6,'LineWidth',1.5);
%set(EBh6,'DisplayName','1/2" Test #6 01/06/20 NON-POISSONIAN','Color',...
 %   [1 0 0]);
%
%EBh7 = errorbar(Xhi,Y7,err7,'LineWidth',1.5);
%set(EBh7,'DisplayName','1/2" Test #7 01/06/20 NON-POISSONIAN','Color',...
 %   [1 0 0]);
%
%EBh8 = errorbar(Xh,Y8,err8,'LineWidth',1.5);
%set(EBh8,'DisplayName','1/2" Test #8 01/07/20 Poissonian','Color',...
 %   [0.32 0.55 0.02]);
%
%EBh9 = errorbar(Xh,Y9,err9,'LineWidth',1.5);
%set(EBh9,'DisplayName','1/2" Test #9 01/08/20 NON-POISSSONIAN','Color',...
 %   [1 0 0]);
%
EBavg2 = errorbar(Xh,Yavg4,errAvg4,'LineWidth',2);
set(EBavg2,'DisplayName','Avg. Azimuthal Pos. 4 06/06/19','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of coinc. rate to rate at center'},'FontSize', 20);
% Create xlabel
xlabel({'Distance From Center of 10" PMT (inches)'},'FontSize', 20);
% Create title
title({'10" PMT Testing - Dec 03 Result #1 w/ Prior Az4 Average'},'FontSize', 28);
box(axes2,'on');
% Set the remaining axes properties
set(axes2,'XGrid','on','XTick',...
    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7],'YGrid','on','YTick',...
    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2]);
% Create legend
% Create legend
legend2 = legend(axes2,'show','FontSize', 18);
set(legend2,...
    'location','southwest');

%----------------------------------------

% Create figure
figure3 = figure;
% Create axes
axes3 = axes('Parent',figure3);
hold(axes3,'on');
set(gca,'FontSize',18)
%
%EBh1 = errorbar(Xh,Y1,err1,'LineWidth',1.5);
%set(EBh1,'DisplayName','1/2" Test #1 12/02/19 Poissonian','Color',...
 %   [0.00 0.45 0.74]);
%
%EBh2 = errorbar(Xh,Y2,err2,'LineWidth',1.5);
%set(EBh2,'DisplayName','1/2" Test #2 12/03/19 Poissonian','Color',...
 %   [0.85 0.33 0.10]);
%
EBh3 = errorbar(Xh,Y3,err3,'LineWidth',1.5);
set(EBh3,'DisplayName','1/2" Test #3 12/03/19 Poissonian','Color',...
    [0.93 0.69 0.13]);
%
%EBh4 = errorbar(Xh,Y4,err4,'LineWidth',1.5);
%set(EBh4,'DisplayName','1/2" Test #4 12/04/19 Poissonian','Color',...
 %   [0.32 0.55 0.02]);
%
%EBh5 = errorbar(Xh,Y5,err5,'LineWidth',1.5);
%set(EBh5,'DisplayName','1/2" Test #5 12/06/19 NON-POISSONIAN','Color',...
 %   [1 0 0]);
%
%EBh6 = errorbar(Xh,Y6,err6,'LineWidth',1.5);
%set(EBh6,'DisplayName','1/2" Test #6 01/06/20 NON-POISSONIAN','Color',...
 %   [1 0 0]);
%
%EBh7 = errorbar(Xhi,Y7,err7,'LineWidth',1.5);
%set(EBh7,'DisplayName','1/2" Test #7 01/06/20 NON-POISSONIAN','Color',...
 %   [1 0 0]);
%
%EBh8 = errorbar(Xh,Y8,err8,'LineWidth',1.5);
%set(EBh8,'DisplayName','1/2" Test #8 01/07/20 Poissonian','Color',...
 %   [0.32 0.55 0.02]);
%
%EBh9 = errorbar(Xh,Y9,err9,'LineWidth',1.5);
%set(EBh9,'DisplayName','1/2" Test #9 01/08/20 NON-POISSSONIAN','Color',...
 %   [1 0 0]);
%
EBavg2 = errorbar(Xh,Yavg4,errAvg4,'LineWidth',2);
set(EBavg2,'DisplayName','Avg. Azimuthal Pos. 4 06/06/19','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of coinc. rate to rate at center'},'FontSize', 20);
% Create xlabel
xlabel({'Distance From Center of 10" PMT (inches)'},'FontSize', 20);
% Create title
title({'10" PMT Testing - Dec 03 Result #2 w/ Prior Az4 Average'},'FontSize', 28);
box(axes3,'on');
% Set the remaining axes properties
set(axes3,'XGrid','on','XTick',...
    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7],'YGrid','on','YTick',...
    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2]);
% Create legend
% Create legend
legend3 = legend(axes3,'show','FontSize', 18);
set(legend3,...
    'location','southwest');

%------------------------

% Create figure
figure4 = figure;
% Create axes
axes4 = axes('Parent',figure4);
hold(axes4,'on');
set(gca,'FontSize',18)
%
EBh1 = errorbar(Xh,Y1,err1,'LineWidth',1.5);
set(EBh1,'DisplayName','1/2" Test #1 12/02/19 Poissonian','Color',...
    [0.00 0.45 0.74]);
%
EBh2 = errorbar(Xh,Y2,err2,'LineWidth',1.5);
set(EBh2,'DisplayName','1/2" Test #2 12/03/19 Poissonian','Color',...
    [0.85 0.33 0.10]);
%
EBh3 = errorbar(Xh,Y3,err3,'LineWidth',1.5);
set(EBh3,'DisplayName','1/2" Test #3 12/03/19 Poissonian','Color',...
    [0.93 0.69 0.13]);
%
%EBh4 = errorbar(Xh,Y4,err4,'LineWidth',1.5);
%set(EBh4,'DisplayName','1/2" Test #4 12/04/19 Poissonian','Color',...
 %   [0.32 0.55 0.02]);
%
%EBh5 = errorbar(Xh,Y5,err5,'LineWidth',1.5);
%set(EBh5,'DisplayName','1/2" Test #5 12/06/19 NON-POISSONIAN','Color',...
 %   [1 0 0]);
%
%EBh6 = errorbar(Xh,Y6,err6,'LineWidth',1.5);
%set(EBh6,'DisplayName','1/2" Test #6 01/06/20 NON-POISSONIAN','Color',...
 %   [1 0 0]);
%
%EBh7 = errorbar(Xhi,Y7,err7,'LineWidth',1.5);
%set(EBh7,'DisplayName','1/2" Test #7 01/06/20 NON-POISSONIAN','Color',...
 %   [1 0 0]);
%
%EBh8 = errorbar(Xh,Y8,err8,'LineWidth',1.5);
%set(EBh8,'DisplayName','1/2" Test #8 01/07/20 Poissonian','Color',...
 %   [0.32 0.55 0.02]);
%
%EBh9 = errorbar(Xh,Y9,err9,'LineWidth',1.5);
%set(EBh9,'DisplayName','1/2" Test #9 01/08/20 NON-POISSSONIAN','Color',...
 %   [1 0 0]);
%
EBavg2 = errorbar(Xh,Yavg4,errAvg4,'LineWidth',2);
set(EBavg2,'DisplayName','Avg. Azimuthal Pos. 4 06/06/19','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of coinc. rate to rate at center'},'FontSize', 20);
% Create xlabel
xlabel({'Distance From Center of 10" PMT (inches)'},'FontSize', 20);
% Create title
title({'10" PMT Testing - Dec 02--03 Results w/ Prior Az4 Average'},'FontSize', 28);
box(axes4,'on');
% Set the remaining axes properties
set(axes4,'XGrid','on','XTick',...
    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7],'YGrid','on','YTick',...
    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2]);
% Create legend
% Create legend
legend4 = legend(axes4,'show','FontSize', 18);
set(legend4,...
    'location','southwest');

%------------------------

% Create figure
figure5 = figure;
% Create axes
axes5 = axes('Parent',figure5);
hold(axes5,'on');
set(gca,'FontSize',18)
%
%EBh1 = errorbar(Xh,Y1,err1,'LineWidth',1.5);
%set(EBh1,'DisplayName','1/2" Test #1 12/02/19 Poissonian','Color',...
 %   [0.00 0.45 0.74]);
%
%EBh2 = errorbar(Xh,Y2,err2,'LineWidth',1.5);
%set(EBh2,'DisplayName','1/2" Test #2 12/03/19 Poissonian','Color',...
 %   [0.85 0.33 0.10]);
%
%EBh3 = errorbar(Xh,Y3,err3,'LineWidth',1.5);
%set(EBh3,'DisplayName','1/2" Test #3 12/03/19 Poissonian','Color',...
 %   [0.93 0.69 0.13]);
%
EBh4 = errorbar(Xh,Y4,err4,'LineWidth',1.5);
set(EBh4,'DisplayName','1/2" Test #4 12/04/19 Poissonian','Color',...
    [0.49 0.18 0.56]);
%
%EBh5 = errorbar(Xh,Y5,err5,'LineWidth',1.5);
%set(EBh5,'DisplayName','1/2" Test #5 12/06/19 NON-POISSONIAN','Color',...
 %   [1 0 0]);
%
%EBh6 = errorbar(Xh,Y6,err6,'LineWidth',1.5);
%set(EBh6,'DisplayName','1/2" Test #6 01/06/20 NON-POISSONIAN','Color',...
 %   [1 0 0]);
%
%EBh7 = errorbar(Xhi,Y7,err7,'LineWidth',1.5);
%set(EBh7,'DisplayName','1/2" Test #7 01/06/20 NON-POISSONIAN','Color',...
 %   [1 0 0]);
%
%EBh8 = errorbar(Xh,Y8,err8,'LineWidth',1.5);
%set(EBh8,'DisplayName','1/2" Test #8 01/07/20 Poissonian','Color',...
 %   [0.32 0.55 0.02]);
%
%EBh9 = errorbar(Xh,Y9,err9,'LineWidth',1.5);
%set(EBh9,'DisplayName','1/2" Test #9 01/08/20 NON-POISSSONIAN','Color',...
 %   [1 0 0]);
%
EBavg2 = errorbar(Xh,Yavg4,errAvg4,'LineWidth',2);
set(EBavg2,'DisplayName','Avg. Azimuthal Pos. 4 06/06/19','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of coinc. rate to rate at center'},'FontSize', 20);
% Create xlabel
xlabel({'Distance From Center of 10" PMT (inches)'},'FontSize', 20);
% Create title
title({'10" PMT Testing - Dec 04 Result w/ Prior Az4 Average'},'FontSize', 28);
box(axes5,'on');
% Set the remaining axes properties
set(axes5,'XGrid','on','XTick',...
    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7],'YGrid','on','YTick',...
    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2]);
% Create legend
% Create legend
legend5 = legend(axes5,'show','FontSize', 18);
set(legend5,...
    'location','southwest');

%------------------------

% Create figure
figure6 = figure;
% Create axes
axes6 = axes('Parent',figure6);
hold(axes6,'on');
set(gca,'FontSize',18)
%
EBh1 = errorbar(Xh,Y1,err1,'LineWidth',1.5);
set(EBh1,'DisplayName','1/2" Test #1 12/02/19 Poissonian','Color',...
    [0.00 0.45 0.74]);
%
EBh2 = errorbar(Xh,Y2,err2,'LineWidth',1.5);
set(EBh2,'DisplayName','1/2" Test #2 12/03/19 Poissonian','Color',...
    [0.85 0.33 0.10]);
%
EBh3 = errorbar(Xh,Y3,err3,'LineWidth',1.5);
set(EBh3,'DisplayName','1/2" Test #3 12/03/19 Poissonian','Color',...
    [0.93 0.69 0.13]);
%
EBh4 = errorbar(Xh,Y4,err4,'LineWidth',1.5);
set(EBh4,'DisplayName','1/2" Test #4 12/04/19 Poissonian','Color',...
    [0.49 0.18 0.56]);
%
%EBh5 = errorbar(Xh,Y5,err5,'LineWidth',1.5);
%set(EBh5,'DisplayName','1/2" Test #5 12/06/19 NON-POISSONIAN','Color',...
 %   [1 0 0]);
%
%EBh6 = errorbar(Xh,Y6,err6,'LineWidth',1.5);
%set(EBh6,'DisplayName','1/2" Test #6 01/06/20 NON-POISSONIAN','Color',...
 %   [1 0 0]);
%
%EBh7 = errorbar(Xhi,Y7,err7,'LineWidth',1.5);
%set(EBh7,'DisplayName','1/2" Test #7 01/06/20 NON-POISSONIAN','Color',...
 %   [1 0 0]);
%
%EBh8 = errorbar(Xh,Y8,err8,'LineWidth',1.5);
%set(EBh8,'DisplayName','1/2" Test #8 01/07/20 Poissonian','Color',...
 %   [0.32 0.55 0.02]);
%
%EBh9 = errorbar(Xh,Y9,err9,'LineWidth',1.5);
%set(EBh9,'DisplayName','1/2" Test #9 01/08/20 NON-POISSSONIAN','Color',...
 %   [1 0 0]);
%
EBavg2 = errorbar(Xh,Yavg4,errAvg4,'LineWidth',2);
set(EBavg2,'DisplayName','Avg. Azimuthal Pos. 4 06/06/19','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of coinc. rate to rate at center'},'FontSize', 20);
% Create xlabel
xlabel({'Distance From Center of 10" PMT (inches)'},'FontSize', 20);
% Create title
title({'10" PMT Testing - Dec 02--04 Results w/ Prior Az4 Average'},'FontSize', 28);
box(axes6,'on');
% Set the remaining axes properties
set(axes6,'XGrid','on','XTick',...
    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7],'YGrid','on','YTick',...
    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2]);
% Create legend
% Create legend
legend6 = legend(axes6,'show','FontSize', 18);
set(legend6,...
    'location','southwest');

%------------------------

% Create figure
figure7 = figure;
% Create axes
axes7 = axes('Parent',figure7);
hold(axes7,'on');
set(gca,'FontSize',18)
%
%EBh1 = errorbar(Xh,Y1,err1,'LineWidth',1.5);
%set(EBh1,'DisplayName','1/2" Test #1 12/02/19 Poissonian','Color',...
 %   [0.00 0.45 0.74]);
%
%EBh2 = errorbar(Xh,Y2,err2,'LineWidth',1.5);
%set(EBh2,'DisplayName','1/2" Test #2 12/03/19 Poissonian','Color',...
 %   [0.85 0.33 0.10]);
%
%EBh3 = errorbar(Xh,Y3,err3,'LineWidth',1.5);
%set(EBh3,'DisplayName','1/2" Test #3 12/03/19 Poissonian','Color',...
 %   [0.93 0.69 0.13]);
%
%EBh4 = errorbar(Xh,Y4,err4,'LineWidth',1.5);
%set(EBh4,'DisplayName','1/2" Test #4 12/04/19 Poissonian','Color',...
 %   [0.49 0.18 0.56]);
%
EBh5 = errorbar(Xh,Y5,err5,'LineWidth',1.5);
set(EBh5,'DisplayName','1/2" Test #5 12/06/19 NON-POISSONIAN','Color',...
    [0.47 0.67 0.19]);
%
%EBh6 = errorbar(Xh,Y6,err6,'LineWidth',1.5);
%set(EBh6,'DisplayName','1/2" Test #6 01/06/20 NON-POISSONIAN','Color',...
 %   [1 0 0]);
%
%EBh7 = errorbar(Xhi,Y7,err7,'LineWidth',1.5);
%set(EBh7,'DisplayName','1/2" Test #7 01/06/20 NON-POISSONIAN','Color',...
 %   [1 0 0]);
%
%EBh8 = errorbar(Xh,Y8,err8,'LineWidth',1.5);
%set(EBh8,'DisplayName','1/2" Test #8 01/07/20 Poissonian','Color',...
 %   [0.32 0.55 0.02]);
%
%EBh9 = errorbar(Xh,Y9,err9,'LineWidth',1.5);
%set(EBh9,'DisplayName','1/2" Test #9 01/08/20 NON-POISSSONIAN','Color',...
 %   [1 0 0]);
%
EBavg2 = errorbar(Xh,Yavg4,errAvg4,'LineWidth',2);
set(EBavg2,'DisplayName','Avg. Azimuthal Pos. 4 06/06/19','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of coinc. rate to rate at center'},'FontSize', 20);
% Create xlabel
xlabel({'Distance From Center of 10" PMT (inches)'},'FontSize', 20);
% Create title
title({'10" PMT Testing - Dec 06 Result w/ Prior Az4 Average'},'FontSize', 28);
box(axes7,'on');
% Set the remaining axes properties
set(axes7,'XGrid','on','XTick',...
    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7],'YGrid','on','YTick',...
    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2]);
% Create legend
% Create legend
legend7 = legend(axes7,'show','FontSize', 18);
set(legend7,...
    'location','southwest');

%------------------------

% Create figure
figure8 = figure;
% Create axes
axes8 = axes('Parent',figure8);
hold(axes8,'on');
set(gca,'FontSize',18)
%
EBh1 = errorbar(Xh,Y1,err1,'LineWidth',1.5);
set(EBh1,'DisplayName','1/2" Test #1 12/02/19 Poissonian','Color',...
    [0.00 0.45 0.74]);
%
EBh2 = errorbar(Xh,Y2,err2,'LineWidth',1.5);
set(EBh2,'DisplayName','1/2" Test #2 12/03/19 Poissonian','Color',...
    [0.85 0.33 0.10]);
%
EBh3 = errorbar(Xh,Y3,err3,'LineWidth',1.5);
set(EBh3,'DisplayName','1/2" Test #3 12/03/19 Poissonian','Color',...
    [0.93 0.69 0.13]);
%
EBh4 = errorbar(Xh,Y4,err4,'LineWidth',1.5);
set(EBh4,'DisplayName','1/2" Test #4 12/04/19 Poissonian','Color',...
    [0.49 0.18 0.56]);
%
EBh5 = errorbar(Xh,Y5,err5,'LineWidth',1.5);
set(EBh5,'DisplayName','1/2" Test #5 12/06/19 NON-POISSONIAN','Color',...
    [0.47 0.67 0.19]);
%
%EBh6 = errorbar(Xh,Y6,err6,'LineWidth',1.5);
%set(EBh6,'DisplayName','1/2" Test #6 01/06/20 NON-POISSONIAN','Color',...
 %   [1 0 0]);
%
%EBh7 = errorbar(Xhi,Y7,err7,'LineWidth',1.5);
%set(EBh7,'DisplayName','1/2" Test #7 01/06/20 NON-POISSONIAN','Color',...
 %   [1 0 0]);
%
%EBh8 = errorbar(Xh,Y8,err8,'LineWidth',1.5);
%set(EBh8,'DisplayName','1/2" Test #8 01/07/20 Poissonian','Color',...
 %   [0.32 0.55 0.02]);
%
%EBh9 = errorbar(Xh,Y9,err9,'LineWidth',1.5);
%set(EBh9,'DisplayName','1/2" Test #9 01/08/20 NON-POISSSONIAN','Color',...
 %   [1 0 0]);
%
EBavg2 = errorbar(Xh,Yavg4,errAvg4,'LineWidth',2);
set(EBavg2,'DisplayName','Avg. Azimuthal Pos. 4 06/06/19','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of coinc. rate to rate at center'},'FontSize', 20);
% Create xlabel
xlabel({'Distance From Center of 10" PMT (inches)'},'FontSize', 20);
% Create title
title({'10" PMT Testing - Dec 02--06 Results w/ Prior Az4 Average'},'FontSize', 28);
box(axes8,'on');
% Set the remaining axes properties
set(axes8,'XGrid','on','XTick',...
    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7],'YGrid','on','YTick',...
    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2]);
% Create legend
% Create legend
legend8 = legend(axes8,'show','FontSize', 18);
set(legend8,...
    'location','southwest');

%------------------------

% Create figure
figure9 = figure;
% Create axes
axes9 = axes('Parent',figure9);
hold(axes9,'on');
set(gca,'FontSize',18)
%
%EBh1 = errorbar(Xh,Y1,err1,'LineWidth',1.5);
%set(EBh1,'DisplayName','1/2" Test #1 12/02/19 Poissonian','Color',...
 %   [0.00 0.45 0.74]);
%
%EBh2 = errorbar(Xh,Y2,err2,'LineWidth',1.5);
%set(EBh2,'DisplayName','1/2" Test #2 12/03/19 Poissonian','Color',...
 %   [0.85 0.33 0.10]);
%
%EBh3 = errorbar(Xh,Y3,err3,'LineWidth',1.5);
%set(EBh3,'DisplayName','1/2" Test #3 12/03/19 Poissonian','Color',...
 %   [0.93 0.69 0.13]);
%
%EBh4 = errorbar(Xh,Y4,err4,'LineWidth',1.5);
%set(EBh4,'DisplayName','1/2" Test #4 12/04/19 Poissonian','Color',...
 %   [0.49 0.18 0.56]);
%
%EBh5 = errorbar(Xh,Y5,err5,'LineWidth',1.5);
%set(EBh5,'DisplayName','1/2" Test #5 12/06/19 NON-POISSONIAN','Color',...
 %   [0.47 0.67 0.19]);
%
EBh6 = errorbar(Xh,Y6,err6,'LineWidth',1.5);
set(EBh6,'DisplayName','1/2" Test #6 01/06/20 NON-POISSONIAN','Color',...
    [0.30 0.75 0.93]);
%
%EBh7 = errorbar(Xhi,Y7,err7,'LineWidth',1.5);
%set(EBh7,'DisplayName','1/2" Test #7 01/06/20 NON-POISSONIAN','Color',...
 %   [1 0 0]);
%
%EBh8 = errorbar(Xh,Y8,err8,'LineWidth',1.5);
%set(EBh8,'DisplayName','1/2" Test #8 01/07/20 Poissonian','Color',...
 %   [0.32 0.55 0.02]);
%
%EBh9 = errorbar(Xh,Y9,err9,'LineWidth',1.5);
%set(EBh9,'DisplayName','1/2" Test #9 01/08/20 NON-POISSSONIAN','Color',...
 %   [1 0 0]);
%
EBavg2 = errorbar(Xh,Yavg4,errAvg4,'LineWidth',2);
set(EBavg2,'DisplayName','Avg. Azimuthal Pos. 4 06/06/19','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of coinc. rate to rate at center'},'FontSize', 20);
% Create xlabel
xlabel({'Distance From Center of 10" PMT (inches)'},'FontSize', 20);
% Create title
title({'10" PMT Testing - Jan 06 Result #1 w/ Prior Az4 Average'},'FontSize', 28);
box(axes9,'on');
% Set the remaining axes properties
set(axes9,'XGrid','on','XTick',...
    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7],'YGrid','on','YTick',...
    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2]);
% Create legend
% Create legend
legend9 = legend(axes9,'show','FontSize', 18);
set(legend9,...
    'location','southwest');

%------------------------

% Create figure
figure10 = figure;
% Create axes
axes10 = axes('Parent',figure10);
hold(axes10,'on');
set(gca,'FontSize',18)
%
%EBh1 = errorbar(Xh,Y1,err1,'LineWidth',1.5);
%set(EBh1,'DisplayName','1/2" Test #1 12/02/19 Poissonian','Color',...
 %   [0.00 0.45 0.74]);
%
%EBh2 = errorbar(Xh,Y2,err2,'LineWidth',1.5);
%set(EBh2,'DisplayName','1/2" Test #2 12/03/19 Poissonian','Color',...
 %   [0.85 0.33 0.10]);
%
%EBh3 = errorbar(Xh,Y3,err3,'LineWidth',1.5);
%set(EBh3,'DisplayName','1/2" Test #3 12/03/19 Poissonian','Color',...
 %   [0.93 0.69 0.13]);
%
%EBh4 = errorbar(Xh,Y4,err4,'LineWidth',1.5);
%set(EBh4,'DisplayName','1/2" Test #4 12/04/19 Poissonian','Color',...
 %   [0.49 0.18 0.56]);
%
%EBh5 = errorbar(Xh,Y5,err5,'LineWidth',1.5);
%set(EBh5,'DisplayName','1/2" Test #5 12/06/19 NON-POISSONIAN','Color',...
 %   [0.47 0.67 0.19]);
%
%EBh6 = errorbar(Xh,Y6,err6,'LineWidth',1.5);
%set(EBh6,'DisplayName','1/2" Test #6 01/06/20 NON-POISSONIAN','Color',...
 %   [0.30 0.75 0.93]);
%
EBh7 = errorbar(Xhi,Y7,err7,'LineWidth',1.5);
set(EBh7,'DisplayName','1/2" Test #7 01/06/20 NON-POISSONIAN','Color',...
    [0.64 0.08 0.18]);
%
%EBh8 = errorbar(Xh,Y8,err8,'LineWidth',1.5);
%set(EBh8,'DisplayName','1/2" Test #8 01/07/20 Poissonian','Color',...
 %   [0.32 0.55 0.02]);
%
%EBh9 = errorbar(Xh,Y9,err9,'LineWidth',1.5);
%set(EBh9,'DisplayName','1/2" Test #9 01/08/20 NON-POISSSONIAN','Color',...
 %   [1 0 0]);
%
EBavg2 = errorbar(Xh,Yavg4,errAvg4,'LineWidth',2);
set(EBavg2,'DisplayName','Avg. Azimuthal Pos. 4 06/06/19','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of coinc. rate to rate at center'},'FontSize', 20);
% Create xlabel
xlabel({'Distance From Center of 10" PMT (inches)'},'FontSize', 20);
% Create title
title({'10" PMT Testing - Jan 06 Result #2 w/ Prior Az4 Average'},'FontSize', 28);
box(axes10,'on');
% Set the remaining axes properties
set(axes10,'XGrid','on','XTick',...
    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7],'YGrid','on','YTick',...
    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2]);
% Create legend
% Create legend
legend10 = legend(axes10,'show','FontSize', 18);
set(legend10,...
    'location','southwest');

%------------------------

% Create figure
figure11 = figure;
% Create axes
axes11 = axes('Parent',figure11);
hold(axes11,'on');
set(gca,'FontSize',18)
%
EBh1 = errorbar(Xh,Y1,err1,'LineWidth',1.5);
set(EBh1,'DisplayName','1/2" Test #1 12/02/19 Poissonian','Color',...
    [0.00 0.45 0.74]);
%
EBh2 = errorbar(Xh,Y2,err2,'LineWidth',1.5);
set(EBh2,'DisplayName','1/2" Test #2 12/03/19 Poissonian','Color',...
    [0.85 0.33 0.10]);
%
EBh3 = errorbar(Xh,Y3,err3,'LineWidth',1.5);
set(EBh3,'DisplayName','1/2" Test #3 12/03/19 Poissonian','Color',...
    [0.93 0.69 0.13]);
%
EBh4 = errorbar(Xh,Y4,err4,'LineWidth',1.5);
set(EBh4,'DisplayName','1/2" Test #4 12/04/19 Poissonian','Color',...
    [0.49 0.18 0.56]);
%
EBh5 = errorbar(Xh,Y5,err5,'LineWidth',1.5);
set(EBh5,'DisplayName','1/2" Test #5 12/06/19 NON-POISSONIAN','Color',...
    [0.47 0.67 0.19]);
%
EBh6 = errorbar(Xh,Y6,err6,'LineWidth',1.5);
set(EBh6,'DisplayName','1/2" Test #6 01/06/20 NON-POISSONIAN','Color',...
    [0.30 0.75 0.93]);
%
EBh7 = errorbar(Xhi,Y7,err7,'LineWidth',1.5);
set(EBh7,'DisplayName','1/2" Test #7 01/06/20 NON-POISSONIAN','Color',...
    [0.64 0.08 0.18]);
%
%EBh8 = errorbar(Xh,Y8,err8,'LineWidth',1.5);
%set(EBh8,'DisplayName','1/2" Test #8 01/07/20 Poissonian','Color',...
 %   [0.32 0.55 0.02]);
%
%EBh9 = errorbar(Xh,Y9,err9,'LineWidth',1.5);
%set(EBh9,'DisplayName','1/2" Test #9 01/08/20 NON-POISSSONIAN','Color',...
 %   [1 0 0]);
%
EBavg2 = errorbar(Xh,Yavg4,errAvg4,'LineWidth',2);
set(EBavg2,'DisplayName','Avg. Azimuthal Pos. 4 06/06/19','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of coinc. rate to rate at center'},'FontSize', 20);
% Create xlabel
xlabel({'Distance From Center of 10" PMT (inches)'},'FontSize', 20);
% Create title
title({'10" PMT Testing - Dec 02--Jan 06 Results w/ Prior Az4 Average'},'FontSize', 28);
box(axes11,'on');
% Set the remaining axes properties
set(axes11,'XGrid','on','XTick',...
    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7],'YGrid','on','YTick',...
    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2]);
% Create legend
% Create legend
legend11 = legend(axes11,'show','FontSize', 18);
set(legend11,...
    'location','southwest');

%------------------------

% Create figure
figure12 = figure;
% Create axes
axes12 = axes('Parent',figure12);
hold(axes12,'on');
set(gca,'FontSize',18)
%
%EBh1 = errorbar(Xh,Y1,err1,'LineWidth',1.5);
%set(EBh1,'DisplayName','1/2" Test #1 12/02/19 Poissonian','Color',...
 %   [0.00 0.45 0.74]);
%
%EBh2 = errorbar(Xh,Y2,err2,'LineWidth',1.5);
%set(EBh2,'DisplayName','1/2" Test #2 12/03/19 Poissonian','Color',...
 %   [0.85 0.33 0.10]);
%
%EBh3 = errorbar(Xh,Y3,err3,'LineWidth',1.5);
%set(EBh3,'DisplayName','1/2" Test #3 12/03/19 Poissonian','Color',...
 %   [0.93 0.69 0.13]);
%
%EBh4 = errorbar(Xh,Y4,err4,'LineWidth',1.5);
%set(EBh4,'DisplayName','1/2" Test #4 12/04/19 Poissonian','Color',...
 %   [0.49 0.18 0.56]);
%
%EBh5 = errorbar(Xh,Y5,err5,'LineWidth',1.5);
%set(EBh5,'DisplayName','1/2" Test #5 12/06/19 NON-POISSONIAN','Color',...
 %   [0.47 0.67 0.19]);
%
%EBh6 = errorbar(Xh,Y6,err6,'LineWidth',1.5);
%set(EBh6,'DisplayName','1/2" Test #6 01/06/20 NON-POISSONIAN','Color',...
 %   [0.30 0.75 0.93]);
%
%EBh7 = errorbar(Xhi,Y7,err7,'LineWidth',1.5);
%set(EBh7,'DisplayName','1/2" Test #7 01/06/20 NON-POISSONIAN','Color',...
 %   [0.64 0.08 0.18]);
%
EBh8 = errorbar(Xh,Y8,err8,'LineWidth',1.5);
set(EBh8,'DisplayName','1/2" Test #8 01/07/20 Poissonian','Color',...
    [1.00 0.07 0.65]);
%
%EBh9 = errorbar(Xh,Y9,err9,'LineWidth',1.5);
%set(EBh9,'DisplayName','1/2" Test #9 01/08/20 NON-POISSSONIAN','Color',...
 %   [1 0 0]);
%
EBavg2 = errorbar(Xh,Yavg4,errAvg4,'LineWidth',2);
set(EBavg2,'DisplayName','Avg. Azimuthal Pos. 4 06/06/19','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of coinc. rate to rate at center'},'FontSize', 20);
% Create xlabel
xlabel({'Distance From Center of 10" PMT (inches)'},'FontSize', 20);
% Create title
title({'10" PMT Testing - Jan 07 Result w/ Prior Az4 Average'},'FontSize', 28);
box(axes12,'on');
% Set the remaining axes properties
set(axes12,'XGrid','on','XTick',...
    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7],'YGrid','on','YTick',...
    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2]);
% Create legend
% Create legend
legend12 = legend(axes12,'show','FontSize', 18);
set(legend12,...
    'location','southwest');

%------------------------

% Create figure
figure13 = figure;
% Create axes
axes13 = axes('Parent',figure13);
hold(axes13,'on');
set(gca,'FontSize',18)
%
EBh1 = errorbar(Xh,Y1,err1,'LineWidth',1.5);
set(EBh1,'DisplayName','1/2" Test #1 12/02/19 Poissonian','Color',...
    [0.00 0.45 0.74]);
%
EBh2 = errorbar(Xh,Y2,err2,'LineWidth',1.5);
set(EBh2,'DisplayName','1/2" Test #2 12/03/19 Poissonian','Color',...
    [0.85 0.33 0.10]);
%
EBh3 = errorbar(Xh,Y3,err3,'LineWidth',1.5);
set(EBh3,'DisplayName','1/2" Test #3 12/03/19 Poissonian','Color',...
    [0.93 0.69 0.13]);
%
EBh4 = errorbar(Xh,Y4,err4,'LineWidth',1.5);
set(EBh4,'DisplayName','1/2" Test #4 12/04/19 Poissonian','Color',...
    [0.49 0.18 0.56]);
%
EBh5 = errorbar(Xh,Y5,err5,'LineWidth',1.5);
set(EBh5,'DisplayName','1/2" Test #5 12/06/19 NON-POISSONIAN','Color',...
    [0.47 0.67 0.19]);
%
EBh6 = errorbar(Xh,Y6,err6,'LineWidth',1.5);
set(EBh6,'DisplayName','1/2" Test #6 01/06/20 NON-POISSONIAN','Color',...
    [0.30 0.75 0.93]);
%
EBh7 = errorbar(Xhi,Y7,err7,'LineWidth',1.5);
set(EBh7,'DisplayName','1/2" Test #7 01/06/20 NON-POISSONIAN','Color',...
    [0.64 0.08 0.18]);
%
EBh8 = errorbar(Xh,Y8,err8,'LineWidth',1.5);
set(EBh8,'DisplayName','1/2" Test #8 01/07/20 Poissonian','Color',...
    [1.00 0.07 0.65]);
%
%EBh9 = errorbar(Xh,Y9,err9,'LineWidth',1.5);
%set(EBh9,'DisplayName','1/2" Test #9 01/08/20 NON-POISSSONIAN','Color',...
 %   [0.72, 0.27, 1.00]);
%
EBavg2 = errorbar(Xh,Yavg4,errAvg4,'LineWidth',2);
set(EBavg2,'DisplayName','Avg. Azimuthal Pos. 4 06/06/19','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of coinc. rate to rate at center'},'FontSize', 20);
% Create xlabel
xlabel({'Distance From Center of 10" PMT (inches)'},'FontSize', 20);
% Create title
title({'10" PMT Testing - Dec 02--Jan 07 Results w/ Prior Az4 Average'},'FontSize', 28);
box(axes13,'on');
% Set the remaining axes properties
set(axes13,'XGrid','on','XTick',...
    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7],'YGrid','on','YTick',...
    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2]);
% Create legend
% Create legend
legend13 = legend(axes13,'show','FontSize', 18);
set(legend13,...
    'location','southwest');

%------------------------

% Create figure
figure14 = figure;
% Create axes
axes14 = axes('Parent',figure14);
hold(axes14,'on');
set(gca,'FontSize',18)
%
%EBh1 = errorbar(Xh,Y1,err1,'LineWidth',1.5);
%set(EBh1,'DisplayName','1/2" Test #1 12/02/19 Poissonian','Color',...
 %   [0.00 0.45 0.74]);
%
%EBh2 = errorbar(Xh,Y2,err2,'LineWidth',1.5);
%set(EBh2,'DisplayName','1/2" Test #2 12/03/19 Poissonian','Color',...
 %   [0.85 0.33 0.10]);
%
%EBh3 = errorbar(Xh,Y3,err3,'LineWidth',1.5);
%set(EBh3,'DisplayName','1/2" Test #3 12/03/19 Poissonian','Color',...
 %   [0.93 0.69 0.13]);
%
%EBh4 = errorbar(Xh,Y4,err4,'LineWidth',1.5);
%set(EBh4,'DisplayName','1/2" Test #4 12/04/19 Poissonian','Color',...
 %   [0.49 0.18 0.56]);
%
%EBh5 = errorbar(Xh,Y5,err5,'LineWidth',1.5);
%set(EBh5,'DisplayName','1/2" Test #5 12/06/19 NON-POISSONIAN','Color',...
 %   [0.47 0.67 0.19]);
%
%EBh6 = errorbar(Xh,Y6,err6,'LineWidth',1.5);
%set(EBh6,'DisplayName','1/2" Test #6 01/06/20 NON-POISSONIAN','Color',...
 %   [0.30 0.75 0.93]);
%
%EBh7 = errorbar(Xhi,Y7,err7,'LineWidth',1.5);
%set(EBh7,'DisplayName','1/2" Test #7 01/06/20 NON-POISSONIAN','Color',...
 %   [0.64 0.08 0.18]);
%
%EBh8 = errorbar(Xh,Y8,err8,'LineWidth',1.5);
%set(EBh8,'DisplayName','1/2" Test #8 01/07/20 Poissonian','Color',...
 %   [1.00 0.07 0.65]);
%
EBh9 = errorbar(Xh,Y9,err9,'LineWidth',1.5);
set(EBh9,'DisplayName','1/2" Test #9 01/08/20 NON-POISSSONIAN','Color',...
    [0.72, 0.27, 1.00]);
%
EBavg2 = errorbar(Xh,Yavg4,errAvg4,'LineWidth',2);
set(EBavg2,'DisplayName','Avg. Azimuthal Pos. 4 06/06/19','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of coinc. rate to rate at center'},'FontSize', 20);
% Create xlabel
xlabel({'Distance From Center of 10" PMT (inches)'},'FontSize', 20);
% Create title
title({'10" PMT Testing - Jan 8 Result w/ Prior Az4 Average'},'FontSize', 28);
box(axes14,'on');
% Set the remaining axes properties
set(axes14,'XGrid','on','XTick',...
    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7],'YGrid','on','YTick',...
    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2]);
% Create legend
% Create legend
legend14 = legend(axes14,'show','FontSize', 18);
set(legend14,...
    'location','southwest');

%------------------------

% Create figure
figure15 = figure;
% Create axes
axes15 = axes('Parent',figure15);
hold(axes15,'on');
set(gca,'FontSize',18)
%
EBh1 = errorbar(Xh,Y1,err1,'LineWidth',1.5);
set(EBh1,'DisplayName','1/2" Test #1 12/02/19 Poissonian','Color',...
    [0.00 0.45 0.74]);
%
EBh2 = errorbar(Xh,Y2,err2,'LineWidth',1.5);
set(EBh2,'DisplayName','1/2" Test #2 12/03/19 Poissonian','Color',...
    [0.85 0.33 0.10]);
%
EBh3 = errorbar(Xh,Y3,err3,'LineWidth',1.5);
set(EBh3,'DisplayName','1/2" Test #3 12/03/19 Poissonian','Color',...
    [0.93 0.69 0.13]);
%
EBh4 = errorbar(Xh,Y4,err4,'LineWidth',1.5);
set(EBh4,'DisplayName','1/2" Test #4 12/04/19 Poissonian','Color',...
    [0.49 0.18 0.56]);
%
EBh5 = errorbar(Xh,Y5,err5,'LineWidth',1.5);
set(EBh5,'DisplayName','1/2" Test #5 12/06/19 NON-POISSONIAN','Color',...
    [0.47 0.67 0.19]);
%
EBh6 = errorbar(Xh,Y6,err6,'LineWidth',1.5);
set(EBh6,'DisplayName','1/2" Test #6 01/06/20 NON-POISSONIAN','Color',...
    [0.30 0.75 0.93]);
%
EBh7 = errorbar(Xhi,Y7,err7,'LineWidth',1.5);
set(EBh7,'DisplayName','1/2" Test #7 01/06/20 NON-POISSONIAN','Color',...
    [0.64 0.08 0.18]);
%
EBh8 = errorbar(Xh,Y8,err8,'LineWidth',1.5);
set(EBh8,'DisplayName','1/2" Test #8 01/07/20 Poissonian','Color',...
    [1.00 0.07 0.65]);
%
EBh9 = errorbar(Xh,Y9,err9,'LineWidth',1.5);
set(EBh9,'DisplayName','1/2" Test #9 01/08/20 NON-POISSSONIAN','Color',...
    [0.72, 0.27, 1.00]);
%
EBavg2 = errorbar(Xh,Yavg4,errAvg4,'LineWidth',2);
set(EBavg2,'DisplayName','Avg. Azimuthal Pos. 4 06/06/19','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of coinc. rate to rate at center'},'FontSize', 20);
% Create xlabel
xlabel({'Distance From Center of 10" PMT (inches)'},'FontSize', 20);
% Create title
title({'10" PMT Testing - Dec 02--Jan 08 Results w/ Prior Az4 Average'},'FontSize', 28);
box(axes15,'on');
% Set the remaining axes properties
set(axes15,'XGrid','on','XTick',...
    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7],'YGrid','on','YTick',...
    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2]);
% Create legend
% Create legend
legend15 = legend(axes15,'show','FontSize', 18);
set(legend15,...
    'location','southwest');