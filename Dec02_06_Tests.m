%% 02-06 December Test Results
% PMT in Azimuthal position 4

%% Relative Fractional Counts w/ Error

%Positions from center; half inch code
Xh = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5];
%Positions from center; half inch code - Bob run
Xhb = [0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5];
%Positions from center; half inch code - incomplete run
Xhi = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5];
%Positions from center; quarter inch code
Xq = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 3.75, 4, 4.25, 4.5, 4.75, 5,...
    5.25, 5.5, 5.75, 6, 6.25, 6.5, 6.75];

%Normalized Fraction; Half Inch Run #1 12/02
Y1 = [0.965073529, 0.966911765, 0.971530249, 0.954725275, 0.864791289,...
    0.921028466, 0.950160037, 0.905777778, 0.78313253, 0.64957265,...
    0.510822511, 0.335546702, 0.064826982, 0.052816901];
err1 = [0.036263397, 0.036309168, 0.035836046, 0.035213327, 0.033527128,...
    0.035145966, 0.035801133, 0.034201942, 0.030624324, 0.02712026,...
    0.02356335, 0.018625702, 0.007657143, 0.006908081];
%Normalized Fraction; Half Inch Run #2 12/03
Y2 = [1.002375297, 1.025641026, 0.985146143, 0.966618287, 0.857685009,...
    0.895676692, 0.950071327, 0.907863, 0.804619827, 0.645727483,...
    0.514563107, 0.327808471, 0.052785924, 0.052955083];
err2 = [0.037811301, 0.038746828, 0.037538067, 0.037246726, 0.03409854,...
    0.034911207, 0.036506881, 0.035685968, 0.03295411, 0.028091067,...
    0.023895749, 0.018743634, 0.007277431, 0.007169492];
%Normalized Fraction; Half Inch Run #3 12/03
Y3 = [0.98935631, 0.998457584, 0.94535809, 0.877922078, 0.778958017,...
    0.917948718, 0.921630094, 0.819502075, 0.799363057, 0.65179543,...
    0.553714591, 0.310454065, 0.061278394, 0.060956385];
err3 = [0.038716997, 0.039233221, 0.038433643, 0.036228649, 0.033089841,...
    0.03706216, 0.03750766, 0.034618458, 0.034463644, 0.030665702,...
    0.02749111, 0.019460651, 0.008168583, 0.008125023];
%Normalized Fraction; Half Inch Run #4 12/04
Y4 = [0.96794129, 0.968602826, 0.952936601, 0.949272513, 0.856006364,...
    0.845522096, 0.920436817, 0.935394372, 0.853010164, 0.631578947,...
    0.579341317, 0.319205804, 0.072837633, 0.062854979];
err4 = [0.033310882, 0.033593032, 0.033083239, 0.033180255, 0.03118426,...
    0.030674543, 0.032378885, 0.032989215, 0.030844554, 0.024996615,...
    0.023648491, 0.016812693, 0.007568117, 0.007006804];
%Normalized Fraction; Half Inch Run #5 12/06
Y5 = [0.98710538, 0.9754135, 0.957033017, 0.897092755, 0.84821024,...
    0.92513369, 0.916780355, 0.91136974, 0.795149415, 0.756989247,...
    0.855578371, 0.392286501, 0.067843866, 0.052150046];
err5 = [0.036208648, 0.036019282, 0.035776444, 0.03463143, 0.033085417,...
    0.034726713, 0.034871602, 0.034463105, 0.031025224, 0.033497,...
    0.040893269, 0.022738927, 0.00807408, 0.006996919];
%Averaged Normalized Fraction; Azimuthal Position 4 (Prior Tests)
Yavg4 = [0.981802648, 1.028865794, 0.997276748, 0.934058818, 0.907278947,...
    0.982333064, 0.948667327, 0.939313907, 0.847033942, 0.717111256,...
    0.575783076, 0.393563998, 0.065332997, 0.060880174];
errAvg4 = [0.018029324, 0.018582973, 0.018141091, 0.017155722, 0.01685918,...
    0.017858786, 0.017355145, 0.017262031, 0.016119061, 0.014480761,...
    0.012556997, 0.009977517, 0.003795995, 0.003656637];


% Create figure
figure1 = figure;
% Create axes
axes1 = axes('Parent',figure1);
hold(axes1,'on');
%
EBh1 = errorbar(Xh,Y1,err1,'LineWidth',1.5);
set(EBh1,'DisplayName','1/2" Test #1 12/02 Poissonian');
%
EBh2 = errorbar(Xh,Y2,err2,'LineWidth',1.5);
set(EBh2,'DisplayName','1/2" Test #2 12/03 Poissonian');
%
EBh3 = errorbar(Xh,Y3,err3,'LineWidth',1.5);
set(EBh3,'DisplayName','1/2" Test #3 12/03 Poissonian');
%
EBh4 = errorbar(Xh,Y4,err4,'LineWidth',1.5);
set(EBh4,'DisplayName','1/2" Test #4 12/04 Poissonian');
%
EBh5 = errorbar(Xh,Y5,err5,'LineWidth',1.5);
set(EBh5,'DisplayName','1/2" Test #5 12/06 NON-POISSONIAN');
%
EBavg2 = errorbar(Xh,Yavg4,errAvg4,'LineWidth',1.5);
set(EBavg2,'DisplayName','Avg. Azimuthal Pos. 4','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of coinc. rate to rate at center'});

% Create xlabel
xlabel({'Distance From Center of 10" PMT'});

% Create title
title({'10" PMT Testing - Dec 02--06 Results w/ Prior Az4 Average'});

box(axes1,'on');
% Set the remaining axes properties
set(axes1,'XGrid','on','XTick',...
    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7],'YGrid','on','YTick',...
    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2]);
% Create legend
% Create legend
legend1 = legend(axes1,'show');
set(legend1,...
    'Position',[0.735725938009788 0.744038155802862 0.156199021207178 0.155802894093464]);

%% Dec 02 1/2" Test 1 Coinc. Rates
%V_pulser = 6.11V
%Central Rate Standard Deviation = 37.2710
%sqrt(avg. Central Rate) = 33.4505

%Test Index
Xch = [1,2,3,5,7,9,11,13,15,17,19,21,23,25,27,29];
Xnch = [4,6,8,10,12,14,16,18,20,22,24,26,28];
%Central Rates
Ych1 = [1103, 1050, 1073, 1103, 1145, 1130, 1074, 1104, 1083, 1167,...
    1157, 1183, 1127, 1132, 1151, 1121];
%Non-Central Rates
Ynch1 = [1052, 1092, 1086, 953, 1003, 1039, 1019, 910, 760, 590,...
    379, 74, 60];

% Create figure
figure2 = figure;
%axis([0 35 0 1750])

% Create axes
axes2 = axes('Parent',figure2);
hold(axes2,'on');

bar1 = bar(Xch,Ych1,0.8,'blue');
set(bar1,'DisplayName','Central Position','FaceColor',[0 0 1]);
bar2 = bar(Xnch,Ynch1,'red');
set(bar2,'DisplayName','Non-Central Positions','FaceColor',[1 0 0],...
    'BarWidth',0.4);

% Create ylabel
ylabel({'Coinc. Counts in 1 Minute'});

% Create xlabel
xlabel({'Index'});

% Create title
title({'10" PMT Testing - 1/2" Test 1 - Dec 02 2019'});

box(axes2,'on');
% Set the remaining axes properties
set(axes2,'XGrid','off','XTick',...
    [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 ...
     26 27 28 29],'YGrid','on','YTick', [0 250 500 750 1000 1250 ...
     1500 1750]);
% Create legend
%legend(axes2,'show');

%% Dec 03 1/2" Test 2 Coinc. Rates
%V_pulser = 6.17V
%Central Rate Standard Deviation = 38.9725
%sqrt(avg. Central Rate) = 32.5413

%Test Index
Xch = [1,2,3,5,7,9,11,13,15,17,19,21,23,25,27,29];
Xnch = [4,6,8,10,12,14,16,18,20,22,24,26,28];
%Central Rates
Ych2 = [1101, 1055, 1004, 1063, 1024, 1043, 1065, 1063, 1040, 1033,...
    1045, 1120, 1146, 1026, 1020, 1095];
%Non-Central Rates
Ynch2 = [1060, 1028, 999, 904, 953, 999, 941, 836, 699, 583, 356,...
    54, 56];

% Create figure
figure3 = figure;
%axis([0 35 0 1750])

% Create axes
axes3 = axes('Parent',figure3);
hold(axes3,'on');

bar3 = bar(Xch,Ych2,'blue');
set(bar3,'DisplayName','Central Position','FaceColor',[0 0 1]);
bar4 = bar(Xnch,Ynch2,0.4,'red');
set(bar4,'DisplayName','Non-Central Positions','FaceColor',[1 0 0],...
    'BarWidth',0.4);

% Create ylabel
ylabel({'Coinc. Counts in 1 Minute'});

% Create xlabel
xlabel({'Index'});

% Create title
title({'10" PMT Testing - 1/2" Test 2 - Dec 03 2019'});

box(axes3,'on');
% Set the remaining axes properties
set(axes3,'XGrid','off','XTick',...
    [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 ...
     26 27 28 29],'YGrid','on','YTick', [0 250 500 750 1000 1250 ...
     1500 1750]);
% Create legend
%legend(axes3,'show');

%% Dec 03 1/2" Test 3 Coinc. Rates
%V_pulser = 6.22V
%Central Rate Standard Deviation = 24.6536
%sqrt(avg. Central Rate) = 30.9475

%Test Index
Xch = [1,2,3,5,7,9,11,13,15,17,19,21,23,25,27,29];
Xnch = [4,6,8,10,12,14,16,18,20,22,24,26,28];
%Central Rates
Ych3 = [965, 976, 1008, 937, 948, 977, 1000, 950, 964, 964, 920, 918,...
    953, 941, 952, 951];
%Non-Central Rates
Ynch3 = [971, 891, 845, 770, 895, 882, 790, 753, 599, 518, 294, 58, 58];

% Create figure
figure4 = figure;
%axis([0 35 0 1750])

% Create axes
axes4 = axes('Parent',figure4);
hold(axes4,'on');

bar5 = bar(Xch,Ych3,'blue');
set(bar5,'DisplayName','Central Position','FaceColor',[0 0 1]);
bar6 = bar(Xnch,Ynch3,0.4,'red');
set(bar6,'DisplayName','Non-Central Positions','FaceColor',[1 0 0],...
    'BarWidth',0.4);

% Create ylabel
ylabel({'Coinc. Counts in 1 Minute'});

% Create xlabel
xlabel({'Index'});

% Create title
title({'10" PMT Testing - 1/2" Test 3 - Dec 03 2019'});

box(axes4,'on');
% Set the remaining axes properties
set(axes4,'XGrid','off','XTick',...
    [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 ...
     26 27 28 29],'YGrid','on','YTick', [0 250 500 750 1000 1250 ...
     1500 1750]);
% Create legend
%legend(axes4,'show');

%% Dec 04 1/2" Test 4 Coinc. Rates
%V_pulser = 6.11V
%Central Rate Standard Deviation = 32.1307
%sqrt(avg. Central Rate) = 35.9287

%Test Index
Xch = [1,2,3,5,7,9,11,13,15,17,19,21,23,25,27,29];
Xnch = [4,6,8,10,12,14,16,18,20,22,24,26,28];
%Central Rates
Ych4 = [1310, 1253, 1279, 1269, 1302, 1241, 1273, 1284, 1280, 1243,...
    1315, 1345, 1327, 1292, 1344, 1297];
%Non-Central Rates
Ynch4 = [1234, 1225, 1207, 1076, 1081, 1180, 1180, 1091, 840, 774,...
    418, 96, 83];

% Create figure
figure5 = figure;
%axis([0 35 0 1750])

% Create axes
axes5 = axes('Parent',figure5);
hold(axes5,'on');

bar7 = bar(Xch,Ych4,'blue');
set(bar7,'DisplayName','Central Position','FaceColor',[0 0 1]);
bar8 = bar(Xnch,Ynch4,0.4,'red');
set(bar8,'DisplayName','Non-Central Positions','FaceColor',[1 0 0],...
    'BarWidth',0.4);

% Create ylabel
ylabel({'Coinc. Counts in 1 Minute'});

% Create xlabel
xlabel({'Index'});

% Create title
title({'10" PMT Testing - 1/2" Test 4 - Dec 04 2019'});

box(axes5,'on');
% Set the remaining axes properties
set(axes5,'XGrid','off','XTick',...
    [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 ...
     26 27 28 29],'YGrid','on','YTick', [0 250 500 750 1000 1250 ...
     1500 1750]);
% Create legend
%legend(axes5,'show');

%% Dec 06 1/2" Test 5 Coinc. Rates
%V_pulser = 6.13V
%Central Rate Standard Deviation = 133.985
%sqrt(avg. Central Rate) = 32.651

%Test Index
Xch = [1,2,3,5,7,9,11,13,15,17,19,21,23,25,27,29];
Xnch = [4,6,8,10,12,14,16,18,20,22,24,26,28];
%Central Rates
Ych5 = [1124, 1110, 1125, 1112, 1099, 1068, 1139, 1105, 1094, 1140, 1169,...
    691, 770, 1045, 1107, 1079];
%Non-Central Rates
Ynch5 = [1091, 1058, 972, 936, 1038, 1008, 1018, 918, 704, 625, 356,...
    73, 57];

% Create figure
figure6 = figure;
%axis([0 35 0 1750])

% Create axes
axes6 = axes('Parent',figure6);
hold(axes6,'on');

bar9 = bar(Xch,Ych5,'blue');
set(bar9,'DisplayName','Central Position','FaceColor',[0 0 1]);
bar10 = bar(Xnch,Ynch5,0.4,'red');
set(bar10,'DisplayName','Non-Central Positions','FaceColor',[1 0 0],...
    'BarWidth',0.4);

% Create ylabel
ylabel({'Coinc. Counts in 1 Minute'});

% Create xlabel
xlabel({'Index'});

% Create title
title({'10" PMT Testing - 1/2" Test 5 - Dec 06 2019'});

box(axes6,'on');
% Set the remaining axes properties
set(axes6,'XGrid','off','XTick',...
    [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 ...
     26 27 28 29],'YGrid','on','YTick', [0 250 500 750 1000 1250 ...
     1500 1750]);
% Create legend
%legend(axes6,'show');

%% Central Position Coinc. Rates Overall

%V_pulser = 6.11 V
Xc1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,...
    19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35,...
    36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52,...
    53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63];
Yc1 = [0, 1262, 1259, 1226, 1209, 1223, 1244, 1274, 1278, 1204, 1234, 1264,...
    1218, 1262, 1266, 1036, 939, 1295, 1102, 1254, 1294, 1288, 1287,...
    778, 1215, 788, 1259, 1267, 1288, 1315, 1320, 1269, 1297, 1323,...
    1330, 1285, 1299, 1296, 1325, 1337, 1310, 1253, 1279, 1269, 1302,...
    1241, 1273, 1284, 1280, 1243, 1315, 1345, 1327, 1292, 1344, 1297,...
    1313, 1310, 1294, 1295, 1356, 1286, 1345];

%V_pulser = 6.13 V
Xc2 = [64, 65, 66, 67, 68, 69,...
    70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86,...
    87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102,...
    103, 104, 105];
Yc2 = [1059, 1103, 1048, 1096, 1040, 1069, 1126, 1090, 1124, 1110, 1125,...
    1112, 1099, 1068, 1139, 1105, 1094, 1140, 1169, 691, 770, 1045, 1125,...
    1107, 1079, 1078, 1149, 700, 887, 1055, 1008, 689, 970, 645, 634,...
    1095, 1084, 924, 682, 1053, 794, 648];

%V_pulser = 6.22 V
Xc3 = [33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48];
Yc3 = [965, 976, 1008, 937, 948, 977, 1000, 950, 964, 964, 920, 918,...
    953, 941, 952, 951];

%V_pulser = 6.11 V
Xc4 = [49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64];
Yc4 = [1310, 1253, 1279, 1269, 1302, 1241, 1273, 1284, 1280, 1243, 1315,...
    1345, 1327, 1292, 1344, 1297];

%V_pulser = 6.13 V
Xc5 = [65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80];
Yc5 = [1124, 1110, 1125, 1112, 1099, 1068, 1139, 1105, 1094, 1140, 1169,...
    691, 770, 1045, 1107, 1079];

% Create figure
figure7 = figure;

% Create axes
axes7 = axes('Parent',figure7);
hold(axes7,'on');

cent1 = plot(Xc1,Yc1,'-o','MarkerIndices',1:1:length(Yc1),'LineWidth',1.0);
set(cent1,'DisplayName','Dec. 04: V_{pulser} = 6.11V');

cent2 = plot(Xc2,Yc2,'-o','MarkerIndices',1:1:length(Yc2),'LineWidth',1.0);
set(cent2,'DisplayName','Dec. 06: V_{pulser} = 6.13V');

%cent3 = plot(Xc3,Yc3,'-o','MarkerIndices',1:1:length(Yc3),'LineWidth',1.0);
%set(cent3,'DisplayName','V_{pulser} = 6.22V');

%cent4 = plot(Xc4,Yc4,'-o','MarkerIndices',1:1:length(Yc4),'LineWidth',1.0);
%set(cent4,'DisplayName','V_{pulser} = 6.11V');

%cent5 = plot(Xc5,Yc5,'-o','MarkerIndices',1:1:length(Yc5),'LineWidth',1.0);
%set(cent5,'DisplayName','V_{pulser} = 6.13V');

% Create ylabel
ylabel({'Coinc. Counts in 1 Minute'});

% Create xlabel
xlabel({'Index'});

% Create title
title({'10" PMT Testing - Central Rate Change - Dec 04 & 06 2019'});

box(axes7,'on');
% Create legend
legend2 = legend(axes7,'show');