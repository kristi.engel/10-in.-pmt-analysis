%% Averaged Normalized Fractions
% Azimuthal Positions 1, 2, 3, and 4

%% Azimuthal Position 1

%Positions from center; half inch code
Xh = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5];

%Normalized Fraction; Half Inch Test #2 06/06 (K)
Y1avg1 = [0.974853311, 1.047089041, 0.967989757, 0.840823184,...
    0.76974509, 0.87554672, 0.872669576, 0.92092257, 0.789494949,...
    0.7032, 0.596604689, 0.281568627, 0.121573302, 0.069133794];
err1Avg1 = [0.034863196, 0.036957106, 0.035017121, 0.031673404,...
    0.029848466, 0.031639593, 0.031534165, 0.033284899, 0.029829767,...
    0.027574555, 0.025023405, 0.015872261, 0.010122937, 0.007627114];
%Normalized Fraction; Half Inch Test #5 06/06 (K)
Y1avg2 = [0.986525112, 0.96269262, 0.877280265, 0.852459016,...
    0.755925156, 0.852842809, 0.861386139, 0.87862689, 0.884210526,...
    0.709785804, 0.577319588, 0.280554389, 0.126004228, 0.05152027];
err1Avg2 = [0.034685085, 0.034008743, 0.032349801, 0.031970486,...
    0.029431715, 0.031892813, 0.031887529, 0.032149776, 0.032768723,...
    0.028421791, 0.024770593, 0.016392646, 0.010642877, 0.006680918];
%Normalized Fraction; Half Inch Test #6 06/06 (K)
Y1avg3 = [1.001745201, 0.970049917, 0.930848861, 0.816528926,...
    0.717027358, 0.843945069, 0.893019413, 0.926341073, 0.829594648,...
    0.721828211, 0.638061184, 0.264508488, 0.12805122, 0.047870183];
err1Avg3 = [0.036220817, 0.034618774, 0.033610713, 0.03082728,...
    0.028204635, 0.031603909, 0.032666961, 0.032942133, 0.030394342,...
    0.027822841, 0.025860206, 0.015377622, 0.010442387, 0.00630631];
%Normalized Fraction; Half Inch Test #7 06/06 (K)
Y1avg4 = [1.001577287, 0.973057399, 0.942404329, 0.818005433,...
    0.784879189, 0.814928819, 0.909505703, 0.874192322, 0.854881266,...
    0.770454545, 0.601577169, 0.294161123, 0.141384389, 0.0562249];
err1Avg4 = [0.034430424, 0.033609812, 0.032739466, 0.029908305,...
    0.029186113, 0.029709162, 0.031720093, 0.030903024, 0.030330411,...
    0.028434596, 0.024242558, 0.01579213, 0.010558037, 0.006496862];
%Averaged Normalized Fraction; Azimuthal Position 1
Y1 = [0.991175228, 0.988222244, 0.929630803, 0.83195414, 0.756894198,...
    0.846815854, 0.884145208, 0.900020714, 0.839545348, 0.72631714,...
    0.603390657, 0.280198157, 0.129253285, 0.056187287];
err1 = [0.017528369, 0.017411385, 0.016722479, 0.015552621,...
    0.014586996, 0.015611808, 0.015977548, 0.016166465, 0.01542594,...
    0.014032977, 0.012490509, 0.007931386, 0.005221711, 0.003398403];

% Create figure
figure1 = figure;
% Create axes
axes1 = axes('Parent',figure1);
hold(axes1,'on');
%
EB11 = errorbar(Xh,Y1avg1,err1Avg1,'LineWidth',1.5);
set(EB11,'DisplayName','Half In. #2 06/06','Color',...
    [0 0.447058823529412 0.741176470588235]);
%
EB12 = errorbar(Xh,Y1avg2,err1Avg2,'LineWidth',1.5);
set(EB12,'DisplayName','Half. In. #5 06/06','Color',...
    [0 0.447058823529412 0.741176470588235]);
%
EB13 = errorbar(Xh,Y1avg3,err1Avg3,'LineWidth',1.5);
set(EB13,'DisplayName','Half In. #6 06/06','Color',...
    [0 0.447058823529412 0.741176470588235]);
%
EB14 = errorbar(Xh,Y1avg4,err1Avg4,'LineWidth',1.5);
set(EB14,'DisplayName','Half In. #7 06/06','Color',...
    [0 0.447058823529412 0.741176470588235]);
%
EB1avg = errorbar(Xh,Y1,err1,'LineWidth',2);
set(EB1avg,'DisplayName','Azimuthal Pos. 1 Avg.','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of coinc. rate to rate at center'});

% Create xlabel
xlabel({'Distance From Center of 10" PMT'});

% Create title
title({'10" PMT Testing - Azimuthal Position 1'});

box(axes1,'on');
% Set the remaining axes properties
set(axes1,'XGrid','on','XTick',...
    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7],'YGrid','on','YTick',...
    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2 1.3 1.4 1.5]);
% Create legend
% Create legend
legend1 = legend(axes1,'show');
set(legend1,...
    'Position',[0.735725938009788 0.744038155802862 0.156199021207178 0.155802894093464]);


%% Azimuthal Position 2

%Positions from center; half inch code
Xh = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5];
%Positions from center; half inch code; data Bob took
XhB = [0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5];
%Positions from center; half inch code - incomplete run
Xhi = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5];

%Normalized Fraction; Half Inch Test 04/24 (K)
Y2avg1 = [0.989063243, 0.952380952, 0.977570093, 1.007590133,...
    0.984441301, 1.015887426, 1.010039284, 0.988942946, 1.086317723,...
    0.987799367, 0.893713252, 0.565217391, 0.07255814, 0.061131387];
err2Avg1 = [0.037493842, 0.036410045, 0.036880607, 0.037915429,...
    0.037218263, 0.037292638, 0.036428616, 0.036157147, 0.039234653,...
    0.036519042, 0.034200501, 0.025896555, 0.008363291, 0.007581667];
%Normalized Fraction; Half Inch Run 05/09 (B)
Y2avg2 = [0.96920176, 1.037328094, 0.97834912, 0.967957276,...
    0.967453733, 1.031088083, 1.088788075, 0.991585761, 0.959142666,...
    0.802318094, 0.401529637, 0.046662346, 0.053770492];
err2Avg2 = [0.042529655, 0.045423899, 0.044401487, 0.043792628,...
    0.04280284, 0.044990779, 0.046685581, 0.043817907, 0.04360082,...
    0.038049266, 0.024790844, 0.007867259, 0.008509676];
%Normalized Fraction; Half Inch Test #3 05/10 (K)
Y2avg3 = [1.000877193, 0.987794246, 0.982158029, 0.981589958,...
    0.973839662, 1.054841474, 1.049615056, 1.134265142, 1.032416906,...
    0.955492038, 0.91163173, 0.670477746, 0.04808476, 0.060334285];
err2Avg3 = [0.036295024, 0.03586839, 0.035273843, 0.034993716,...
    0.034956531, 0.037156732, 0.037001155, 0.03880406, 0.035842166,...
    0.03395737, 0.033029871, 0.027039103, 0.006334909, 0.007118724];
%Normalized Fraction; Half Inch Test #4 05/10 (K)
Y2avg4 = [1.018766756, 1.085404428, 1.009466437, 0.94499179,...
    0.976123027, 1.004012841, 1.073547338, 1.074340528, 1.0640625,...
    0.964398734, 0.903822938, 0.623520126, 0.038628301, 0.073954984];
err2Avg4 = [0.03706996, 0.038901033, 0.036155355, 0.033800056,...
    0.034287936, 0.034789365, 0.036616324, 0.036333172, 0.035687194,...
    0.033628546, 0.032498517, 0.025407629, 0.005571365, 0.007851601];
%Normalized Fraction; Half Inch Test* 05/07 (K)
Y2avg5 = [1.011881188, 0.971915747, 0.950604839, 0.976673428,...
    0.95087001, 1.028890015, 1.104177151, 1.11746988, 1.065809005,...
    0.945525292, 0.897016362, 0.650590551];
err2Avg5 = [0.038842573, 0.038060105, 0.037599728, 0.038396075,...
    0.037894252, 0.039743187, 0.041533037, 0.041819071, 0.040209538,...
    0.036804964, 0.035363308, 0.029131533];
%Averaged Normalized Fraction; Azimuthal Position 2
Y2 = [1.005147095, 0.993339427, 0.991425499, 0.977838886,...
    0.970646255, 1.014217098, 1.053693383, 1.080761314,...
    1.048038379, 0.962471619, 0.881700475, 0.58226709, 0.051483387,...
    0.065140218];
err2 = [0.018718381, 0.017184721, 0.017191513, 0.017029779,...
    0.016895667, 0.017196822, 0.017647717, 0.017957251, 0.017475023,...
    0.016581025, 0.015511429, 0.011849936, 0.003562036, 0.003890856];

% Create figure
figure2 = figure;
% Create axes
axes2 = axes('Parent',figure2);
hold(axes2,'on');
%   
EB21 = errorbar(Xh,Y2avg1,err2Avg1,'LineWidth',1.5);
set(EB21,'DisplayName','Half In. 04/24','Color',[0.49 0.18 0.56]);
%
EB22 = errorbar(XhB,Y2avg2,err2Avg2,'LineWidth',1.5);
set(EB22,'DisplayName','Half In. 05/09','Color',[0.49 0.18 0.56]);
%
EB23 = errorbar(Xh,Y2avg3,err2Avg3,'LineWidth',1.5);
set(EB23,'DisplayName','Half In. #3 05/10','Color',[0.49 0.18 0.56]);
%
EB24 = errorbar(Xh,Y2avg4,err2Avg4,'LineWidth',1.5);
set(EB24,'DisplayName','Half In. #4 05/10','Color',[0.49 0.18 0.56]);
%
EB25 = errorbar(Xhi,Y2avg5,err2Avg5,'LineWidth',1.5);
set(EB25,'DisplayName','Half In. 05/07','Color',[0.49 0.18 0.56]);
%
EB2avg = errorbar(Xh,Y2,err2,'LineWidth',2);
set(EB2avg,'DisplayName','Azimuthal Pos. 2 Avg.','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of coinc. rate to rate at center'});

% Create xlabel
xlabel({'Distance From Center of 10" PMT'});

% Create title
title({'10" PMT Testing - Azimuthal Position 2'});

box(axes2,'on');
% Set the remaining axes properties
set(axes2,'XGrid','on','XTick',...
    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7],'YGrid','on','YTick',...
    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2 1.3 1.4 1.5]);
% Create legend
% Create legend
legend2 = legend(axes2,'show');
set(legend2,...
    'Position',[0.735725938009788 0.744038155802862 0.156199021207178 0.155802894093464]);


%% Azimuthal Position 3

%Positions from center; half inch code
Xh = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5];

%Normalized Fraction; Half Inch Test #2 05/22 (K)
Y3avg1 = [1.023784902, 1.1484375, 1.068097015, 1.101165501,...
    1.112140871, 1.318389753, 1.269797422, 1.359924918, 1.22431259,...
    0.829150579, 0.509956289, 0.171897634, 0.04624823, 0.052826691];
err3Avg1 = [0.040008436, 0.042018084, 0.039095566, 0.039900205,...
    0.040048274, 0.044736348, 0.04372177, 0.046305281, 0.043638024,...
    0.033647272, 0.024932819, 0.013426554, 0.006682843, 0.007088872];
%Normalized Fraction; Half Inch Test #3 05/22 (K)
Y3avg2 = [1.025, 1.097234612, 1.118165785, 1.104820876, 1.147757256,...
    1.270456503, 1.244941885, 1.315675206, 1.177083333, 0.849073257,...
    0.541092848, 0.157894737, 0.038793103, 0.065033408];
err3Avg2 = [0.03720494, 0.038933101, 0.039208586, 0.03895058,...
    0.039859368, 0.042301229, 0.041701665, 0.043522453, 0.040288089,...
    0.032673408, 0.024714875, 0.012025726, 0.00583875, 0.007734346];
%Normalized Fraction; Half Inch Test #4 05/22 (K)
Y3avg3 = [1.008358997, 1.194406856, 1.166515014, 1.097951915,...
    1.143233743, 1.260869565, 1.285904847, 1.356498195, 1.194139194,...
    0.816400911, 0.521779425, 0.16424214, 0.042492918, 0.059942912];
err3Avg3 = [0.036531937, 0.041484744, 0.04099417, 0.038915635,...
    0.039734681, 0.042709535, 0.043344818, 0.045328144, 0.041790549,...
    0.032365455, 0.024692855, 0.01291527, 0.006401409, 0.007664435];
%Normalized Fraction; Half Inch Test #5 05/22 (K)
Y3avg4 = [1.082454458, 1.21448999, 1.163339383, 1.082262211,...
    1.139896373, 1.262656859, 1.226666667, 1.326032013, 1.248126928,...
    0.866106938, 0.519569892, 0.162969283, 0.039284155, 0.071331828];
err3Avg4 = [0.039994107, 0.043137033, 0.040862065, 0.037805119,...
    0.039311631, 0.042220989, 0.040610161, 0.043102242, 0.042269647,...
    0.03311995, 0.023728687, 0.012263071, 0.00591337, 0.008167322];
%Averaged Normalized Fraction; Azimuthal Position 3
Y3 = [1.034899589, 1.16364224, 1.129029299, 1.096550126,...
    1.135757061, 1.27809317, 1.256827705, 1.339532583, 1.210915512,...
    0.840182921, 0.523099614, 0.164250948, 0.041704602, 0.06228371];
err3 = [0.019233749, 0.020710947, 0.020024995, 0.01944998,...
    0.019869705, 0.021502109, 0.021181626, 0.022291852, 0.021006815,...
    0.016477531, 0.012260858, 0.006334812, 0.003109435, 0.003836673];

% Create figure   
figure3 = figure;
% Create axes
axes3 = axes('Parent',figure3);
hold(axes3,'on');
%
EB31 = errorbar(Xh,Y3avg1,err3Avg1,'LineWidth',1.5);
set(EB31,'DisplayName','Half In. #2 05/22','Color',[0.32 0.55 0.02]);
%
EB32 = errorbar(Xh,Y3avg2,err3Avg2,'LineWidth',1.5);
set(EB32,'DisplayName','Half In. #3 05/22','Color',[0.32 0.55 0.02]);
%
EB33 = errorbar(Xh,Y3avg3,err3Avg3,'LineWidth',1.5);
set(EB33,'DisplayName','Half In. #4 05/22','Color',[0.32 0.55 0.02]);
%
EB34 = errorbar(Xh,Y3avg4,err3Avg4,'LineWidth',1.5);
set(EB34,'DisplayName','Half In. #5 05/22','Color',[0.32 0.55 0.02]);
%
EB3avg = errorbar(Xh,Y3,err3,'LineWidth',2);
set(EB3avg,'DisplayName','Azimuthal Pos. 3 Avg.','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of coinc. rate to rate at center'});

% Create xlabel
xlabel({'Distance From Center of 10" PMT'});

% Create title
title({'10" PMT Testing - Azimuthal Position 3'});

box(axes3,'on');
% Set the remaining axes properties
set(axes3,'XGrid','on','XTick',...
    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7],'YGrid','on','YTick',...
    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2 1.3 1.4 1.5]);
% Create legend
% Create legend
legend3 = legend(axes3,'show');
set(legend3,...
    'Position',[0.735725938009788 0.744038155802862 0.156199021207178 0.155802894093464]);

%% Azimuthal Position 4

%Positions from center; half inch code
Xh = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5];

%Normalized Fraction; Half Inch Test #2 06/03 (K)
Y4avg1 = [1.004553734, 1.021668972, 0.946740129, 0.895927602,...
    0.902360019, 0.977818854, 0.975319927, 0.933575978, 0.861974406,...
    0.694572217, 0.559216385, 0.395769061, 0.064516129, 0.057971014];
err4Avg1 = [0.037073228, 0.037726696, 0.035789644, 0.034263693,...
    0.034812738, 0.036681722, 0.036418053, 0.035298777, 0.033578122,...
    0.029340928, 0.025242861, 0.02044213, 0.007724959, 0.007350647];
%Normalized Fraction; Half Inch Test #3 06/03 (K)
Y4avg2 = [1.013010319, 1.03336339, 0.977973568, 0.944989339,...
    0.904904051, 1.014196983, 0.976169462, 0.934447866, 0.858890345,...
    0.709114697, 0.522193211, 0.37928007, 0.064660979, 0.06087735];
err4Avg2 = [0.037004301, 0.037593087, 0.035818801, 0.034449577,...
    0.033480793, 0.036827312, 0.035806468, 0.034732898, 0.032752519,...
    0.028944757, 0.02394031, 0.019903339, 0.007742575, 0.007493977];
%Normalized Fraction; Half Inch Test #4 06/03 (K)
Y4avg3 = [0.985326812, 1.058397465, 1.06275395, 0.96483705, 0.951103418,...
    0.985658409, 0.919491525, 0.944591029, 0.842338353, 0.761154856,...
    0.606217617, 0.392690183, 0.063911376, 0.051369863];
err4Avg3 = [0.036165241, 0.038280138, 0.038334125, 0.035023789,...
    0.034850267, 0.035762261, 0.033726515, 0.034973506, 0.03256261,...
    0.030321018, 0.026118627, 0.019982844, 0.007496837, 0.00671645];
%Normalized Fraction; Half Inch Test #5 06/03 (K)
Y4avg4 = [0.924319728, 1.002033347, 1.001639344, 0.930481283,...
    0.870748299, 0.95165801, 0.923688394, 0.944640754, 0.824932666,...
    0.703603255, 0.61550509, 0.40651668, 0.068243505, 0.073302469];
err4Avg4 = [0.033900404, 0.034975955, 0.03510266, 0.033491208,...
    0.0316272, 0.033499864, 0.032762193, 0.033047227, 0.029944024,...
    0.027148222, 0.025106326, 0.019480144, 0.007397855, 0.007657255];
%Averaged Normalized Fraction; Azimuthal Position 4
Y4 = [0.981802648, 1.028865794, 0.997276748, 0.934058818, 0.907278947,...
    0.982333064, 0.948667327, 0.939313907, 0.847033942, 0.717111256,...
    0.575783076, 0.393563998, 0.065332997, 0.060880174];
err4 = [0.018029324, 0.018582973, 0.018141091, 0.017155722, 0.01685918,...
    0.017858786, 0.017355145, 0.017262031, 0.016119061, 0.014480761,...
    0.012556997, 0.009977517, 0.003795995, 0.003656637];

% Create figure   
figure4 = figure;
% Create axes
axes4 = axes('Parent',figure4);
hold(axes4,'on');
%
EB41 = errorbar(Xh,Y4avg1,err4Avg1,'LineWidth',1.5);
set(EB41,'DisplayName','Half In. #2 06/03','Color',[0.85,0.33,0.10]);
%
EB42 = errorbar(Xh,Y4avg2,err4Avg2,'LineWidth',1.5);
set(EB42,'DisplayName','Half In. #3 06/03','Color',[0.85,0.33,0.10]);
%
EB43 = errorbar(Xh,Y4avg3,err4Avg3,'LineWidth',1.5);
set(EB43,'DisplayName','Half In. #4 06/03','Color',[0.85,0.33,0.10]);
%
EB44 = errorbar(Xh,Y4avg4,err4Avg4,'LineWidth',1.5);
set(EB44,'DisplayName','Half In. #5 06/03','Color',[0.85,0.33,0.10]);
%
EB4avg = errorbar(Xh,Y4,err4,'LineWidth',2);
set(EB4avg,'DisplayName','Azimuthal Pos. 4 Avg.','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of coinc. rate to rate at center'});

% Create xlabel
xlabel({'Distance From Center of 10" PMT'});

% Create title
title({'10" PMT Testing - Azimuthal Position 4'});

box(axes4,'on');
% Set the remaining axes properties
set(axes4,'XGrid','on','XTick',...
    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7],'YGrid','on','YTick',...
    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2 1.3 1.4 1.5]);
% Create legend
% Create legend
legend4 = legend(axes4,'show');
set(legend4,...
    'Position',[0.735725938009788 0.744038155802862 0.156199021207178 0.155802894093464]);

%% All Azimuthal Averages

%Positions from center; half inch code
Xh = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5];

%Averaged Normalized Fraction; Azimuthal Position 1
Y1 = [0.991175228, 0.988222244, 0.929630803, 0.83195414, 0.756894198,...
    0.846815854, 0.884145208, 0.900020714, 0.839545348, 0.72631714,...
    0.603390657, 0.280198157, 0.129253285, 0.056187287];
err1 = [0.017528369, 0.017411385, 0.016722479, 0.015552621,...
    0.014586996, 0.015611808, 0.015977548, 0.016166465, 0.01542594,...
    0.014032977, 0.012490509, 0.007931386, 0.005221711, 0.003398403];
%Averaged Normalized Fraction; Azimuthal Position 2
Y2 = [1.005147095, 0.993339427, 0.991425499, 0.977838886,...
    0.970646255, 1.014217098, 1.053693383, 1.080761314,...
    1.048038379, 0.962471619, 0.881700475, 0.58226709, 0.051483387,...
    0.065140218];
err2 = [0.018718381, 0.017184721, 0.017191513, 0.017029779,...
    0.016895667, 0.017196822, 0.017647717, 0.017957251, 0.017475023,...
    0.016581025, 0.015511429, 0.011849936, 0.003562036, 0.003890856];
%Averaged Normalized Fraction; Azimuthal Position 3
Y3 = [1.034899589, 1.16364224, 1.129029299, 1.096550126,...
    1.135757061, 1.27809317, 1.256827705, 1.339532583, 1.210915512,...
    0.840182921, 0.523099614, 0.164250948, 0.041704602, 0.06228371];
err3 = [0.019233749, 0.020710947, 0.020024995, 0.01944998,...
    0.019869705, 0.021502109, 0.021181626, 0.022291852, 0.021006815,...
    0.016477531, 0.012260858, 0.006334812, 0.003109435, 0.003836673];
%Averaged Normalized Fraction; Azimuthal Position 4
Y4 = [0.981802648, 1.028865794, 0.997276748, 0.934058818, 0.907278947,...
    0.982333064, 0.948667327, 0.939313907, 0.847033942, 0.717111256,...
    0.575783076, 0.393563998, 0.065332997, 0.060880174];
err4 = [0.018029324, 0.018582973, 0.018141091, 0.017155722, 0.01685918,...
    0.017858786, 0.017355145, 0.017262031, 0.016119061, 0.014480761,...
    0.012556997, 0.009977517, 0.003795995, 0.003656637];
%Average of the Averaged Normalized Fractions
Yavg = [1.00325614, 1.043517426, 1.011840587, 0.960100492,...
    0.942644115, 1.030364797, 1.035833406, 1.064907129, 0.986383295,...
    0.811520734, 0.645993455, 0.355070049, 0.071943568, 0.061122847];
errAvg = [0.009194479, 0.009262631, 0.009032196, 0.008676541,...
    0.008577883, 0.009085484, 0.009071199, 0.009282786, 0.008819178,...
    0.007717902, 0.006636189, 0.004630123, 0.002000508, 0.00185032];

% Create figure
figure5 = figure;
% Create axes
axes5 = axes('Parent',figure5);
hold(axes5,'on');
%
EB1 = errorbar(Xh,Y1,err1,'LineWidth',1.5);
set(EB1,'DisplayName','Azimuthal 1 Avg.','Color',...
    [0 0.447058823529412 0.741176470588235]);
%
EB2 = errorbar(Xh,Y2,err2,'LineWidth',1.5);
set(EB2,'DisplayName','Azimuthal 2 Avg.','Color',[0.49 0.18 0.56]);
%
EB3 = errorbar(Xh,Y3,err3,'LineWidth',1.5);
set(EB3,'DisplayName','Azimuthal 3 Avg.','Color',[0.32 0.55 0.02]);
%
EB4 = errorbar(Xh,Y4,err4,'LineWidth',1.5);
set(EB4,'DisplayName','Azimuthal 4 Avg.','Color',[0.85,0.33,0.10]);
%
EBavg = errorbar(Xh,Yavg,errAvg,'LineWidth',2);
set(EBavg,'DisplayName','Overall Azimuthal Avg.','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of coinc. rate to rate at center'});

% Create xlabel
xlabel({'Distance From Center of 10" PMT'});

% Create title
title({'10" PMT Testing - Azimuthal Averages & Overall Average'});

box(axes5,'on');
% Set the remaining axes properties
set(axes5,'XGrid','on','XTick',...
    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7],'YGrid','on','YTick',...
    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2 1.3 1.4 1.5]);
% Create legend
% Create legend
legend5 = legend(axes5,'show');
set(legend5,...
    'Position',[0.735725938009788 0.744038155802862 0.156199021207178 0.155802894093464]);


% Create figure
figure6 = figure;
% Create axes
axes6 = axes('Parent',figure6);
hold(axes6,'on');
%
%Azimuthal Position 1 
%
EB11 = errorbar(Xh,Y1avg1,err1Avg1,'LineWidth',1.5);
set(EB11,'DisplayName','Half In. #2 06/06','Color',[0.32 0.70 0.96]);
%
EB12 = errorbar(Xh,Y1avg2,err1Avg2,'LineWidth',1.5);
set(EB12,'DisplayName','Half In. #5 06/06','Color',[0.32 0.70 0.96]);
%
EB13 = errorbar(Xh,Y1avg3,err1Avg3,'LineWidth',1.5);
set(EB13,'DisplayName','Half In. #6 06/06','Color',[0.32 0.70 0.96]);
%
EB14 = errorbar(Xh,Y1avg4,err1Avg4,'LineWidth',1.5);
set(EB14,'DisplayName','Half In. #7 06/06','Color',[0.32 0.70 0.96]);
%
EB1 = errorbar(Xh,Y1,err1,'LineWidth',2);
set(EB1,'DisplayName','Azimuthal 1 Avg.','Color',...
    [0 0.447058823529412 0.741176470588235]);
%
%Azimuthal Position 2   
%
EB21 = errorbar(Xh,Y2avg1,err2Avg1,'LineWidth',1.5);
set(EB21,'DisplayName','Half In. 04/24','Color',[0.76 0.36 0.85]);
%
EB22 = errorbar(XhB,Y2avg2,err2Avg2,'LineWidth',1.5);
set(EB22,'DisplayName','Half In. 05/09','Color',[0.76 0.36 0.85]);
%
EB23 = errorbar(Xh,Y2avg3,err2Avg3,'LineWidth',1.5);
set(EB23,'DisplayName','Half In. #3 05/10','Color',[0.76 0.36 0.85]);
%
EB24 = errorbar(Xh,Y2avg4,err2Avg4,'LineWidth',1.5);
set(EB24,'DisplayName','Half In. #4 05/10','Color',[0.76 0.36 0.85]);
%
EB25 = errorbar(Xhi,Y2avg5,err2Avg5,'LineWidth',1.5);
set(EB25,'DisplayName','Half In. 05/07','Color',[0.76 0.36 0.85]);
%
EB2 = errorbar(Xh,Y2,err2,'LineWidth',2);
set(EB2,'DisplayName','Azimuthal 2 Avg.','Color',[0.49 0.18 0.56]);
%
%Azimuthal Position 3   
%
EB31 = errorbar(Xh,Y3avg1,err3Avg1,'LineWidth',1.5);
set(EB31,'DisplayName','Half In. #2 05/22','Color',[0.64 0.93 0.27]);
%
EB32 = errorbar(Xh,Y3avg2,err3Avg2,'LineWidth',1.5);
set(EB32,'DisplayName','Half In. #3 05/22','Color',[0.64 0.93 0.27]);
%
EB33 = errorbar(Xh,Y3avg3,err3Avg3,'LineWidth',1.5);
set(EB33,'DisplayName','Half In. #4 05/22','Color',[0.64 0.93 0.27]);
%
EB34 = errorbar(Xh,Y3avg4,err3Avg4,'LineWidth',1.5);
set(EB34,'DisplayName','Half In. #5 05/22','Color',[0.64 0.93 0.27]);
%
EB3 = errorbar(Xh,Y3,err3,'LineWidth',2);
set(EB3,'DisplayName','Azimuthal 3 Avg.','Color',[0.32 0.55 0.02]);
%
%Azimuthal Position 4   
%
EB41 = errorbar(Xh,Y4avg1,err4Avg1,'LineWidth',1.5);
set(EB41,'DisplayName','Half In. #2 06/03','Color',[0.98,0.62,0.47]);
%
EB42 = errorbar(Xh,Y4avg2,err4Avg2,'LineWidth',1.5);
set(EB42,'DisplayName','Half In. #3 06/03','Color',[0.98,0.62,0.47]);
%
EB43 = errorbar(Xh,Y4avg3,err4Avg3,'LineWidth',1.5);
set(EB43,'DisplayName','Half In. #4 06/03','Color',[0.98,0.62,0.47]);
%
EB44 = errorbar(Xh,Y4avg4,err4Avg4,'LineWidth',1.5);
set(EB44,'DisplayName','Half In. #5 06/03','Color',[0.98,0.62,0.47]);
%
EB4avg = errorbar(Xh,Y4,err4,'LineWidth',2);
set(EB4avg,'DisplayName','Azimuthal Pos. 4 Avg.','Color',[0.85,0.33,0.10]);
%
%
EBavg = errorbar(Xh,Yavg,errAvg,'LineWidth',2.5);
set(EBavg,'DisplayName','Overall Azimuthal Avg.','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of coinc. rate to rate at center'});

% Create xlabel
xlabel({'Distance From Center of 10" PMT'});

% Create title
title({'10" PMT Testing - Results, Averages, and Overall Average'});

box(axes6,'on');
% Set the remaining axes properties
set(axes6,'XGrid','on','XTick',...
    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7],'YGrid','on','YTick',...
    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2 1.3 1.4 1.5]);
% Create legend
% Create legend
legend6 = legend(axes6,'show');
set(legend6,...
    'Position',[0.735725938009788 0.744038155802862 0.156199021207178 0.155802894093464]);

