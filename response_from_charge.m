%Positions from center; half inch code
Xh = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5];

%Normalized Fraction from Charge Test #1 12/02
Y1 = [1.019467715, 1.021062707, 1.017666868, 0.994181693, 9.376343E-01,...
    0.980563072, 0.943480801, 0.951023993, 0.923298313, 0.900901473,...
    0.882945611, 0.821093592, 0.807631064, 8.429074E-01];
err1 = [0.832548316, 0.849814777, 0.846323155, 0.813938016, 0.669629733,...
    0.813070205, 0.764992468, 0.762810875, 0.743308149, 0.712850794,...
    0.714603653, 0.644089802, 0.624607389, 0.650641587];
%Averaged Normalized Fraction; Azimuthal Position 4 (Prior Tests; 06/06/19)
Yavg4 = [0.981802648, 1.028865794, 0.997276748, 0.934058818, 0.907278947,...
    0.982333064, 0.948667327, 0.939313907, 0.847033942, 0.717111256,...
    0.575783076, 0.393563998, 0.065332997, 0.060880174];
errAvg4 = [0.018029324, 0.018582973, 0.018141091, 0.017155722, 0.01685918,...
    0.017858786, 0.017355145, 0.017262031, 0.016119061, 0.014480761,...
    0.012556997, 0.009977517, 0.003795995, 0.003656637];


% Create figure
figure1 = figure;
% Create axes
axes1 = axes('Parent',figure1);
hold(axes1,'on');
%
EBh1 = shadedErrorBar(Xh, Y1, err1, 'lineprops', '-r',...
    'transparent',false,'patchSaturation',0.075);
%set(EBh1,'DisplayName','1/2" Test #1 12/02/19','Color',...
%    [1 0 0]);
%
EBavg2 = errorbar(Xh,Yavg4,errAvg4,'LineWidth',2,'Color',[0 0 0]);
%set(EBavg2,'DisplayName','Avg. Azimuthal Pos. 4 06/06/19','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of average charge to average charge at center'},'FontSize', 14);
% Create xlabel
xlabel({'Distance From Center of 10" PMT (inches)'},'FontSize', 14);
% Create title
title({'10" PMT Testing - Dec 02 Result w/ Prior Az4 Average'},'FontSize', 18);
box(axes1,'on');
% Set the remaining axes properties
%set(axes1,'XGrid','on','XTick',...
%    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5],'YGrid','on','YTick',...
%    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2]);
% Create legend
% Create legend
legend(['12/02/19 pt.0'; '06/06/19 avg.'],'location','northeast','FontSize', 12)
legend
grid on

%-----------------------------------------------------


%Positions from center; half inch code
Xh = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5];

%Normalized Fraction from Charge Test #2 12/03
Y2 = [0.980742526, 1.041972311, 1.02049895, 0.944280912, 9.590921E-01,...
    0.970090993, 0.964527483, 0.965661189, 0.946074992, 0.899953935,...
    0.892291751, 0.846790086, 0.872313996, 9.153469E-01];
err2 = [0.80377417, 0.855528951, 0.841766987, 0.756246486, 0.747140725,...
    0.778057878, 0.767005541, 0.759310116, 0.745900691, 0.64331812,...
    0.719637185, 0.66203334, 0.675340199, 0.705642116];
%Averaged Normalized Fraction; Azimuthal Position 4 (Prior Tests; 06/06/19)
Yavg4 = [0.981802648, 1.028865794, 0.997276748, 0.934058818, 0.907278947,...
    0.982333064, 0.948667327, 0.939313907, 0.847033942, 0.717111256,...
    0.575783076, 0.393563998, 0.065332997, 0.060880174];
errAvg4 = [0.018029324, 0.018582973, 0.018141091, 0.017155722, 0.01685918,...
    0.017858786, 0.017355145, 0.017262031, 0.016119061, 0.014480761,...
    0.012556997, 0.009977517, 0.003795995, 0.003656637];


% Create figure
figure2 = figure;
% Create axes
axes2 = axes('Parent',figure2);
hold(axes2,'on');
%
EBh2 = shadedErrorBar(Xh, Y2, err2, 'lineprops', '-r',...
    'transparent',false,'patchSaturation',0.075);
%set(EBh1,'DisplayName','1/2" Test #1 12/02/19','Color',...
%    [1 0 0]);
%
EBavg2 = errorbar(Xh,Yavg4,errAvg4,'LineWidth',2,'Color',[0 0 0]);
%set(EBavg2,'DisplayName','Avg. Azimuthal Pos. 4 06/06/19','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of average charge to average charge at center'},'FontSize', 14);
% Create xlabel
xlabel({'Distance From Center of 10" PMT (inches)'},'FontSize', 14);
% Create title
title({'10" PMT Testing - Dec 03 Result w/ Prior Az4 Average'},'FontSize', 18);
box(axes2,'on');
% Set the remaining axes properties
%set(axes1,'XGrid','on','XTick',...
%    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5],'YGrid','on','YTick',...
%    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2]);
% Create legend
% Create legend
legend(['12/03/19 pt.0'; '06/06/19 avg.'],'location','northeast','FontSize', 12)
legend
grid on

%-----------------------------------------------------


%Positions from center; half inch code
Xh = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5];
%Positions from center; half inch code, partial run
Xhp = [0, 0.5, 3, 3.5, 4];

%Normalized Fraction from Charge Test #3 12/04
Y3 = [0.957444398, 1.05500623, 1.018201956, 0.946777627, 9.690384E-01,...
    0.956655145, 0.959949705, 0.907142226, 0.91055409, 0.940326169,...
    0.886945998, 0.830312568, 0.78135349, 8.163334E-01];
err3 = [0.762991425, 0.889395984, 0.843610384, 0.781455542, 0.809173959,...
    0.786798383, 0.798586729, 0.731050706, 0.735862341, 0.77070482,...
    0.742782832, 0.659758746, 0.60909105, 0.626301691];
%Normalized Fraction from Charge Test #4 12/04
Y4 = [1.007061275, 9.920859E-01, 0.947127003, 0.949742148, 0.921856055];
err4 = [0.837008618, 0.821982481, 0.777761736, 0.743738532, 0.750598216];
%Averaged Normalized Fraction; Azimuthal Position 4 (Prior Tests; 06/06/19)
Yavg4 = [0.981802648, 1.028865794, 0.997276748, 0.934058818, 0.907278947,...
    0.982333064, 0.948667327, 0.939313907, 0.847033942, 0.717111256,...
    0.575783076, 0.393563998, 0.065332997, 0.060880174];
errAvg4 = [0.018029324, 0.018582973, 0.018141091, 0.017155722, 0.01685918,...
    0.017858786, 0.017355145, 0.017262031, 0.016119061, 0.014480761,...
    0.012556997, 0.009977517, 0.003795995, 0.003656637];


% Create figure
figure3 = figure;
% Create axes
axes3 = axes('Parent',figure3);
hold(axes3,'on');
%
EBh3 = shadedErrorBar(Xh, Y3, err3, 'lineprops', '-r',...
    'transparent',false,'patchSaturation',0.075);
%set(EBh1,'DisplayName','1/2" Test #1 12/02/19','Color',...
%    [1 0 0]);
EBh4 = errorbar(Xhp, Y4, err4, 'LineStyle','none','Color',[1 0 0]);
%
EBavg2 = errorbar(Xh,Yavg4,errAvg4,'LineWidth',2,'Color',[0 0 0]);
%set(EBavg2,'DisplayName','Avg. Azimuthal Pos. 4 06/06/19','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of average charge to average charge at center'},'FontSize', 14);
% Create xlabel
xlabel({'Distance From Center of 10" PMT (inches)'},'FontSize', 14);
% Create title
title({'10" PMT Testing - Dec 04 Results w/ Prior Az4 Average'},'FontSize', 18);
box(axes3,'on');
% Set the remaining axes properties
%set(axes1,'XGrid','on','XTick',...
%    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5],'YGrid','on','YTick',...
%    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2]);
% Create legend
% Create legend
legend(['12/04/19 pt.1'; '12/04/19 pt.2'; '06/06/19 avg.'],'location','northeast','FontSize', 12)
legend
grid on

%-----------------------------------------------------


%Positions from center; half inch code
Xh = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5];

%Normalized Fraction from Charge Test #5 12/06
Y5 = [1.039426133, 1.000001736, 0.981237937, 1.004947627, 9.353331E-01,...
    0.953640132, 1.007856334, 0.937044406, 0.958921212, 0.923606755,...
    0.891134951, 0.818209052, 0.82587964, 8.788462E-01];
err5 = [0.858243994, 0.791039315, 0.794554387, 0.817930418, 0.742316083,...
    0.760249933, 0.808273358, 0.742772532, 0.779472857, 0.736473089,...
    0.706807177, 0.641721329, 0.63739501, 0.670590127];
%Averaged Normalized Fraction; Azimuthal Position 4 (Prior Tests; 06/06/19)
Yavg4 = [0.981802648, 1.028865794, 0.997276748, 0.934058818, 0.907278947,...
    0.982333064, 0.948667327, 0.939313907, 0.847033942, 0.717111256,...
    0.575783076, 0.393563998, 0.065332997, 0.060880174];
errAvg4 = [0.018029324, 0.018582973, 0.018141091, 0.017155722, 0.01685918,...
    0.017858786, 0.017355145, 0.017262031, 0.016119061, 0.014480761,...
    0.012556997, 0.009977517, 0.003795995, 0.003656637];


% Create figure
figure4 = figure;
% Create axes
axes4 = axes('Parent',figure4);
hold(axes4,'on');
%
EBh5 = shadedErrorBar(Xh, Y5, err5, 'lineprops', '-r',...
    'transparent',false,'patchSaturation',0.075);
%set(EBh1,'DisplayName','1/2" Test #1 12/02/19','Color',...
%    [1 0 0]);
%EBh4 = errorbar(Xhp, Y4, err4, 'LineStyle','none','Color',[1 0 0]);
%
EBavg2 = errorbar(Xh,Yavg4,errAvg4,'LineWidth',2,'Color',[0 0 0]);
%set(EBavg2,'DisplayName','Avg. Azimuthal Pos. 4 06/06/19','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of average charge to average charge at center'},'FontSize', 14);
% Create xlabel
xlabel({'Distance From Center of 10" PMT (inches)'},'FontSize', 14);
% Create title
title({'10" PMT Testing - Dec 06 Result w/ Prior Az4 Average'},'FontSize', 18);
box(axes4,'on');
% Set the remaining axes properties
%set(axes1,'XGrid','on','XTick',...
%    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5],'YGrid','on','YTick',...
%    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2]);
% Create legend
% Create legend
legend(['12/06/19 pt.0'; '06/06/19 avg.'],'location','northeast','FontSize', 12)
legend
grid on

%-----------------------------------------------------


%Positions from center; half inch code
Xh = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5];

%Normalized Fraction from Charge Test #6 01/06
Y6 = [1.049461721, 1.051521279, 1.008404859, 0.976607917, 8.730636E-01,...
    1.02888259, 1.024779075, 0.938795074, 0.931491043, 0.849880839,...
    0.825756514, 0.808981056, 0.763453046, 7.463502E-01];
err6 = [0.900958332, 0.892242124, 0.854930398, 0.833425472, 0.710499986,...
    0.864651743, 0.878558251, 0.778396802, 0.766434788, 0.683728995,...
    0.712497373, 0.646426552, 0.591206603, 0.577172982];
%Averaged Normalized Fraction; Azimuthal Position 4 (Prior Tests; 06/06/19)
Yavg4 = [0.981802648, 1.028865794, 0.997276748, 0.934058818, 0.907278947,...
    0.982333064, 0.948667327, 0.939313907, 0.847033942, 0.717111256,...
    0.575783076, 0.393563998, 0.065332997, 0.060880174];
errAvg4 = [0.018029324, 0.018582973, 0.018141091, 0.017155722, 0.01685918,...
    0.017858786, 0.017355145, 0.017262031, 0.016119061, 0.014480761,...
    0.012556997, 0.009977517, 0.003795995, 0.003656637];


% Create figure
figure5 = figure;
% Create axes
axes5 = axes('Parent',figure5);
hold(axes5,'on');
%
EBh6 = shadedErrorBar(Xh, Y6, err6, 'lineprops', '-r',...
    'transparent',false,'patchSaturation',0.075);
%set(EBh1,'DisplayName','1/2" Test #1 12/02/19','Color',...
%    [1 0 0]);
%EBh4 = errorbar(Xhp, Y4, err4, 'LineStyle','none','Color',[1 0 0]);
%
EBavg2 = errorbar(Xh,Yavg4,errAvg4,'LineWidth',2,'Color',[0 0 0]);
%set(EBavg2,'DisplayName','Avg. Azimuthal Pos. 4 06/06/19','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of average charge to average charge at center'},'FontSize', 14);
% Create xlabel
xlabel({'Distance From Center of 10" PMT (inches)'},'FontSize', 14);
% Create title
title({'10" PMT Testing - Jan 06 Result w/ Prior Az4 Average'},'FontSize', 18);
box(axes5,'on');
% Set the remaining axes properties
%set(axes1,'XGrid','on','XTick',...
%    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5],'YGrid','on','YTick',...
%    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2]);
% Create legend
% Create legend
legend(['01/06/20 pt.0'; '06/06/19 avg.'],'location','northeast','FontSize', 12)
legend
grid on

%-----------------------------------------------------


%Positions from center; half inch code
Xh = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5];

%Normalized Fraction from Charge Test #7 01/07
Y7 = [1.003133108, 0.968738961, 1.007790211, 0.966898255, 1.006520E+00,...
    0.965936343, 0.99152514, 0.950683366, 0.93625009, 0.888266521,...
    0.850041473, 0.811259643, 0.684854008, 7.549486E-01];
err7 = [0.84128187, 0.885146956, 0.826119373, 0.800027286, 0.851001309,...
    0.77412821, 0.807807806, 0.772797754, 0.745122074, 0.712660562,...
    0.721665717, 0.653743098, 0.528993436, 0.584515496];
%Averaged Normalized Fraction; Azimuthal Position 4 (Prior Tests; 06/06/19)
Yavg4 = [0.981802648, 1.028865794, 0.997276748, 0.934058818, 0.907278947,...
    0.982333064, 0.948667327, 0.939313907, 0.847033942, 0.717111256,...
    0.575783076, 0.393563998, 0.065332997, 0.060880174];
errAvg4 = [0.018029324, 0.018582973, 0.018141091, 0.017155722, 0.01685918,...
    0.017858786, 0.017355145, 0.017262031, 0.016119061, 0.014480761,...
    0.012556997, 0.009977517, 0.003795995, 0.003656637];


% Create figure
figure6 = figure;
% Create axes
axes6 = axes('Parent',figure6);
hold(axes6,'on');
%
EBh7 = shadedErrorBar(Xh, Y7, err7, 'lineprops', '-r',...
    'transparent',false,'patchSaturation',0.075);
%set(EBh1,'DisplayName','1/2" Test #1 12/02/19','Color',...
%    [1 0 0]);
%EBh4 = errorbar(Xhp, Y4, err4, 'LineStyle','none','Color',[1 0 0]);
%
EBavg2 = errorbar(Xh,Yavg4,errAvg4,'LineWidth',2,'Color',[0 0 0]);
%set(EBavg2,'DisplayName','Avg. Azimuthal Pos. 4 06/06/19','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of average charge to average charge at center'},'FontSize', 14);
% Create xlabel
xlabel({'Distance From Center of 10" PMT (inches)'},'FontSize', 14);
% Create title
title({'10" PMT Testing - Jan 07 Result w/ Prior Az4 Average'},'FontSize', 18);
box(axes6,'on');
% Set the remaining axes properties
%set(axes1,'XGrid','on','XTick',...
%    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5],'YGrid','on','YTick',...
%    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2]);
% Create legend
% Create legend
legend(['01/07/20 pt.0'; '06/06/19 avg.'],'location','northeast','FontSize', 12)
legend
grid on

%-----------------------------------------------------


%Positions from center; half inch code
Xh = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5];

%Normalized Fraction from Charge Test Average (12/02--01/07)
Y8 = [1.008105268, 1.018627023, 1.008966797, 0.972282339, 0.946780248,...
    0.975961379, 0.977035077, 0.942870343, 0.932635114, 0.900489282,...
    0.871519383, 0.822774333, 0.789247541, 0.825788794];
err8 = [0.315516836, 0.323441939, 0.340800076, 0.326969607, 0.309184966,...
    0.325336834, 0.302839923, 0.285744406, 0.284424698, 0.290295032,...
    0.29383918, 0.26590908, 0.25015786, 0.260234788];
%Averaged Normalized Fraction; Azimuthal Position 4 (Prior Tests; 06/06/19)
Yavg4 = [0.981802648, 1.028865794, 0.997276748, 0.934058818, 0.907278947,...
    0.982333064, 0.948667327, 0.939313907, 0.847033942, 0.717111256,...
    0.575783076, 0.393563998, 0.065332997, 0.060880174];
errAvg4 = [0.018029324, 0.018582973, 0.018141091, 0.017155722, 0.01685918,...
    0.017858786, 0.017355145, 0.017262031, 0.016119061, 0.014480761,...
    0.012556997, 0.009977517, 0.003795995, 0.003656637];


% Create figure
figure7 = figure;
% Create axes
axes7 = axes('Parent',figure7);
hold(axes7,'on');
%
EBh1 = shadedErrorBar(Xh, Y1, err1, 'lineprops', '-r',...
    'transparent',false,'patchSaturation',0.075);
%
EBh2 = shadedErrorBar(Xh, Y2, err2, 'lineprops', '-r',...
    'transparent',false,'patchSaturation',0.075);
%
EBh3 = shadedErrorBar(Xh, Y3, err3, 'lineprops', '-r',...
    'transparent',false,'patchSaturation',0.075);
%
EBh4 = errorbar(Xhp, Y4, err4, 'LineStyle','none','Color',[1 0 0]);
%
EBh5 = shadedErrorBar(Xh, Y5, err5, 'lineprops', '-r',...
    'transparent',false,'patchSaturation',0.075);
%
EBh6 = shadedErrorBar(Xh, Y6, err6, 'lineprops', '-r',...
    'transparent',false,'patchSaturation',0.075);
%
EBh7 = shadedErrorBar(Xh, Y7, err7, 'lineprops', '-r',...
    'transparent',false,'patchSaturation',0.075);
%
EBh8 = shadedErrorBar(Xh, Y8, err8, 'lineprops', '-b',...
    'transparent',false,'patchSaturation',0.075);
%set(EBh1,'DisplayName','1/2" Test #1 12/02/19','Color',...
%    [1 0 0]);
%
EBavg2 = errorbar(Xh,Yavg4,errAvg4,'LineWidth',2,'Color',[0 0 0]);
%set(EBavg2,'DisplayName','Avg. Azimuthal Pos. 4 06/06/19','Color',[0 0 0]);

% Create ylabel
ylabel({'Ratio of average charge to average charge at center'},'FontSize', 14);
% Create xlabel
xlabel({'Distance From Center of 10" PMT (inches)'},'FontSize', 14);
% Create title
title({'10" PMT Testing - Dec 02--Jan 07 Avg Result w/ Prior Az4 Average'},'FontSize', 18);
box(axes6,'on');
% Set the remaining axes properties
%set(axes1,'XGrid','on','XTick',...
%    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5],'YGrid','on','YTick',...
%    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2]);
% Create legend
% Create legend
legend(['12/02/19 pt.0'; '12/03/19 pt.0'; '12/04/19 pt.1';...
    '12/04/19 pt.2'; '12/06/19 pt.0'; '01/06/20 pt.0'; '01/07/20 pt.0';...
    '12/2-1/7 avg.'; '06/06/19 avg.'],'location','northeast','FontSize', 12)
legend
grid on