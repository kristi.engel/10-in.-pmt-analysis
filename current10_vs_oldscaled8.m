%% Averaged 10" Normalized Fractions w/ Scaled 8" Results
% Azimuthal Positions 1, 2, 3, and 4

%% Plotting w/ Current Implementation
% Currently just using a scaled 8" response for the 10"

%Positions from center; 10" PMT
Xh = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5];
%Positions from center; 8" Scaled (first two)
Xs = [0, 1.181818182, 2.363636364, 3.545454545, 4.727272727,...
    5.909090909, 6.5];
%Positions from center; 8" Scaled (last three)
Xsi = [0, 1.181818182, 2.363636364, 3.545454545, 4.727272727, 5.909090909];


%Averaged Normalized Fraction; Azimuthal Position 1
Y1 = [0.991175228, 0.988222244, 0.929630803, 0.83195414, 0.756894198,...
    0.846815854, 0.884145208, 0.900020714, 0.839545348, 0.72631714,...
    0.603390657, 0.280198157, 0.129253285, 0.056187287];
err1 = [0.017528369, 0.017411385, 0.016722479, 0.015552621,...
    0.014586996, 0.015611808, 0.015977548, 0.016166465, 0.01542594,...
    0.014032977, 0.012490509, 0.007931386, 0.005221711, 0.003398403];
%Averaged Normalized Fraction; Azimuthal Position 2
Y2 = [1.005147095, 0.993339427, 0.991425499, 0.977838886,...
    0.970646255, 1.014217098, 1.053693383, 1.080761314,...
    1.048038379, 0.962471619, 0.881700475, 0.58226709, 0.051483387,...
    0.065140218];
err2 = [0.018718381, 0.017184721, 0.017191513, 0.017029779,...
    0.016895667, 0.017196822, 0.017647717, 0.017957251, 0.017475023,...
    0.016581025, 0.015511429, 0.011849936, 0.003562036, 0.003890856];
%Averaged Normalized Fraction; Azimuthal Position 3
Y3 = [1.034899589, 1.16364224, 1.129029299, 1.096550126,...
    1.135757061, 1.27809317, 1.256827705, 1.339532583, 1.210915512,...
    0.840182921, 0.523099614, 0.164250948, 0.041704602, 0.06228371];
err3 = [0.019233749, 0.020710947, 0.020024995, 0.01944998,...
    0.019869705, 0.021502109, 0.021181626, 0.022291852, 0.021006815,...
    0.016477531, 0.012260858, 0.006334812, 0.003109435, 0.003836673];
%Averaged Normalized Fraction; Azimuthal Position 4
Y4 = [0.981802648, 1.028865794, 0.997276748, 0.934058818, 0.907278947,...
    0.982333064, 0.948667327, 0.939313907, 0.847033942, 0.717111256,...
    0.575783076, 0.393563998, 0.065332997, 0.060880174];
err4 = [0.018029324, 0.018582973, 0.018141091, 0.017155722, 0.01685918,...
    0.017858786, 0.017355145, 0.017262031, 0.016119061, 0.014480761,...
    0.012556997, 0.009977517, 0.003795995, 0.003656637];
%Average of the Averaged Normalized Fractions
Yavg = [1.00325614, 1.043517426, 1.011840587, 0.960100492,...
    0.942644115, 1.030364797, 1.035833406, 1.064907129, 0.986383295,...
    0.811520734, 0.645993455, 0.355070049, 0.071943568, 0.061122847];
errAvg = [0.009194479, 0.009262631, 0.009032196, 0.008676541,...
    0.008577883, 0.009085484, 0.009071199, 0.009282786, 0.008819178,...
    0.007717902, 0.006636189, 0.004630123, 0.002000508, 0.00185032];

% cratesaug3-aug13 normalized using avg. central rate
Y5 = [1.006496176, 0.952635474, 0.915631938, 0.838746814, 0.713962668,...
    0.348244388, 0.099498397];
Y6 = [0.994983965, 0.953868925, 0.910698133, 0.806265932, 0.681481786,...
    0.363868103, 0.097442645];
Y7 = [0.993339364, 0.937422909, 0.93269468, 0.860126634, 0.711495765,...
    0.356056245];
Y8 = [0.998478744, 0.921799194, 0.931255653, 0.845941946, 0.687237892,...
    0.353589343];
Y9 = [1.006701752, 0.937422909, 0.93310583, 0.85560398, 0.695666475,...
    0.351739166];

% Create figure
figure1 = figure;
% Create axes
axes1 = axes('Parent',figure1);
hold(axes1,'on');
%
EBavg = errorbar(Xh,Yavg,errAvg,'LineWidth',2);
set(EBavg,'DisplayName','Overall Azimuthal Avg.','Color',[0 0 0]);
%
scale1 = plot(Xs,Y5,'o','MarkerIndices',1:1:length(Y5),'LineWidth',1.5);
set(scale1,'DisplayName','8" Normalized Result; Scaled by 6.5/5.5',...
    'Color',[0.64,0.08,0.18]);
%
scale2 = plot(Xs,Y6,'o','MarkerIndices',1:1:length(Y6),'LineWidth',1.5);
set(scale2,'DisplayName','8" Normalized Result; Scaled by 6.5/5.5',...
    'Color',[0.64,0.08,0.18]);
%
scale3 = plot(Xsi,Y7,'o','MarkerIndices',1:1:length(Y7),'LineWidth',1.5);
set(scale3,'DisplayName','8" Normalized Result; Scaled by 6.5/5.5',...
    'Color',[0.64,0.08,0.18]);
%
scale4 = plot(Xsi,Y8,'o','MarkerIndices',1:1:length(Y8),'LineWidth',1.5);
set(scale4,'DisplayName','8" Normalized Result; Scaled by 6.5/5.5',...
    'Color',[0.64,0.08,0.18]);
%
scale5 = plot(Xsi,Y9,'o','MarkerIndices',1:1:length(Y9),'LineWidth',1.5);
set(scale5,'DisplayName','8" Normalized Result; Scaled by 6.5/5.5',...
    'Color',[0.64,0.08,0.18]);

% Create ylabel
ylabel({'Ratio of coinc. rate to rate at center'});

% Create xlabel
xlabel({'Distance From Center of 10" PMT'});

% Create title
title({'10" PMT Testing - Overall Average w/ Scaled 8" Result'});

box(axes1,'on');
% Set the remaining axes properties
set(axes1,'XGrid','on','XTick',...
    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7],'YGrid','on','YTick',...
    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2 1.3 1.4 1.5]);
% Create legend
% Create legend
legend1 = legend(axes1,'show');
set(legend1,...
    'Position',[0.735725938009788 0.744038155802862 0.156199021207178 0.155802894093464]);


% Create figure
figure2 = figure;
% Create axes
axes2 = axes('Parent',figure2);
hold(axes2,'on');
%
EB1 = errorbar(Xh,Y1,err1,'LineWidth',1.5);
set(EB1,'DisplayName','Azimuthal 1 Avg.','Color',...
    [0 0.447058823529412 0.741176470588235]);
%
EB2 = errorbar(Xh,Y2,err2,'LineWidth',1.5);
set(EB2,'DisplayName','Azimuthal 2 Avg.','Color',[0.49 0.18 0.56]);
%
EB3 = errorbar(Xh,Y3,err3,'LineWidth',1.5);
set(EB3,'DisplayName','Azimuthal 3 Avg.','Color',[0.32 0.55 0.02]);
%
EB4 = errorbar(Xh,Y4,err4,'LineWidth',1.5);
set(EB4,'DisplayName','Azimuthal 4 Avg.','Color',[0.85,0.33,0.10]);
%
EBavg2 = errorbar(Xh,Yavg,errAvg,'LineWidth',2);
set(EBavg2,'DisplayName','Overall Azimuthal Avg.','Color',[0 0 0]);
%
scale12 = plot(Xs,Y5,'-o','MarkerIndices',1:1:length(Y5),'LineWidth',1.5);
set(scale12,'DisplayName','8" Normalized Result; Scaled by 6.5/5.5',...
    'Color',[0.64,0.08,0.18]);
%
scale22 = plot(Xs,Y6,'-o','MarkerIndices',1:1:length(Y6),'LineWidth',1.5);
set(scale22,'DisplayName','8" Normalized Result; Scaled by 6.5/5.5',...
    'Color',[0.64,0.08,0.18]);
%
scale32 = plot(Xsi,Y7,'-o','MarkerIndices',1:1:length(Y7),'LineWidth',1.5);
set(scale32,'DisplayName','8" Normalized Result; Scaled by 6.5/5.5',...
    'Color',[0.64,0.08,0.18]);
%
scale42 = plot(Xsi,Y8,'-o','MarkerIndices',1:1:length(Y8),'LineWidth',1.5);
set(scale42,'DisplayName','8" Normalized Result; Scaled by 6.5/5.5',...
    'Color',[0.64,0.08,0.18]);
%
scale52 = plot(Xsi,Y9,'-o','MarkerIndices',1:1:length(Y9),'LineWidth',1.5);
set(scale52,'DisplayName','8" Normalized Result; Scaled by 6.5/5.5',...
    'Color',[0.64,0.08,0.18]);

% Create ylabel
ylabel({'Ratio of coinc. rate to rate at center'});

% Create xlabel
xlabel({'Distance From Center of 10" PMT'});

% Create title
title({'10" PMT Testing - Azimuthal Averages & Overall Average w/ Scaled 8" Result'});

box(axes2,'on');
% Set the remaining axes properties
set(axes2,'XGrid','on','XTick',...
    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7],'YGrid','on','YTick',...
    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2 1.3 1.4 1.5]);
% Create legend
% Create legend
legend2 = legend(axes2,'show');
set(legend2,...
    'Position',[0.735725938009788 0.744038155802862 0.156199021207178 0.155802894093464]);

%% Scaled Normalized Fractional Rates

%Positions from center; 10" PMT
Xh = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5];
%Positions from center; 8" Scaled (first two)
Xs = [0, 1.181818182, 2.363636364, 3.545454545, 4.727272727,...
    5.909090909, 6.5];
%Positions from center; 8" Scaled (last three)
Xsi = [0, 1.181818182, 2.363636364, 3.545454545, 4.727272727, 5.909090909];


%Scaled Average of the Averaged Normalized Fractions
YSavg = [0, 0.521758713, 1.011840587, 1.440150739, 1.885288231,...
    2.575911991, 3.107500217, 3.727174953, 3.94553318, 3.651843304,...
    3.229967277, 1.952885267, 0.431661406, 0.397298507];
errSavg = [0, 0.004631315, 0.009032196, 0.013014812, 0.017155765,...
    0.022713709, 0.027213596, 0.032489751, 0.035276712, 0.034730561,...
    0.033180944, 0.025465674, 0.012003049, 0.012027081];
%Scaled cratesaug3-aug13 normalized using avg. central rate
Y10 = [0, 1.125841924, 2.164220945, 2.973738703, 3.375096247,...
    2.057807746, 0.646739577];
Y11 = [0, 1.127299639, 2.152559224, 2.858579214, 3.221550261,...
    2.150129699, 0.633377189];
Y12 = [0, 1.107863438, 2.204551061, 3.049539885, 3.363434526, 2.103968723];
Y13 = [0, 1.089399048, 2.201149726, 2.999248716, 3.248760942, 2.089391572];
Y14 = [0, 1.107863438, 2.205522871, 3.03350502, 3.288605154, 2.078458709];

% Create figure
figure3 = figure;
% Create axes
axes3 = axes('Parent',figure3);
hold(axes3,'on');
%
EBavg3 = errorbar(Xh,YSavg,errSavg,'LineWidth',2);
set(EBavg3,'DisplayName','Overall Azimuthal Avg.','Color',[0 0 0]);
%
scale6 = plot(Xs,Y10,'o','MarkerIndices',1:1:length(Y10),'LineWidth',1.5);
set(scale6,'DisplayName','8" Normalized Result; Scaled by 6.5/5.5',...
    'Color',[0.64,0.08,0.18]);
%
scale7 = plot(Xs,Y11,'o','MarkerIndices',1:1:length(Y11),'LineWidth',1.5);
set(scale7,'DisplayName','8" Normalized Result; Scaled by 6.5/5.5',...
    'Color',[0.64,0.08,0.18]);
%
scale8 = plot(Xsi,Y12,'o','MarkerIndices',1:1:length(Y12),'LineWidth',1.5);
set(scale8,'DisplayName','8" Normalized Result; Scaled by 6.5/5.5',...
    'Color',[0.64,0.08,0.18]);
%
scale9 = plot(Xsi,Y13,'o','MarkerIndices',1:1:length(Y13),'LineWidth',1.5);
set(scale9,'DisplayName','8" Normalized Result; Scaled by 6.5/5.5',...
    'Color',[0.64,0.08,0.18]);
%
scale10 = plot(Xsi,Y14,'o','MarkerIndices',1:1:length(Y14),'LineWidth',1.5);
set(scale10,'DisplayName','8" Normalized Result; Scaled by 6.5/5.5',...
    'Color',[0.64,0.08,0.18]);

% Create ylabel
ylabel({'Ratio of coinc. rate to rate at center'});

% Create xlabel
xlabel({'R; Distance From Center of 10" PMT'});

% Create title
title({'10" PMT Testing - Overall Average w/ Scaled 8" Result - Both Scaled by R'});

box(axes3,'on');
% Set the remaining axes properties
set(axes3,'XGrid','on','XTick',...
    [0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 5.5 6 6.5 7],'YGrid','on','YTick',...
    [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 1.1 1.2 1.3 1.4 1.5 1.6 ...
    1.7 1.8 1.9 2.0 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 2.9 3.0 3.1 3.2 3.3 ...
    3.4 3.5 3.6 3.7 3.8 3.9 4.0]);
% Create legend
% Create legend
legend3 = legend(axes3,'show');
set(legend3,...
    'Position',[0.735725938009788 0.744038155802862 0.156199021207178 0.155802894093464]);
